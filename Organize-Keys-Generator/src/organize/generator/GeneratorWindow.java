/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.generator;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.awt.event.ActionEvent;

public class GeneratorWindow extends JFrame {
	private static final long serialVersionUID = 6612457640109920644L;

	private volatile Object lock = new Object();
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GeneratorWindow frame = new GeneratorWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GeneratorWindow() {
		setResizable(false);
		setTitle("Organize KeyPair Generator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 889, 237);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "KeyPair configuration", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		
		JButton btnNewButton = new JButton("Generate");
		
		JLabel lblGeneratedSuccessfully = new JLabel("Generated successfully.");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 431, Short.MAX_VALUE)
							.addGap(14))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(lblGeneratedSuccessfully, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
							.addGap(20)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 320, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblGeneratedSuccessfully, GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
					.addContainerGap())
		);
		
		JLabel lblNewLabel = new JLabel("Key size");
		
		JLabel lblNewLabel_2 = new JLabel("Algorithm");
		
		JComboBox comboBox = new JComboBox();
		comboBox.setEnabled(false);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"RSA"}));
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"512", "1024", "2048", "3072", "4096", "8192"}));
		comboBox_1.setSelectedIndex(3);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(12)
							.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblNewLabel))
					.addGap(27)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_2)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(12)
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)))
					.addGap(477))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox_1, GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
						.addComponent(comboBox, GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
					.addGap(35))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (lock) {
					lblGeneratedSuccessfully.setText("Generating...");
					btnNewButton.setEnabled(false);
					btnNewButton.setText("Working...");
					
					new Thread() {
						public void run() {
							synchronized (lock) {
								try {
									KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance((String) comboBox.getSelectedItem());
									keyPairGenerator.initialize(Integer.valueOf((String) comboBox_1.getSelectedItem()));
									KeyPair kp = keyPairGenerator.generateKeyPair();
									File pub = new File("./Base64RSAx509EncodedPublicKey.key");
									File priv = new File("./Base64PKSC8EncodedPrivateKey.key");
									
									String pubKey = new String(Base64.getEncoder().encode(new X509EncodedKeySpec(kp.getPublic().getEncoded()).getEncoded()));
									String privKey = new String(Base64.getEncoder().encode(new PKCS8EncodedKeySpec(kp.getPrivate().getEncoded()).getEncoded()));
									
									PrintWriter pWriter = new PrintWriter(new FileWriter(pub));
									pWriter.print(pubKey);
									pWriter.close();
									
									pWriter = new PrintWriter(new FileWriter(priv));
									pWriter.print(privKey);
									pWriter.close();
									
									System.out.println("Chave pública:");
									System.out.println(pubKey);
									System.out.println("---------");
									System.out.println("Chave privada:");
									System.out.println(privKey);
									
									lblGeneratedSuccessfully.setText("Generated at: " + new File(pub.getCanonicalPath()).getParent());
								} catch (Exception ex) {
									lblGeneratedSuccessfully.setText("Failed: " + ex.getMessage());
								}
							}
							
							btnNewButton.setText("Generate");
							btnNewButton.setEnabled(true);
						}
					}.start();
				}
				
			}
		});
	}
}

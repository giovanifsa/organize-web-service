--
-- Organize Framework - Framework for groupware softwares.
-- Copyright (C) 2018  Giovani Frigo

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
--

LOCK TABLES 
	DatabaseFilePart WRITE, 
	DatabaseFile WRITE, 
	DatabaseProfilePicture WRITE, 
	DatabaseUser WRITE,
	DatabaseGroup WRITE,
	DatabaseChatMessage WRITE,
	DatabaseChat WRITE,
	DatabaseFileRepositoryFile WRITE,
	DatabaseFileRepository WRITE,
	DatabaseGroupFileRepositoryFile WRITE,
	DatabaseGroupFileRepository WRITE,
	DatabaseUserFileRepositoryFile WRITE,
	DatabaseUserFileRepository WRITE,
	DatabaseCryptographySetting WRITE,
	DatabaseSession WRITE,
	DatabaseOnlineUser WRITE;
	
DROP TRIGGER IF EXISTS TriggerDatabaseFilePartInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseProfilePictureInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseGroupInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseChatMessageInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseFileRepositoryFileInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseGroupFileRepositoryFileInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseUserFileRepositoryFileInsert;
DROP TRIGGER IF EXISTS TriggerDatabaseUserDelete;
DROP TRIGGER IF EXISTS TriggerDatabaseSessionDelete;

CREATE TRIGGER TriggerDatabaseFilePartInsert AFTER INSERT ON DatabaseFilePart 
	FOR EACH ROW BEGIN 
			UPDATE DatabaseFile SET DatabaseFile.filePartsCount = DatabaseFile.filePartsCount + 1, DatabaseFile.fileSizeInBytes = DatabaseFile.fileSizeInBytes + NEW.filePartSize WHERE DatabaseFile.fileID = NEW.fileID; 
	END;
	
CREATE TRIGGER TriggerDatabaseProfilePictureInsert AFTER INSERT ON DatabaseProfilePicture 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseUser SET DatabaseUser.profilePictureCount = DatabaseUser.profilePictureCount + 1 WHERE DatabaseUser.userID = NEW.userID; 
	END;
	
CREATE TRIGGER TriggerDatabaseGroupInsert AFTER INSERT ON DatabaseGroup 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseUser SET DatabaseUser.groupOwnershipCount = DatabaseUser.groupOwnershipCount + 1 WHERE DatabaseUser.userID = NEW.ownerUserID; 
	END;
	
CREATE TRIGGER TriggerDatabaseChatMessageInsert AFTER INSERT ON DatabaseChatMessage 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseChat SET DatabaseChat.messageCount = DatabaseChat.messageCount + 1 WHERE DatabaseChat.chatID = NEW.chatID;
	END;
	
CREATE TRIGGER TriggerDatabaseFileRepositoryFileInsert AFTER INSERT ON DatabaseFileRepositoryFile 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseFileRepository SET DatabaseFileRepository.fileRepositoryFileCount = DatabaseFileRepository.fileRepositoryFileCount + 1 WHERE DatabaseFileRepository.fileRepositoryID = NEW.fileRepositoryID; 
	END;
	
CREATE TRIGGER TriggerDatabaseGroupFileRepositoryFileInsert AFTER INSERT ON DatabaseGroupFileRepositoryFile 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseGroupFileRepository SET DatabaseGroupFileRepository.groupFileRepositoryFileCount = DatabaseGroupFileRepository.groupFileRepositoryFileCount + 1 WHERE DatabaseGroupFileRepository.groupFileRepositoryID = NEW.groupFileRepositoryID; 
	END;
	
CREATE TRIGGER TriggerDatabaseUserFileRepositoryFileInsert AFTER INSERT ON DatabaseUserFileRepositoryFile 
	FOR EACH ROW BEGIN 
		UPDATE DatabaseUserFileRepository SET DatabaseUserFileRepository.userFileRepositoryFileCount = DatabaseUserFileRepository.userFileRepositoryFileCount + 1 WHERE DatabaseUserFileRepository.userFileRepositoryID = DatabaseUserFileRepositoryFile.userFileRepositoryID; 
	END;
	
CREATE TRIGGER TriggerDatabaseUserDelete AFTER DELETE ON DatabaseUser 
	FOR EACH ROW BEGIN 
		DELETE FROM DatabaseCryptographySetting WHERE DatabaseCryptographySetting.cryptographySettingID = OLD.cryptographySettingID; 
	END;

CREATE TRIGGER TriggerDatabaseSessionDelete AFTER DELETE ON DatabaseSession 
	FOR EACH ROW BEGIN 
		DELETE FROM DatabaseOnlineUser WHERE DatabaseOnlineUser.deviceToken = OLD.deviceToken; 
	END;
	
UNLOCK TABLES;
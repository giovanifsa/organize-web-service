--
-- Organize Framework - Framework for groupware softwares.
-- Copyright (C) 2018  Giovani Frigo

-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
--

CREATE TABLE IF NOT EXISTS DatabaseCryptographySetting(
	cryptographySettingID BIGINT NOT NULL AUTO_INCREMENT, 
	usedHashName LONGTEXT NOT NULL, 
	usedSalt LONGBLOB NOT NULL, 
	iterations INT NOT NULL, 
	length INT NOT NULL, 
	PRIMARY KEY (cryptographySettingID)) 
	ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseUser(
	userID BIGINT NOT NULL AUTO_INCREMENT, 
	username VARCHAR(3072) NOT NULL UNIQUE, 
	hashedPassword LONGBLOB NOT NULL, 
	email LONGTEXT NOT NULL, 
	displayName LONGTEXT NOT NULL, 
	recoverQuestion LONGTEXT NOT NULL, 
	recoverQuestionAnswer LONGTEXT NOT NULL, 
	selectedProfilePictureID BIGINT, 
	profilePictureCount BIGINT NOT NULL, 
	groupOwnershipCount BIGINT NOT NULL, 
	isAccountDisabled BIT NOT NULL, 
	haveSelectedProfilePicture BIT NOT NULL, 
	cryptographySettingID BIGINT NOT NULL, 
	PRIMARY KEY (userID), 
	FOREIGN KEY (cryptographySettingID) REFERENCES DatabaseCryptographySetting(cryptographySettingID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseUserBlock(
	userBlockID BIGINT NOT NULL AUTO_INCREMENT, 
	blockingUserID BIGINT NOT NULL, 
	blockedUserID BIGINT NOT NULL,
	PRIMARY KEY (userBlockID), 
	FOREIGN KEY (blockingUserID) REFERENCES DatabaseUser(userID), 
	FOREIGN KEY (blockedUserID) REFERENCES DatabaseUser(userID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseSession(
	sessionID BIGINT NOT NULL AUTO_INCREMENT, 
	sessionToken VARCHAR(3072) NOT NULL UNIQUE, 
	deviceToken VARCHAR(3072) NOT NULL, 
	userID BIGINT NOT NULL, 
	expiresAtMS BIGINT NOT NULL, 
	sessionCreatedAtMS BIGINT NOT NULL, 
	PRIMARY KEY (sessionID), 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseFile(
	fileID BIGINT NOT NULL AUTO_INCREMENT, 
	fileName LONGTEXT NOT NULL, 
	fileExtension LONGTEXT, 
	ownerUserID BIGINT NOT NULL, 
	filePartsCount BIGINT NOT NULL, 
	fileSizeInBytes BIGINT NOT NULL, 
	isFileComplete BIT NOT NULL, 
	PRIMARY KEY (fileID), 
	FOREIGN KEY (ownerUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseFilePart(
	filePartID BIGINT NOT NULL AUTO_INCREMENT, 
	fileID BIGINT NOT NULL, 
	filePartNumber BIGINT NOT NULL, 
	filePartBytes LONGBLOB NOT NULL, 
	filePartSize INT NOT NULL, 
	PRIMARY KEY (filePartID), 
	FOREIGN KEY (fileID) REFERENCES DatabaseFile(fileID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseFileRepository(
	fileRepositoryID BIGINT NOT NULL AUTO_INCREMENT, 
	fileRepositoryFileCount BIGINT NOT NULL, 
	PRIMARY KEY (fileRepositoryID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseFileRepositoryFile(
	fileRepositoryFileID BIGINT NOT NULL AUTO_INCREMENT, 
	fileRepositoryID BIGINT NOT NULL, 
	ownerUserID BIGINT NOT NULL, 
	fileID BIGINT NOT NULL, 
	fileRepositoryFileNumber BIGINT NOT NULL, 
	description LONGTEXT, 
	PRIMARY KEY (fileRepositoryFileID), 
	FOREIGN KEY (fileRepositoryID) REFERENCES DatabaseFileRepository(fileRepositoryID) ON DELETE CASCADE, 
	FOREIGN KEY (ownerUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (fileID) REFERENCES DatabaseFile(fileID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseChat(
	chatID BIGINT NOT NULL AUTO_INCREMENT, 
	messageCount BIGINT NOT NULL, 
	PRIMARY KEY (chatID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabasePrivateChat(
	privateChatID BIGINT NOT NULL AUTO_INCREMENT, 
	chatID BIGINT NOT NULL, 
	userID1 BIGINT NOT NULL, 
	userID2 BIGINT NOT NULL, 
	PRIMARY KEY (privateChatID), 
	FOREIGN KEY (chatID) REFERENCES DatabaseChat(chatID) ON DELETE CASCADE, 
	FOREIGN KEY (userID1) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (userID2) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	UNIQUE KEY usersUnique (userID1, userID2)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseChatMessage(
	chatMessageID BIGINT NOT NULL AUTO_INCREMENT, 
	chatID BIGINT NOT NULL, 
	ownerUserID BIGINT NOT NULL, 
	message LONGTEXT NOT NULL, 
	isQuote BIT NOT NULL, 
	quotedOwnerUserID BIGINT, 
	chatMessageNumber BIGINT NOT NULL, 
	PRIMARY KEY (chatMessageID), 
	FOREIGN KEY (chatID) REFERENCES DatabaseChat(chatID) ON DELETE CASCADE, 
	FOREIGN KEY (ownerUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE,
	FOREIGN KEY (quotedOwnerUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroup(
	groupID BIGINT NOT NULL AUTO_INCREMENT, 
	groupName LONGTEXT NOT NULL, 
	ownerUserID BIGINT NOT NULL, 
	groupAccess INT NOT NULL, 
	PRIMARY KEY (groupID), 
	FOREIGN KEY (ownerUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupUser(
	groupUserID BIGINT NOT NULL AUTO_INCREMENT, 
	groupID BIGINT NOT NULL, 
	userID BIGINT NOT NULL, 
	PRIMARY KEY (groupUserID), 
	FOREIGN KEY (groupID) REFERENCES DatabaseGroup(groupID) ON DELETE CASCADE, 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupUser (groupID, userID)) ENGINE=InnoDB;
				
CREATE TABLE IF NOT EXISTS DatabaseGroupChat(
	groupChatID BIGINT NOT NULL AUTO_INCREMENT, 
	groupID BIGINT NOT NULL, 
	chatID BIGINT NOT NULL, 
	chatName LONGTEXT NOT NULL,	
	PRIMARY KEY (groupChatID), 
	FOREIGN KEY (groupID) REFERENCES DatabaseGroup(groupID) ON DELETE CASCADE, 
	FOREIGN KEY (chatID) REFERENCES DatabaseChat(chatID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupChat (groupID, chatID)) ENGINE=InnoDB;
				
CREATE TABLE IF NOT EXISTS DatabaseGroupFileRepository(
	groupFileRepositoryID BIGINT NOT NULL AUTO_INCREMENT, 
	groupID BIGINT NOT NULL, 
	fileRepositoryID BIGINT NOT NULL, 
	groupFileRepositoryName LONGTEXT NOT NULL, 
	groupFileRepositoryFileCount BIGINT NOT NULL, 
	PRIMARY KEY (groupFileRepositoryID), 
	FOREIGN KEY (groupID) REFERENCES DatabaseGroup(groupID) ON DELETE CASCADE, 
	FOREIGN KEY (fileRepositoryID) REFERENCES DatabaseFileRepository(fileRepositoryID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupFileRepository (groupID, fileRepositoryID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupPermissionLevel(
	groupPermissionLevelID BIGINT NOT NULL AUTO_INCREMENT, 
	groupID BIGINT NOT NULL, 
	permissionName LONGTEXT NOT NULL, 
	removable BIT NOT NULL, 
	allowBanGroupUsers BIT NOT NULL, 
	allowKickGroupUsers BIT NOT NULL, 
	allowManagePermissionLevels BIT NOT NULL, 
	allowAccessAllGroupChats BIT NOT NULL, 
	allowAccessAllGroupFileRepositories BIT NOT NULL, 
	isGroupOwner BIT NOT NULL, 
	PRIMARY KEY (groupPermissionLevelID), 
	FOREIGN KEY (groupID) REFERENCES DatabaseGroup(groupID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupUserPermission(
	groupUserPermissionID BIGINT NOT NULL AUTO_INCREMENT, 
	groupUserID BIGINT NOT NULL, 
	groupPermissionLevelID BIGINT NOT NULL, 
	removable BIT NOT NULL, 
	PRIMARY KEY (groupUserPermissionID), 
	FOREIGN KEY (groupUserID) REFERENCES DatabaseGroupUser(groupUserID) ON DELETE CASCADE, 
	FOREIGN KEY (groupPermissionLevelID) REFERENCES DatabaseGroupPermissionLevel(groupPermissionLevelID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupUserPermission (groupUserID, groupPermissionLevelID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupFileRepositoryPermissionSetting(
	groupFileRepositoryPermissionSettingID BIGINT NOT NULL AUTO_INCREMENT, 
	groupFileRepositoryID BIGINT NOT NULL, 
	groupPermissionLevelID BIGINT NOT NULL, 
	PRIMARY KEY (groupFileRepositoryPermissionSettingID), 
	FOREIGN KEY (groupFileRepositoryID) REFERENCES DatabaseGroupFileRepository(groupFileRepositoryID) ON DELETE CASCADE, 
	FOREIGN KEY (groupPermissionLevelID) REFERENCES DatabaseGroupPermissionLevel(groupPermissionLevelID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupFileRepositoryPermissionSetting (groupFileRepositoryID, groupPermissionLevelID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupChatPermissionSetting(
	groupChatPermissionSettingID BIGINT NOT NULL AUTO_INCREMENT, 
	groupChatID BIGINT NOT NULL, 
	groupPermissionLevelID BIGINT NOT NULL, 
	PRIMARY KEY (groupChatPermissionSettingID), 
	FOREIGN KEY (groupChatID) REFERENCES DatabaseGroupChat(groupChatID) ON DELETE CASCADE, 
	FOREIGN KEY (groupPermissionLevelID) REFERENCES DatabaseGroupPermissionLevel(groupPermissionLevelID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupChatPermissionSetting (groupChatID, groupPermissionLevelID)) ENGINE=InnoDB;
				
CREATE TABLE IF NOT EXISTS DatabasePrivateChatFileRepository(
	privateChatFileRepositoryID BIGINT NOT NULL AUTO_INCREMENT, 
	privateChatID BIGINT NOT NULL, 
	fileRepositoryID BIGINT NOT NULL, 
	PRIMARY KEY (privateChatFileRepositoryID), 
	FOREIGN KEY (privateChatID) REFERENCES DatabasePrivateChat(privateChatID) ON DELETE CASCADE, 
	FOREIGN KEY (fileRepositoryID) REFERENCES DatabaseFileRepository(fileRepositoryID) ON DELETE CASCADE, 
	UNIQUE KEY uniquePrivateChatFileRepository (privateChatID, fileRepositoryID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupFileRepositoryFile(
	groupFileRepositoryFileID BIGINT NOT NULL AUTO_INCREMENT, 
	groupFileRepositoryID BIGINT NOT NULL, 
	fileRepositoryFileID BIGINT NOT NULL, 
	groupFileRepositoryFileNumber BIGINT NOT NULL, 
	PRIMARY KEY (groupFileRepositoryFileID), 
	FOREIGN KEY (groupFileRepositoryID) REFERENCES DatabaseGroupFileRepository(groupFileRepositoryID) ON DELETE CASCADE, 
	FOREIGN KEY (fileRepositoryFileID) REFERENCES DatabaseFileRepositoryFile(fileRepositoryFileID) ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS DatabaseUserFileRepository(
	userFileRepositoryID BIGINT NOT NULL AUTO_INCREMENT, 
	userID BIGINT NOT NULL, 
	fileRepositoryID BIGINT NOT NULL, 
	userFileRepositoryFileCount BIGINT NOT NULL, 
	PRIMARY KEY (userFileRepositoryID), 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (fileRepositoryID) REFERENCES DatabaseFileRepository(fileRepositoryID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueUserFileRepository (userID, fileRepositoryID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseUserFileRepositoryFile(
	userFileRepositoryFileID BIGINT NOT NULL AUTO_INCREMENT, 
	userFileRepositoryID BIGINT NOT NULL, 
	fileRepositoryFileID BIGINT NOT NULL, 
	userFileRepositoryFileNumber BIGINT NOT NULL, 
	PRIMARY KEY (userFileRepositoryFileID), 
	FOREIGN KEY (userFileRepositoryID) REFERENCES DatabaseUserFileRepository(userFileRepositoryID) ON DELETE CASCADE, 
	FOREIGN KEY (fileRepositoryFileID) REFERENCES DatabaseFileRepositoryFile(fileRepositoryFileID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseOnlineUser(
	onlineUserID BIGINT NOT NULL AUTO_INCREMENT, 
	userID BIGINT NOT NULL, 
	deviceToken VARCHAR(3072) NOT NULL UNIQUE, 
	userStatus INT NOT NULL, 
	customStatus LONGTEXT NOT NULL, 
	lastUpdatedMS BIGINT NOT NULL, 
	PRIMARY KEY (onlineUserID), 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseGroupInvite(
	groupInviteID BIGINT NOT NULL AUTO_INCREMENT, 
	invitedUserID BIGINT NOT NULL, 
	invitingUserID BIGINT NOT NULL, 
	groupID BIGINT NOT NULL, 
	PRIMARY KEY (groupInviteID), 
	FOREIGN KEY (invitedUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (invitingUserID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueGroupInvite (invitedUserID, invitingUserID, groupID)) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseProfilePicture(
	profilePictureID BIGINT NOT NULL AUTO_INCREMENT, 
	userID BIGINT NOT NULL, 
	fileID BIGINT NOT NULL, 
	profilePictureNumber BIGINT NOT NULL, 
	PRIMARY KEY (profilePictureID), 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (fileID) REFERENCES DatabaseFile(fileID) ON DELETE CASCADE) ENGINE=InnoDB;
	
CREATE TABLE IF NOT EXISTS DatabaseBannedGroupUser(
	bannedGroupUserID BIGINT NOT NULL AUTO_INCREMENT, 
	userID BIGINT NOT NULL, 
	groupID BIGINT NOT NULL, 
	PRIMARY KEY (bannedGroupUserID), 
	FOREIGN KEY (userID) REFERENCES DatabaseUser(userID) ON DELETE CASCADE, 
	FOREIGN KEY (groupID) REFERENCES DatabaseGroup(groupID) ON DELETE CASCADE, 
	UNIQUE KEY uniqueBannedGroupUser (userID, groupID)) ENGINE=InnoDB;
	
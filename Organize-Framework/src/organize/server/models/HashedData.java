/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

public class HashedData {
	private String usedHashName;
	private byte[] usedSalt;
	private byte[] hashedData;
	private int iterations;
	private int length;
	
	public HashedData(String usedHashName, byte[] usedSalt, byte[] hashedData, int iterations, int length) {
		this.usedHashName = usedHashName;
		this.usedSalt = usedSalt;
		this.hashedData = hashedData;
		this.iterations = iterations;
		this.length = length;
	}
	public String getUsedHashName() {
		return usedHashName;
	}
	public void setUsedHashName(String usedHashName) {
		this.usedHashName = usedHashName;
	}
	public byte[] getUsedSalt() {
		return usedSalt;
	}
	public void setUsedSalt(byte[] usedSalt) {
		this.usedSalt = usedSalt;
	}
	public byte[] getHashedData() {
		return hashedData;
	}
	public void setHashedData(byte[] hashedData) {
		this.hashedData = hashedData;
	}
	public int getIterations() {
		return iterations;
	}
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}

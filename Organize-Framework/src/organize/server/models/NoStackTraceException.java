/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

public class NoStackTraceException extends Exception {
	private static final long serialVersionUID = -2584803214987011000L;
	
	public NoStackTraceException() {
		
	}
	
	public NoStackTraceException(String exceptionMessage) {
		super(exceptionMessage);
	}

	@Override
	public Throwable fillInStackTrace() { //Disabling stack trace filling so we get no performance impact
		return this;
	}
}

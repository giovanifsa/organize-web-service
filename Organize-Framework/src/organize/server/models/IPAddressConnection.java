/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

import java.util.concurrent.ConcurrentLinkedQueue;

class ConnectionLog {
	private final long momentMS;
	private final long weight;
	
	public ConnectionLog(long momentMS, long weight) {
		this.momentMS = momentMS;
		this.weight = weight;
	}

	public long getMomentMS() {
		return momentMS;
	}

	public long getWeight() {
		return weight;
	}
}

public class IPAddressConnection {
	private final String ipAdress;
	private ConcurrentLinkedQueue<ConnectionLog> connectionList = new ConcurrentLinkedQueue<>();
	
	public IPAddressConnection(String IPAddress) {
		this.ipAdress = IPAddress;
	}
	
	public void addConnection(long momentMS, long weight) {
		connectionList.add(new ConnectionLog(momentMS, weight));
	}
	
	public boolean isConnectionAllowed(long lastValidTimeMS, long maximumWeight) {
		long weight = 0;
		
		for (ConnectionLog connection : connectionList) {
			if (connection.getMomentMS() >= lastValidTimeMS) {
				weight += connection.getWeight();
				
				if (weight > maximumWeight) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public void clearOld(long lastValidTimeMS) {
		for (ConnectionLog connection : connectionList) {
			if (connection.getMomentMS() < lastValidTimeMS) {
				connectionList.remove(connection);
			}
		}
	}
	
	public String getIPAddress() {
		return ipAdress;
	}
}

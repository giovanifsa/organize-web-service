/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

public class StaticData {
	public static final String ORGANIZE_FOLDER = "." + File.separator + "organize" + File.separator;
	public static final String CONFIGURATION_FILE = ORGANIZE_FOLDER + "configs" + File.separator + "server.cfg";
	public static final Charset STANDARD_CHARSET = StandardCharsets.UTF_8;
	
	public static long filterLowerValueOfLong(long value1, long value2) {
		if (value1 > value2) {
			return value2;
		}
		
		else if (value1 < value2) {
			return value1;
		}
		
		else {
			return value1;
		}
	}
	
	public static long filterHigherValueOfLong(long value1, long value2) {
		if (value1 > value2) {
			return value1;
		}
		
		else if (value1 < value2) {
			return value2;
		}
		
		else {
			return value2;
		}
	}
	
	public static String generateRandomString(int stringSize, SecureRandom secureRandom) {
		StringBuilder strBuilder = new StringBuilder();
		
		for (int i = 0; i < stringSize; i++) {
			strBuilder.append((char) (byte) secureRandom.nextInt() % Byte.MAX_VALUE);
		}
		
		return strBuilder.toString();
	}
}

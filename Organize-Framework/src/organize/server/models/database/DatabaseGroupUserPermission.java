/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupUserPermission {
	private long groupUserPermissionID;
	private long groupUserID;
	private long groupPermissionLevelID;
	private boolean removable;
	
	public DatabaseGroupUserPermission(long groupUserPermissionID, long groupUserID, long groupPermissionLevelID, boolean removable) {
		this.groupUserPermissionID = groupUserPermissionID;
		this.groupUserID = groupUserID;
		this.groupPermissionLevelID = groupPermissionLevelID;
		this.removable = removable;
	}

	public long getGroupUserPermissionID() {
		return groupUserPermissionID;
	}

	public void setGroupUserPermissionID(long groupUserPermissionID) {
		this.groupUserPermissionID = groupUserPermissionID;
	}

	public long getGroupUserID() {
		return groupUserID;
	}

	public void setGroupUserID(long groupUserID) {
		this.groupUserID = groupUserID;
	}

	public long getGroupPermissionLevelID() {
		return groupPermissionLevelID;
	}

	public void setGroupPermissionLevelID(long groupPermissionLevelID) {
		this.groupPermissionLevelID = groupPermissionLevelID;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}
}

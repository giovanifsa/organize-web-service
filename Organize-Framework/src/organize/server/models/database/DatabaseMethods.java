/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import organize.server.models.GenericErrorCode;

public interface DatabaseMethods {
	public DatabaseUser createUser(@NotNull String username, byte[] hashedPassword, @NotNull String email, @NotNull String displayName, @NotNull String recoverQuestion, @NotNull String recoverQuestionAnswer, long cryptographySettingID) throws DatabaseException, GenericErrorCode;
	public DatabaseUser getUserByUsername(String username) throws DatabaseException, GenericErrorCode;
	public DatabaseUser getUserByID(long userID) throws DatabaseException, GenericErrorCode;
	public void deleteUserByID(long userID) throws DatabaseException, GenericErrorCode;
	public void disableUserByID(long userID, boolean disabled) throws DatabaseException, GenericErrorCode;
	public DatabasePrivateChat createPrivateChat(long chatID, long userID1, long userID2) throws DatabaseException, GenericErrorCode;
	public DatabasePrivateChat getPrivateChat(long userID1, long userID2, boolean rotateUsers) throws DatabaseException, GenericErrorCode;
	public DatabasePrivateChat getPrivateChat(long privateChatID) throws DatabaseException, GenericErrorCode;
	public DatabaseProfilePicture createProfilePicture(long userID, long fileID) throws DatabaseException, GenericErrorCode;
	public DatabaseProfilePicture getProfilePictureByID(long profilePictureID) throws DatabaseException, GenericErrorCode;
	public DatabaseProfilePicture getProfilePictureByNumber(long userID, long profilePictureNumber) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseProfilePicture> getUserProfilePictures(long userID) throws DatabaseException, GenericErrorCode;
	public DatabaseUserFileRepository getUserFileRepositoryByUserID(long userID) throws DatabaseException, GenericErrorCode;
	public DatabaseUserFileRepository getUserFileRepositoryByID(long userFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public DatabaseUserFileRepository createUserFileRepository(long userID, long fileRepositoryID) throws DatabaseException, GenericErrorCode;
	public DatabasePrivateChatFileRepository createPrivateChatFileRepository(long privateChatID, long fileRepositoryID) throws DatabaseException, GenericErrorCode;
	public DatabasePrivateChatFileRepository getPrivateChatFileRepository(long privateChatFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabasePrivateChatFileRepository> getPrivateChatFileRepositories(long privateChatID) throws DatabaseException, GenericErrorCode;
	public DatabaseUser changeUserRecoverQuestion(long userID, @NotNull String newRecoverQuestion) throws DatabaseException, GenericErrorCode;
	public DatabaseUser changeUserRecoverAnswer(long userID, @NotNull String newRecoverQuestionAnswer) throws DatabaseException, GenericErrorCode;
	public DatabaseUser changeUserPassword(long userID, byte[] newHashedPassword, long newCryptographySettingID) throws DatabaseException, GenericErrorCode;
	public DatabaseUserFileRepositoryFile getUserFileRepositoryFile(long userFileRepositoryFileID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseUserFileRepositoryFile> getAllUserFileRepositoryFiles(long userFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseUserFileRepositoryFile> getUserFileRepositoryFilesInRange(long userRepositoryID, long startingFileNumber, long endingFileNumber) throws DatabaseException, GenericErrorCode;
	public DatabaseFileRepositoryFile getFileRepositoryFileFromPrivateChatFileRepository(long privateChatFileRepositoryID, long fileRepositoryFileID) throws DatabaseException, GenericErrorCode;
	
	public DatabaseOnlineUser getOnlineUserByID(long onlineUserID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseOnlineUser> getOnlineUserByUserID(long userID) throws DatabaseException, GenericErrorCode;
	public void deleteOnlineUser(long onlineUserID) throws DatabaseException, GenericErrorCode;
	public DatabaseOnlineUser createOnlineUser(long userID, @NotNull String deviceToken, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode;
	public DatabaseOnlineUser updateOnlineUser(long onlineUserID, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode;
	public DatabaseOnlineUser createOrUpdateOnlineUser(long userID, @NotNull String deviceToken, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseOnlineUser> getAllOnlineUsers() throws DatabaseException, GenericErrorCode;
	
	//Files
	public DatabaseFile createFile(long ownerUserID, @NotNull String filename, String fileExtension) throws DatabaseException, GenericErrorCode;
	public DatabaseFilePart createFilePart(long fileID, byte[] filePart) throws DatabaseException, GenericErrorCode;
	public DatabaseFilePart getFilePart(long fileID, long filePartNumber) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseFilePart> getAllFileParts(long fileID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseFilePart> getFilePartsInRange(long fileID, long startingPartNumber, long endingPartNumber) throws DatabaseException, GenericErrorCode;
	public DatabaseFile getFile(long fileID) throws DatabaseException, GenericErrorCode;
	public void deleteFile(long fileID) throws DatabaseException, GenericErrorCode;
	public DatabaseFile setFileComplete(long fileID, boolean isFileComplete) throws DatabaseException, GenericErrorCode;
	 
	//Groups
	public DatabaseGroup createGroup(@NotNull String groupName, long ownerUserID, int groupAccess) throws DatabaseException, GenericErrorCode;
	public DatabaseGroup getGroup(long groupID) throws DatabaseException, GenericErrorCode;
	public void deleteGroup(long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupUser getGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupUser getGroupUser(long groupUserID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupUser createGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode;
	public void deleteGroupUser(long groupUserID) throws DatabaseException, GenericErrorCode; 
	public DatabaseGroupChatPermissionSetting createGroupChatPermissionSetting(long groupChatID, long groupPermissionLevelID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupFileRepositoryPermissionSetting createGroupFileRepositoryPermissionSetting(long groupFileRepositoryID, long groupPermissionLevelID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupPermissionLevel createGroupPermissionLevel(long groupID, @NotNull String permissionName, boolean removable, boolean allowBanGroupUsers, 
			boolean allowKickGroupUsers, boolean allowManagePermissionLevels, boolean allowAccessAllGroupChats, boolean allowAccessAllGroupFileRepositories, 
			boolean isGroupOwner) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupChat createGroupChat(long groupID, long chatID, @NotNull String chatName) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupChat> getAllGroupChats(long groupID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupFileRepository> getAllGroupFileRepositories(long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupChat getGroupChat(long groupChatID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupChatPermissionSetting> getGroupChatPermissionSettings(long groupChatID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupFileRepositoryPermissionSetting> getGroupFileRepositoryPermissionSettings(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupUserPermission> getGroupUserPermissions(long groupUserID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupPermissionLevel> getGroupPermissionLevels(long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupPermissionLevel getGroupPermissionLevel(long groupPermissionLevelID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupUserPermission createGroupUserPermission(long groupUserID, long groupPermissionLevelID, boolean removable) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupFileRepository getGroupFileRepository(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupFileRepositoryFile> getAllGroupFileRepositoryFiles(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupFileRepositoryFile> getGroupFileRepositoryFilesInRange(long groupFileRepositoryID, long startingFileNumber, long endingFilenumber) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupFileRepositoryFile getGroupFileRepositoryFile(long groupFileRepositoryFileID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupInvite createGroupInvite(long groupID, long invitedUserID, long invitingUserID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseGroupInvite> getGroupInvitesForUser(long invitedUserID) throws DatabaseException, GenericErrorCode;
	public void deleteGroupInvite(long groupInviteID) throws DatabaseException, GenericErrorCode;
	public DatabaseGroupInvite getGroupInvite(long groupInviteID) throws DatabaseException, GenericErrorCode;
	public DatabaseBannedGroupUser createBannedGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseBannedGroupUser getBannedGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode;
	public DatabaseBannedGroupUser getBannedGroupUser(long bannedGroupUserID) throws DatabaseException, GenericErrorCode;
	public void deleteBannedGroupUser(long bannedGroupUserID) throws DatabaseException, GenericErrorCode;
	
	//Chats
	public DatabaseChat getChat(long chatID) throws DatabaseException, GenericErrorCode;
	public DatabaseChat createChat() throws DatabaseException, GenericErrorCode;
	public DatabaseChatMessage createChatMessage(long chatID, long ownerUserID, @NotNull String message, boolean isQuote, long quotedOwnerUserID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseChatMessage> getAllMessagesFromChat(long chatID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseChatMessage> getMessagesFromChatInRange(long chatID, long startingMessageNumber, long endingMessageNumber) throws DatabaseException, GenericErrorCode;

	//Sessions
	public DatabaseSession createSession(@NotNull String sessionToken, @NotNull String deviceToken, long userID, long expiresAtMS, long sessionCreatedAtMS) throws DatabaseException, GenericErrorCode;
	public void deleteSession(long sessionID) throws DatabaseException, GenericErrorCode;
	public DatabaseSession getSessionBySessionID(long sessionID) throws DatabaseException, GenericErrorCode;
	public void deleteSessionsByUserID(long userID) throws DatabaseException, GenericErrorCode;
	public DatabaseSession getSessionBySessionToken(@NotNull String sessionToken) throws DatabaseException, GenericErrorCode;
	public void deleteSessionBySessionToken(@NotNull String sessionToken) throws DatabaseException, GenericErrorCode;
	public DatabaseSession renewSession(long sessionID, long expiresAtMS) throws DatabaseException, GenericErrorCode;
	
	//Cryptography
	public DatabaseCryptographySetting createCryptographySetting(@NotNull String usedHashName, byte[] usedSalt, int iterations, int length) throws DatabaseException, GenericErrorCode;
	public DatabaseCryptographySetting getCryptographySetting(long cryptographySettingID) throws DatabaseException, GenericErrorCode;
	public void deleteCryptographySetting(long cryptographySettingID) throws DatabaseException, GenericErrorCode;
	
	//File repositories
	public DatabaseFileRepository createFileRepository() throws DatabaseException, GenericErrorCode;
	public DatabaseFileRepository getFileRepository(long fileRepositoryID) throws DatabaseException, GenericErrorCode;
	public DatabaseFileRepositoryFile createFileRepositoryFile(long fileRepositoryID, long fileID, long repositoryFileNumber, long ownerUserID, String description) throws DatabaseException, GenericErrorCode;
	public void deleteFileRepositoryFile(long fileRepositoryFileID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseFileRepositoryFile> getAllFileRepositoryFiles(long fileRepositoryID) throws DatabaseException, GenericErrorCode;
	public ArrayList<DatabaseFileRepositoryFile> getFileRepositoryFilesInRange(long fileRepositoryID, long startingFileNumber, long endingFileNumber) throws DatabaseException, GenericErrorCode;
	public DatabaseFileRepositoryFile getFileRepositoryFile(long fileRepositoryFileID) throws DatabaseException, GenericErrorCode;
}

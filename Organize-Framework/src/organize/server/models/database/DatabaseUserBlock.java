/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseUserBlock {
	private long userBlockID;
	private long blockingUserID;
	private long blockedUserID;
	
	public DatabaseUserBlock(long userBlockID, long blockingUserID, long blockedUserID) {
		this.userBlockID = userBlockID;
		this.blockingUserID = blockingUserID;
		this.blockedUserID = blockedUserID;
	}

	public long getUserBlockID() {
		return userBlockID;
	}

	public void setUserBlockID(long userBlockID) {
		this.userBlockID = userBlockID;
	}

	public long getBlockingUserID() {
		return blockingUserID;
	}

	public void setBlockingUserID(long blockingUserID) {
		this.blockingUserID = blockingUserID;
	}

	public long getBlockedUserID() {
		return blockedUserID;
	}

	public void setBlockedUserID(long blockedUserID) {
		this.blockedUserID = blockedUserID;
	}
}

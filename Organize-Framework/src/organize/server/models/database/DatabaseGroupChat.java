/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupChat {
	private long groupChatID;
	private long groupID;
	private long chatID;
	private String chatName;
	
	public DatabaseGroupChat(long groupChatID, long groupID, long chatID, String chatName) {
		this.groupChatID = groupChatID;
		this.groupID = groupID;
		this.chatID = chatID;
		this.chatName = chatName;
	}
	
	public long getGroupChatID() {
		return groupChatID;
	}
	public void setGroupChatID(long groupChatID) {
		this.groupChatID = groupChatID;
	}
	public long getGroupID() {
		return groupID;
	}
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	public long getChatID() {
		return chatID;
	}
	public void setChatID(long chatID) {
		this.chatID = chatID;
	}
	public String getChatName() {
		return chatName;
	}
	public void setChatName(String chatName) {
		this.chatName = chatName;
	}
	
	
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseFileRepositoryFile {
	private long fileRepositoryFileID;
	private long fileRepositoryID;
	private long ownerUserID;
	private long fileID;
	private long fileRepositoryFileNumber;
	private String description;
	
	public DatabaseFileRepositoryFile(long fileRepositoryFileID, long fileRepositoryID, long fileID, long fileRepositoryFileNumber, long ownerUserID, String description) {
		this.fileRepositoryFileID = fileRepositoryFileID;
		this.fileRepositoryID = fileRepositoryID;
		this.ownerUserID = ownerUserID;
		this.fileID = fileID;
		this.fileRepositoryFileNumber = fileRepositoryFileNumber;
		this.description = description;
	}
	public long getFileRepositoryID() {
		return fileRepositoryID;
	}
	public void setFileRepositoryID(long fileRepositoryID) {
		this.fileRepositoryID = fileRepositoryID;
	}
	public long getOwnerUserID() {
		return ownerUserID;
	}
	public void setOwnerUserID(long ownerUserID) {
		this.ownerUserID = ownerUserID;
	}
	public long getFileID() {
		return fileID;
	}
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getFileRepositoryFileID() {
		return fileRepositoryFileID;
	}
	public void setFileRepositoryFileID(long fileRepositoryFileID) {
		this.fileRepositoryFileID = fileRepositoryFileID;
	}
	public long getFileRepositoryFileNumber() {
		return fileRepositoryFileNumber;
	}
	public void setFileRepositoryFileNumber(long fileRepositoryFileNumber) {
		this.fileRepositoryFileNumber = fileRepositoryFileNumber;
	}
	
	
}

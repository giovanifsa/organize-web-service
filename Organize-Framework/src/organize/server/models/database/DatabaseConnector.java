/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

import organize.server.ServerMain;
import organize.server.modules.DatabaseManager;
import organize.server.modules.ServerModule;

public abstract class DatabaseConnector extends ServerModule implements DatabaseMethods {
	protected final DatabaseManager databaseManager;
	
	public DatabaseConnector(DatabaseManager databaseManager) {
		super(databaseManager.getServerMain());
		this.databaseManager = databaseManager;
	}
	
	@Override
	public ServerMain getServerMain() {
		return databaseManager.getServerMain();
	}
	
	public DatabaseManager getCreatorDatabaseManager() {
		return databaseManager;
	}
}

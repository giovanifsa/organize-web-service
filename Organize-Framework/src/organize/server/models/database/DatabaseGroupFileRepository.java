/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupFileRepository {
	private long groupFileRepositoryID;
	private long groupID;
	private long fileRepositoryID;
	private String groupRepositoryName;
	private long groupFileRepositoryFileCount;
	
	public DatabaseGroupFileRepository(long groupFileRepositoryID, long groupID, long fileRepositoryID,
			String groupRepositoryName, long groupFileRepositoryFileCount) {
		this.groupFileRepositoryID = groupFileRepositoryID;
		this.groupID = groupID;
		this.fileRepositoryID = fileRepositoryID;
		this.groupRepositoryName = groupRepositoryName;
		this.groupFileRepositoryFileCount = groupFileRepositoryFileCount;
	}
	
	public long getGroupFileRepositoryID() {
		return groupFileRepositoryID;
	}
	
	public void setGroupFileRepositoryID(long groupFileRepositoryID) {
		this.groupFileRepositoryID = groupFileRepositoryID;
	}
	
	public long getGroupID() {
		return groupID;
	}
	
	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	public long getFileRepositoryID() {
		return fileRepositoryID;
	}
	
	public void setFileRepositoryID(long fileRepositoryID) {
		this.fileRepositoryID = fileRepositoryID;
	}
	
	public String getGroupRepositoryName() {
		return groupRepositoryName;
	}
	
	public void setGroupRepositoryName(String groupRepositoryName) {
		this.groupRepositoryName = groupRepositoryName;
	}
	
	public long getGroupFileRepositoryFileCount() {
		return groupFileRepositoryFileCount;
	}
	
	public void setGroupFileRepositoryFileCount(long groupFileRepositoryFileCount) {
		this.groupFileRepositoryFileCount = groupFileRepositoryFileCount;
	}
}

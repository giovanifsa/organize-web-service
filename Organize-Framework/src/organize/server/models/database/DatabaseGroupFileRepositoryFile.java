/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupFileRepositoryFile {
	private long groupFileRepositoryFileID;
	private long groupFileRepositoryID;
	private long fileRepositoryFileID;
	private long groupFileRepositoryFileNumber;
	
	public DatabaseGroupFileRepositoryFile(long groupFileRepositoryFileID, long groupFileRepositoryID, long fileRepositoryFileID, long groupFileRepositoryFileNumber) {
		this.groupFileRepositoryFileID = groupFileRepositoryFileID;
		this.fileRepositoryFileID = fileRepositoryFileID;
		this.groupFileRepositoryID = groupFileRepositoryID;
		this.groupFileRepositoryFileNumber = groupFileRepositoryFileNumber;
	}

	public long getGroupFileRepositoryFileID() {
		return groupFileRepositoryFileID;
	}

	public void setGroupFileRepositoryFileID(long groupFileRepositoryFileID) {
		this.groupFileRepositoryFileID = groupFileRepositoryFileID;
	}

	public long getFileRepositoryFileID() {
		return fileRepositoryFileID;
	}

	public void setFileRepositoryFileID(long fileRepositoryFileID) {
		this.fileRepositoryFileID = fileRepositoryFileID;
	}

	public long getGroupFileRepositoryID() {
		return groupFileRepositoryID;
	}

	public void setGroupFileRepositoryID(long groupFileRepositoryID) {
		this.groupFileRepositoryID = groupFileRepositoryID;
	}

	public long getGroupFileRepositoryFileNumber() {
		return groupFileRepositoryFileNumber;
	}

	public void setGroupFileRepositoryFileNumber(long groupFileRepositoryFileNumber) {
		this.groupFileRepositoryFileNumber = groupFileRepositoryFileNumber;
	}
	
	
}

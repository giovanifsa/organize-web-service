/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupFileRepositoryPermissionSetting {
	private long groupFileRepositoryPermissionSettingID;
	private long groupFileRepositoryID;
	private long groupPermissionLevelID;
	public DatabaseGroupFileRepositoryPermissionSetting(long groupFileRepositoryPermissionSettingID,
			long groupFileRepositoryID, long groupPermissionLevelID) {
		this.groupFileRepositoryPermissionSettingID = groupFileRepositoryPermissionSettingID;
		this.groupFileRepositoryID = groupFileRepositoryID;
		this.groupPermissionLevelID = groupPermissionLevelID;
	}
	public long getGroupFileRepositoryPermissionSettingID() {
		return groupFileRepositoryPermissionSettingID;
	}
	
	public void setGroupFileRepositoryPermissionSettingID(long groupFileRepositoryPermissionSettingID) {
		this.groupFileRepositoryPermissionSettingID = groupFileRepositoryPermissionSettingID;
	}
	
	public long getGroupFileRepositoryID() {
		return groupFileRepositoryID;
	}
	
	public void setGroupFileRepositoryID(long groupFileRepositoryID) {
		this.groupFileRepositoryID = groupFileRepositoryID;
	}
	
	public long getGroupPermissionLevelID() {
		return groupPermissionLevelID;
	}
	
	public void setGroupPermissionLevelID(long groupPermissionLevelID) {
		this.groupPermissionLevelID = groupPermissionLevelID;
	}
	
	
}

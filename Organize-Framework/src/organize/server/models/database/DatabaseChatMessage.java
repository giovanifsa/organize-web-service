/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseChatMessage {
	private long chatMessageID;
	private long chatID;
	private long ownerUserID;
	private String message;
	private boolean isQuote;
	private long quotedOwnerUserID;
	private long chatMessageNumber;
	
	public DatabaseChatMessage(long chatMessageID, long chatID, long ownerUserID, String message, boolean isQuote,
			long quotedOwnerUserID, long chatMessageNumber) {
		this.chatMessageID = chatMessageID;
		this.chatID = chatID;
		this.ownerUserID = ownerUserID;
		this.message = message;
		this.isQuote = isQuote;
		this.quotedOwnerUserID = quotedOwnerUserID;
		this.chatMessageNumber = chatMessageNumber;
	}

	public long getChatMessageID() {
		return chatMessageID;
	}

	public void setChatMessageID(long chatMessageID) {
		this.chatMessageID = chatMessageID;
	}

	public long getChatID() {
		return chatID;
	}

	public void setChatID(long chatID) {
		this.chatID = chatID;
	}

	public long getOwnerUserID() {
		return ownerUserID;
	}

	public void setOwnerUserID(long ownerUserID) {
		this.ownerUserID = ownerUserID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isQuote() {
		return isQuote;
	}

	public void setQuote(boolean isQuote) {
		this.isQuote = isQuote;
	}

	public long getQuotedOwnerUserID() {
		return quotedOwnerUserID;
	}

	public void setQuotedOwnerUserID(long quotedOwnerUserID) {
		this.quotedOwnerUserID = quotedOwnerUserID;
	}

	public long getChatMessageNumber() {
		return chatMessageNumber;
	}

	public void setChatMessageNumber(long chatMessageNumber) {
		this.chatMessageNumber = chatMessageNumber;
	}
	
	
}

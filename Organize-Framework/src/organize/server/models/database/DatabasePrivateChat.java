/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabasePrivateChat {
	private long privateChatID;
	private long chatID;
	private long userID1;
	private long userID2;
	
	public DatabasePrivateChat(long privateChatID, long chatID, long userID1, long userID2) {
		this.privateChatID = privateChatID;
		this.chatID = chatID;
		this.userID1 = userID1;
		this.userID2 = userID2;
	}
	
	public long getPrivateChatID() {
		return privateChatID;
	}
	
	public void setPrivateChatID(long privateChatID) {
		this.privateChatID = privateChatID;
	}
	
	public long getChatID() {
		return chatID;
	}
	
	public void setChatID(long chatID) {
		this.chatID = chatID;
	}
	
	public long getUserID1() {
		return userID1;
	}
	
	public void setUserID1(long userID1) {
		this.userID1 = userID1;
	}
	
	public long getUserID2() {
		return userID2;
	}
	
	public void setUserID2(long userID2) {
		this.userID2 = userID2;
	}
}

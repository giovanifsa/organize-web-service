/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseOnlineUser {
	public static final int
		STATUS_ONLINE = 1,
		STATUS_OFFLINE = 0,
		STATUS_AWAY = 2,
		SLEEPING = 4,
		OCCUPIED = 3,
		CUSTOM = 5;
	
	private long onlineUserID;
	private int userStatus;
	private String customStatus;
	private String deviceToken;
	private long userID;
	private long lastUpdatedMS = 0;
	
	public DatabaseOnlineUser(long onlineUserID, long userID, String deviceToken, int userStatus, String customStatus, long lastUpdatedMS) {
		this.onlineUserID = onlineUserID;
		this.userID = userID;
		this.deviceToken = deviceToken;
		this.userStatus = userStatus;
		this.customStatus = customStatus;
		this.lastUpdatedMS = lastUpdatedMS;
	}
	
	public DatabaseOnlineUser(long userID, int userStatus, String customStatus) {
		this.userID = userID;
		this.userStatus = userStatus;
		this.customStatus = customStatus;
	}
	
	public int getUserStatus() {
		return userStatus;
	}
	
	public String getCustomStatus() {
		return customStatus;
	}
	
	public DatabaseOnlineUser removeSensitiveData() {
		return new DatabaseOnlineUser(userID, userStatus, customStatus);
	}
	
	public String getDeviceToken() {
		return deviceToken;
	}
	
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public long getUserID() {
		return userID;
	}
	
	public void setUserID(long userID) {
		this.userID = userID;
	}
	
	public long getLastUpdatedMS() {
		return lastUpdatedMS;
	}
	
	public void setLastUpdatedMS(long lastUpdatedMS) {
		this.lastUpdatedMS = lastUpdatedMS;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public void setCustomStatus(String customStatus) {
		this.customStatus = customStatus;
	}

	public long getOnlineUserID() {
		return onlineUserID;
	}

	public void setOnlineUserID(long onlineUserID) {
		this.onlineUserID = onlineUserID;
	}
}

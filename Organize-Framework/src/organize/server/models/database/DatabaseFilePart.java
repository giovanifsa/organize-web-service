/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseFilePart {
	private long filePartID;
	private long fileID;
	private long filePartNumber;
	private byte filePartBytes[];
	private int filePartSize;
	
	public DatabaseFilePart(long filePartID, long fileID, long filePartNumber, byte filePartBytes[], int filePartSize) {
		this.filePartID = filePartID;
		this.fileID = fileID;
		this.filePartNumber = filePartNumber;
		this.filePartBytes = filePartBytes;
		this.filePartSize = filePartSize;
	}
	
	public long getFileID() {
		return fileID;
	}
	
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	
	public long getFilePartNumber() {
		return filePartNumber;
	}
	
	public void setFilePartNumber(long filePartNumber) {
		this.filePartNumber = filePartNumber;
	}
	
	public byte[] getFilePartBytes() {
		return filePartBytes;
	}
	
	public void setFilePartBytes(byte[] filePartBytes) {
		this.filePartBytes = filePartBytes;
	}
	
	public long getFilePartSize() {
		return filePartSize;
	}
	
	public void setFilePartSize(int filePartSize) {
		this.filePartSize = filePartSize;
	}

	public long getFilePartID() {
		return filePartID;
	}

	public void setFilePartID(long filePartID) {
		this.filePartID = filePartID;
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseUserFileRepositoryFile {
	private long userFileRepositoryFileID;
	private long userFileRepositoryID;
	private long fileRepositoryFileID;
	private long userFileRepositoryFileNumber;
	
	public DatabaseUserFileRepositoryFile(long userFileRepositoryFileID, long userFileRepositoryID,
			long fileRepositoryFileID, long userFileRepositoryFileNumber) {
		this.userFileRepositoryFileID = userFileRepositoryFileID;
		this.userFileRepositoryID = userFileRepositoryID;
		this.fileRepositoryFileID = fileRepositoryFileID;
		this.userFileRepositoryFileNumber = userFileRepositoryFileNumber;
	}

	public long getUserFileRepositoryFileID() {
		return userFileRepositoryFileID;
	}

	public void setUserFileRepositoryFileID(long userFileRepositoryFileID) {
		this.userFileRepositoryFileID = userFileRepositoryFileID;
	}

	public long getUserFileRepositoryID() {
		return userFileRepositoryID;
	}

	public void setUserFileRepositoryID(long userFileRepositoryID) {
		this.userFileRepositoryID = userFileRepositoryID;
	}

	public long getFileRepositoryFileID() {
		return fileRepositoryFileID;
	}

	public void setFileRepositoryFileID(long fileRepositoryFileID) {
		this.fileRepositoryFileID = fileRepositoryFileID;
	}

	public long getUserFileRepositoryFileNumber() {
		return userFileRepositoryFileNumber;
	}

	public void setUserFileRepositoryFileNumber(long userFileRepositoryFileNumber) {
		this.userFileRepositoryFileNumber = userFileRepositoryFileNumber;
	}
	
	
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroup {
	public static final int 
		ACCESS_OWNERINVITE = 0,
		ACCESS_PERMISSIONSINVITE = 1,
		ACCESS_USERINVITE = 2,
		ACCESS_PUBLIC = 3;
	
	private long groupID;
	private String groupName;
	private long ownerUserID;
	private int groupAccess;
	
	public DatabaseGroup(long groupID, String groupName, long ownerUserID, int groupAccess) {
		this.groupID = groupID;
		this.groupName = groupName;
		this.ownerUserID = ownerUserID;
		this.groupAccess = groupAccess;
	}

	public long getGroupID() {
		return groupID;
	}

	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public long getOwnerUserID() {
		return ownerUserID;
	}

	public void setOwnerUserID(long ownerUserID) {
		this.ownerUserID = ownerUserID;
	}

	public int getGroupAccess() {
		return groupAccess;
	}

	public void setGroupAccess(int groupAccess) {
		this.groupAccess = groupAccess;
	}
	
	
}

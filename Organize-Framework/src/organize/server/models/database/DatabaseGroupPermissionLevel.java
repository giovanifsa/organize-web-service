/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupPermissionLevel {
	private long groupPermissionLevelID;
	private long groupID;
	private String permissionName;
	private boolean removable;
	
	private boolean allowBanGroupUsers;
	private boolean allowKickGroupUsers;
	private boolean allowManagePermissionLevels;
	private boolean allowAccessAllGroupChats;
	private boolean allowAccessAllGroupFileRepositories;
	private boolean isGroupOwner;
	
	public DatabaseGroupPermissionLevel(long groupPermissionLevelID, long groupID, String permissionName,
			boolean removable, boolean allowBanGroupUsers, boolean allowKickGroupUsers,
			boolean allowManagePermissionLevels, boolean allowAccessAllGroupChats,
			boolean allowAccessAllGroupFileRepositories, boolean isGroupOwner) {
		this.groupPermissionLevelID = groupPermissionLevelID;
		this.groupID = groupID;
		this.permissionName = permissionName;
		this.removable = removable;
		this.allowBanGroupUsers = allowBanGroupUsers;
		this.allowKickGroupUsers = allowKickGroupUsers;
		this.allowManagePermissionLevels = allowManagePermissionLevels;
		this.allowAccessAllGroupChats = allowAccessAllGroupChats;
		this.allowAccessAllGroupFileRepositories = allowAccessAllGroupFileRepositories;
		this.isGroupOwner = isGroupOwner;
	}

	public long getGroupPermissionLevelID() {
		return groupPermissionLevelID;
	}

	public void setGroupPermissionLevelID(long groupPermissionLevelID) {
		this.groupPermissionLevelID = groupPermissionLevelID;
	}

	public long getGroupID() {
		return groupID;
	}

	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public boolean isAllowBanGroupUsers() {
		return allowBanGroupUsers;
	}

	public void setAllowBanGroupUsers(boolean allowBanGroupUsers) {
		this.allowBanGroupUsers = allowBanGroupUsers;
	}

	public boolean isAllowKickGroupUsers() {
		return allowKickGroupUsers;
	}

	public void setAllowKickGroupUsers(boolean allowKickGroupUsers) {
		this.allowKickGroupUsers = allowKickGroupUsers;
	}

	public boolean isAllowManagePermissionLevels() {
		return allowManagePermissionLevels;
	}

	public void setAllowManagePermissionLevels(boolean allowManagePermissionLevels) {
		this.allowManagePermissionLevels = allowManagePermissionLevels;
	}

	public boolean isAllowAccessAllGroupChats() {
		return allowAccessAllGroupChats;
	}

	public void setAllowAccessAllGroupChats(boolean allowAccessAllGroupChats) {
		this.allowAccessAllGroupChats = allowAccessAllGroupChats;
	}

	public boolean isAllowAccessAllGroupFileRepositories() {
		return allowAccessAllGroupFileRepositories;
	}

	public void setAllowAccessAllGroupFileRepositories(boolean allowAccessAllGroupFileRepositories) {
		this.allowAccessAllGroupFileRepositories = allowAccessAllGroupFileRepositories;
	}

	public boolean isGroupOwner() {
		return isGroupOwner;
	}

	public void setGroupOwner(boolean isGroupOwner) {
		this.isGroupOwner = isGroupOwner;
	}
	
	
}

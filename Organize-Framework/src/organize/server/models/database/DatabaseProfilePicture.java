/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseProfilePicture {
	private long profilePictureID;
	private long userID;
	private long fileID;
	private long profilePictureNumber;
	
	public DatabaseProfilePicture(long profilePictureID, long userID, long fileID, long profilePictureNumber) {
		this.profilePictureID = profilePictureID;
		this.userID = userID;
		this.fileID = fileID;
		this.profilePictureNumber = profilePictureNumber;
	}
	
	public long getProfilePictureID() {
		return profilePictureID;
	}
	
	public void setProfilePictureID(long profilePictureID) {
		this.profilePictureID = profilePictureID;
	}
	
	public long getUserID() {
		return userID;
	}
	
	public void setUserID(long userID) {
		this.userID = userID;
	}
	
	public long getFileID() {
		return fileID;
	}
	
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	
	public long getProfilePictureNumber() {
		return profilePictureNumber;
	}
	
	public void setProfilePictureNumber(long profilePictureNumber) {
		this.profilePictureNumber = profilePictureNumber;
	}
}

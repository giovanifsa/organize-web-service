/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseUserFileRepository {
	private long userFileRepositoryID;
	private long userID;
	private long fileRepositoryID;
	private long fileRepositoryFileCount;
	
	public DatabaseUserFileRepository(long userFileRepositoryID, long userID, long fileRepositoryID, long fileRepositoryFileCount) {
		this.userFileRepositoryID = userFileRepositoryID;
		this.userID = userID;
		this.fileRepositoryID = fileRepositoryID;
		this.fileRepositoryFileCount = fileRepositoryFileCount;
	}

	public long getUserFileRepositoryID() {
		return userFileRepositoryID;
	}

	public void setUserFileRepositoryID(long userFileRepositoryID) {
		this.userFileRepositoryID = userFileRepositoryID;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public long getFileRepositoryID() {
		return fileRepositoryID;
	}

	public void setFileRepositoryID(long fileRepositoryID) {
		this.fileRepositoryID = fileRepositoryID;
	}

	public long getFileRepositoryFileCount() {
		return fileRepositoryFileCount;
	}

	public void setFileRepositoryFileCount(long fileRepositoryFileCount) {
		this.fileRepositoryFileCount = fileRepositoryFileCount;
	}
	
	
}

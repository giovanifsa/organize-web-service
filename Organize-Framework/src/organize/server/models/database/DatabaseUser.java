/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseUser {
	private long userID;
	private String username;
	private byte[] hashedPassword;
	private String email;
	private String displayName;
	private String recoverQuestion;
	private String recoverQuestionAnswer;
	private long selectedProfilePictureID;
	private long profilePictureCount;
	private long groupOwnershipCount;
	private boolean isAccountDisabled;
	private boolean haveSelectedProfilePicture;
	private long cryptographySettingID;
	
	public DatabaseUser(long userID, String username, byte[] hashedPassword, String email, String displayName, String recoverQuestion, String recoverQuestionAnswer, 
			long selectedProfilePictureID, long profilePictureCount, long groupOwnershipCount, boolean isAccountDisabled, boolean haveSelectedProfilePicture, long cryptographySettingID) {
		
		this.userID = userID;
		this.username = username;
		this.hashedPassword = hashedPassword;
		this.email = email;
		this.displayName = displayName;
		this.recoverQuestion = recoverQuestion;
		this.selectedProfilePictureID = selectedProfilePictureID;
		this.profilePictureCount = profilePictureCount;
		this.isAccountDisabled = isAccountDisabled;
		this.haveSelectedProfilePicture = haveSelectedProfilePicture;
		this.cryptographySettingID = cryptographySettingID;
	}
	
	public DatabaseUser removeSensitiveData() {
		return new DatabaseUser(userID, null, null, null, displayName, null, null, selectedProfilePictureID, profilePictureCount, 0, isAccountDisabled, haveSelectedProfilePicture, 0);
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(byte[] hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getRecoverQuestion() {
		return recoverQuestion;
	}

	public void setRecoverQuestion(String recoverQuestion) {
		this.recoverQuestion = recoverQuestion;
	}

	public long getProfilePictureCount() {
		return profilePictureCount;
	}

	public void setProfilePictureCount(long profilePictureCount) {
		this.profilePictureCount = profilePictureCount;
	}

	public boolean isAccountDisabled() {
		return isAccountDisabled;
	}

	public void setAccountDisabled(boolean isAccountDisabled) {
		this.isAccountDisabled = isAccountDisabled;
	}

	public String getRecoverQuestionAnswer() {
		return recoverQuestionAnswer;
	}

	public void setRecoverQuestionAnswer(String recoverQuestionAnswer) {
		this.recoverQuestionAnswer = recoverQuestionAnswer;
	}

	public long getSelectedProfilePictureID() {
		return selectedProfilePictureID;
	}

	public void setSelectedProfilePictureID(long selectedProfilePictureID) {
		this.selectedProfilePictureID = selectedProfilePictureID;
	}

	public long getGroupOwnershipCount() {
		return groupOwnershipCount;
	}

	public void setGroupOwnershipCount(long groupOwnershipCount) {
		this.groupOwnershipCount = groupOwnershipCount;
	}

	public boolean isHaveSelectedProfilePicture() {
		return haveSelectedProfilePicture;
	}

	public void setHaveSelectedProfilePicture(boolean haveSelectedProfilePicture) {
		this.haveSelectedProfilePicture = haveSelectedProfilePicture;
	}

	public long getCryptographySettingID() {
		return cryptographySettingID;
	}

	public void setCryptographySettingID(long cryptographySettingID) {
		this.cryptographySettingID = cryptographySettingID;
	}
	
	
}

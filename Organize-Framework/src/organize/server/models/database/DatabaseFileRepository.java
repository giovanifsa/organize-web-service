/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseFileRepository {
	private long fileRepositoryID;
	private long repositoryFileCount;

	public DatabaseFileRepository(long fileRepositoryID, long repositoryFileCount) {
		this.fileRepositoryID = fileRepositoryID;
		this.repositoryFileCount = repositoryFileCount;
	}

	public long getFileRepositoryID() {
		return fileRepositoryID;
	}

	public void setFileRepositoryID(long fileRepositoryID) {
		this.fileRepositoryID = fileRepositoryID;
	}

	public long getRepositoryFileCount() {
		return repositoryFileCount;
	}

	public void setRepositoryFileCount(long repositoryFileCount) {
		this.repositoryFileCount = repositoryFileCount;
	}
}

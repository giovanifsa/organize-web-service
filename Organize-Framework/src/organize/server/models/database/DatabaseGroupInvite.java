/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseGroupInvite {
	private long groupInviteID;
	private long invitedUserID;
	private long invitingUserID;
	private long groupID;
	
	public DatabaseGroupInvite(long groupInviteID, long invitedUserID, long invitingUserID, long groupID) {
		this.groupInviteID = groupInviteID;
		this.invitedUserID = invitedUserID;
		this.invitingUserID = invitingUserID;
		this.groupID = groupID;
	}

	public long getGroupInviteID() {
		return groupInviteID;
	}

	public void setGroupInviteID(long groupInviteID) {
		this.groupInviteID = groupInviteID;
	}

	public long getInvitedUserID() {
		return invitedUserID;
	}

	public void setInvitedUserID(long invitedUserID) {
		this.invitedUserID = invitedUserID;
	}

	public long getInvitingUserID() {
		return invitingUserID;
	}

	public void setInvitingUserID(long invitingUserID) {
		this.invitingUserID = invitingUserID;
	}

	public long getGroupID() {
		return groupID;
	}

	public void setGroupID(long groupID) {
		this.groupID = groupID;
	}
	
	
}

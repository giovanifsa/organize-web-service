/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseFile {
	private long fileID;
	private String fileName;
	private String fileExtension;
	private long ownerUserID;
	private long filePartsCount;
	private long fileSizeInBytes;
	private boolean isFileComplete;
	
	public DatabaseFile(long fileID, String fileName, String fileExtension, long ownerUserID, long filePartsCount, long fileSizeInBytes, boolean isFileComplete) {
		this.fileID = fileID;
		this.fileName = fileName;
		this.fileExtension = fileExtension;
		this.ownerUserID = ownerUserID;
		this.filePartsCount = filePartsCount;
		this.fileSizeInBytes = fileSizeInBytes;
		this.isFileComplete = isFileComplete;
	}
	
	public long getFileID() {
		return fileID;
	}
	
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
	
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	
	public long getOwnerUserID() {
		return ownerUserID;
	}
	
	public void setOwnerUserID(long ownerUserID) {
		this.ownerUserID = ownerUserID;
	}
	
	public long getFilePartsCount() {
		return filePartsCount;
	}
	
	public void setFilePartsCount(long filePartsCount) {
		this.filePartsCount = filePartsCount;
	}
	
	public long getFileSizeInBytes() {
		return fileSizeInBytes;
	}
	
	public void setFileSizeInBytes(long fileSizeInBytes) {
		this.fileSizeInBytes = fileSizeInBytes;
	}
	
	public boolean getIsFileComplete() {
		return isFileComplete;
	}
}

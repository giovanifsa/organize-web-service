/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.database;

public class DatabaseSession {
	private long sessionID;
	private String sessionToken;
	private String deviceToken;
	private long userID;
	private long expiresAtMS;
	private long sessionCreatedAtMS;
	
	public DatabaseSession(long sessionID, String sessionToken, String deviceToken, long userID, long expiresAtMS, long sessionCreatedAtMS) {
		this.sessionID = sessionID;
		this.sessionToken = sessionToken;
		this.deviceToken = deviceToken;
		this.userID = userID;
		this.expiresAtMS = expiresAtMS;
		this.sessionCreatedAtMS = sessionCreatedAtMS;
	}

	public String getDeviceToken() {
		return deviceToken;
	}
	
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public long getSessionID() {
		return sessionID;
	}
	
	public void setSessionID(long sessionID) {
		this.sessionID = sessionID;
	}
	
	public long getExpiresAt() {
		return expiresAtMS;
	}
	
	public void setExpiresAt(long expiresAtMS) {
		this.expiresAtMS = expiresAtMS;
	}
	
	public long getUserID() {
		return userID;
	}
	
	public void setUserID(long userID) {
		this.userID = userID;
	}
	
	public long getSessionCreatedAtMS() {
		return sessionCreatedAtMS;
	}
	
	public void setSessionCreatedAtMS(long sessionCreatedAtMS) {
		this.sessionCreatedAtMS = sessionCreatedAtMS;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
}
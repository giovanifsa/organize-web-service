/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Cryptographer {
	private final SecretKey keySpec;
	private final Cipher cipher;
	private final String keyType;
	private final String blockCipherMode;
	private final String padding;
	private final byte[] key;
	private final byte[] ivSpec;
	
	public Cryptographer(byte[] key, String keyType, String blockCipherMode, byte[] ivSpec, String padding) throws GenericErrorCode {
		this.key = key;
		this.keyType = keyType;
		this.blockCipherMode = blockCipherMode;
		this.padding = padding;
		this.ivSpec = ivSpec;
		
		keySpec = new SecretKeySpec(key, 0, key.length, keyType);
		
		try {
			cipher = Cipher.getInstance(keyType + "/" + blockCipherMode + "/" + padding);
			
			if (blockCipherMode.toLowerCase().equals("cbc")) {
				cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(ivSpec));
			}
			
			else {
				cipher.init(Cipher.DECRYPT_MODE, keySpec);
			}
			
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchPaddingException ex) {
			throw new GenericErrorCode(GenericErrorCode.BAD_CRYPTOGRAPHY_SETTINGS);
		}
	}
	
	public byte[] decryptWithKey(byte[] encryptedData) throws GenericErrorCode {
		try {
			if (blockCipherMode.toLowerCase().equals("cbc")) {
				cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(ivSpec));
			}
			
			else {
				cipher.init(Cipher.DECRYPT_MODE, keySpec);
			}
			
			return cipher.doFinal(encryptedData);
		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new GenericErrorCode(GenericErrorCode.DECRYPTION_FAILED);
		}
	}
	
	public byte[] encryptWithKey(byte[] decryptedData) throws GenericErrorCode {
		try {
			if (blockCipherMode.toLowerCase().equals("cbc")) {
				cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(ivSpec));
			}
			
			else {
				cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			}
			
			return cipher.doFinal(decryptedData);
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			throw new GenericErrorCode(GenericErrorCode.ENCRYPTION_FAILED);
		}
		
	}
}

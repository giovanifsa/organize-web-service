/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models;

public class GenericErrorCode extends NoStackTraceException {
	private static final long serialVersionUID = -2143830274198217254L;
	
	public static final int 
		OK = 0,
	
		SERVER_ERROR = 1,
		SERVICE_NOT_AVAILABLE = 2,
		SERVER_OVERLOAD = 3,
		
		UNKNOWN_USER = 4,
		UNKNOWN_FILE = 5,
		UNKNOWN_CHAT = 6,
		UNKNOWN_CHAT_MESSAGE = 7,
		UNKNOWN_FILE_PART = 8,
		UNKNOWN_FILE_ACCESS = 9,
		UNKNOWN_GROUP = 10,
		UNKNOWN_FILE_REPOSITORY = 11,
		UNKNOWN_REPOSITORY_FILE = 12,
		UNKNOWN_SESSION = 13,
		UNKNOWN_GROUP_USER = 14,
		UNKNOWN_USER_FILE_REPOSITORY_FILE = 15,
		UNKNOWN_GROUP_INVITE = 16,
		UNKNOWN_CRYPTOGRAPHY_SETTING = 17,
		UNKNOWN_ONLINE_USER = 18,
		UNKNOWN_PRIVATE_CHAT = 19,
		UNKNOWN_USER_FILE_REPOSITORY = 20,
		UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY = 21,
		UNKNOWN_GROUP_CHAT = 22,
		UNKNOWN_GROUP_FILE_REPOSITORY = 23,
		UNKNOWN_GROUP_FILE_REPOSITORY_FILE = 24,
		UNKNOWN_RESPONSE_PUBLIC_KEY = 25,
		UNKNOWN_FILE_REPOSITORY_FILE = 26,
		UNKNOWN_PROFILE_PICTURE = 27,
		UNKNOWN_GROUP_PERMISSION_LEVEL = 28,
		UNKNOWN_BANNED_GROUP_USER = 29,
		UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY_FILE = 30,
		
		USER_ALREADY_EXISTS = 31,
		GROUP_USER_ALREADY_EXISTS = 32,
		USER_ALREADY_INVITED_TO_GROUP = 33,
		SESSION_ALREADY_EXISTS = 34,
		PRIVATE_CHAT_ALREADY_EXISTS = 35,
		GROUP_CHAT_PERMISSION_SETTING_ALREADY_EXISTS = 36,
		GROUP_FILE_REPOSITORY_PERMISSION_SETTING_ALREADY_EXISTS = 37,
		ONLINE_USER_ALREADY_EXISTS = 38,
		BANNED_GROUP_USER_ALREADY_EXISTS = 39,
		
		USER_GROUP_CREATION_LIMIT_REACHED = 40,
		USER_GROUP_ENTER_LIMIT_REACHED = 41,
		
		PARAMETER_LIMIT_EXCEEDED = 42,
		RANGE_LIMIT_EXCEEDED = 43,
		INVALID_PARAMETERS = 44,
		PARAMETER_MINIMUM_NOT_REACHED = 45,
		DEVICE_LIMIT_EXCEEDED = 46,
		API_TRANSFER_LIMIT_EXCEEDED = 47,
		IPADDRESS_MAX_CONNECTIONS_EXCEEDED = 48,
		
		API_TRANSFER_FAILED = 49,
		DECRYPTION_FAILED = 50,
		ENCRYPTION_FAILED = 51,
		API_MALFORMED_REQUEST = 52,
		BAD_CRYPTOGRAPHY_SETTINGS = 53,
		CANNOT_INVITE_USER_BANNED_FROM_GROUP = 54,
		CANNOT_JOIN_BANNED_GROUP = 55,
		API_DISABLED_ERROR = 56,
		
		USER_HAS_NO_CHAT_ACCESS = 57,
		USER_HAS_NO_FILE_REPOSITORY_ACCESS = 58,
		USER_HAS_NO_GROUP_PERMISSION = 59,
	
		SELF_MESSAGING = 60;
	
	private int errorCode;
	
	public GenericErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public GenericErrorCode(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public boolean equals(GenericErrorCode errorCode) {
		return this.errorCode == errorCode.errorCode;
	}
}

package organize.server.models.dbconnectors;

import java.sql.Connection;

import javax.sql.DataSource;

public class SQLConnectionPoolObject {
	private final Connection connection;
	private volatile boolean error = false;
	private volatile boolean locked = false;
	private volatile DataSource dataSource;
	
	public SQLConnectionPoolObject(Connection connection, DataSource dataSource) {
		this.connection = connection;
	}
	
	public synchronized Connection getConnection() {
		return connection;
	}
	
	public synchronized boolean isError() {
		return error;
	}
	
	public synchronized void setError(boolean error) {
		this.error = error;
	}
	
	public synchronized boolean isLocked() {
		return locked;
	}
	
	public synchronized void lock() {
		this.locked = true;
	}
	
	public synchronized void unlock() {
		this.locked = false;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
}

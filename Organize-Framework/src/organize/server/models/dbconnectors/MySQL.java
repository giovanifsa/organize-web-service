/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.models.dbconnectors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.jdbc.MysqlDataSource;

import organize.server.models.GenericErrorCode;
import organize.server.models.configuration.ConfigurationListener;
import organize.server.models.database.DatabaseBannedGroupUser;
import organize.server.models.database.DatabaseChat;
import organize.server.models.database.DatabaseChatMessage;
import organize.server.models.database.DatabaseConnector;
import organize.server.models.database.DatabaseCryptographySetting;
import organize.server.models.database.DatabaseException;
import organize.server.models.database.DatabaseFile;
import organize.server.models.database.DatabaseFilePart;
import organize.server.models.database.DatabaseFileRepository;
import organize.server.models.database.DatabaseFileRepositoryFile;
import organize.server.models.database.DatabaseGroup;
import organize.server.models.database.DatabaseGroupChat;
import organize.server.models.database.DatabaseGroupChatPermissionSetting;
import organize.server.models.database.DatabaseGroupFileRepository;
import organize.server.models.database.DatabaseGroupFileRepositoryFile;
import organize.server.models.database.DatabaseGroupFileRepositoryPermissionSetting;
import organize.server.models.database.DatabaseGroupInvite;
import organize.server.models.database.DatabaseGroupPermissionLevel;
import organize.server.models.database.DatabaseGroupUser;
import organize.server.models.database.DatabaseGroupUserPermission;
import organize.server.models.database.DatabaseOnlineUser;
import organize.server.models.database.DatabasePrivateChat;
import organize.server.models.database.DatabasePrivateChatFileRepository;
import organize.server.models.database.DatabaseProfilePicture;
import organize.server.models.database.DatabaseSession;
import organize.server.models.database.DatabaseUser;
import organize.server.models.database.DatabaseUserFileRepository;
import organize.server.models.database.DatabaseUserFileRepositoryFile;
import organize.server.modules.DatabaseManager;

public class MySQL extends DatabaseConnector {
	public static final String mysql_config_prefix = "db_mysql_";
	public static final String db_mysql_address[] = {"db_mysql_address", "localhost"};
	public static final String db_mysql_port[] = {"db_mysql_port", "3306"};
	public static final String db_mysql_username[] = {"db_mysql_username", "root"};
	public static final String db_mysql_password[] = {"db_mysql_password", "abacaxi97"};
	public static final String db_mysql_database[] = {"db_mysql_database", "organize"};
	public static final String db_mysql_restartonconfigchange[] = {"db_mysql_restartonconfigchange", "true"};
	public static final String db_mysql_maxconnectionvalidationwaitms[] = {"db_mysql_maxconnectionvalidationwaitms" , "10000"};
	public static final String db_mysql_maxconnectionstopms[] = {"db_mysql_maxconnectionstopms" , "15000"};
	public static final String db_mysql_maxconnectionpoolwaitms[] = {"db_mysql_maxconnectionpoolwaitms" , "30000"};
	public static final String db_mysql_connectionpoolcount[] = {"db_mysql_connectionpoolcount" , "20"};
	public static final String db_mysql_connectionpoolmanagerdelayms[] = {"db_mysql_connectionpoolmanagerdelay" , "1000"};
	public static final String db_mysql_autocreateschema[] = {"db_mysql_autocreateschema", "true"};
	
	/**
	 * Duplicate entry '%s' for key %d
	 * The message returned with this error uses the format string for ER_DUP_ENTRY_WITH_KEY_NAME.
	 */
	public static final int MYSQL_ER_DUP_ENTRY = 1062;
	/**
	 * Message: Cannot add or update a child row: a foreign key constraint fails (%s)
	 * InnoDB reports this error when you try to add a row but there is no parent row, and a foreign key constraint fails. Add the parent row first.
	 */
	public static final int MYSQL_ER_NO_REFERENCED_ROW_2 = 1452;
	/**
	 * Message: Column '%s' cannot be null
	 * */
	public static final int MYSQL_ER_BAD_NULL_ERROR = 1048;
	
	private volatile MysqlDataSource dataSource = null;
	private volatile ScheduledFuture<?> connectionPoolManager = null;
	private volatile ArrayList<SQLConnectionPoolObject> connectionPool = null;
	
	private volatile ConfigurationListener confListener = new ConfigurationListener() {
		
		@Override
		public void onRemove(String var, String lastValue) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onCreate(String var, String value) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onChange(String var, String oldValue, String newValue) {
			if (getConfigurationManager().getBooleanValue(db_mysql_restartonconfigchange[0], db_mysql_restartonconfigchange[1])) {
				if (isStarted()) {
					logRuntimeInformation("Restarting due to \"db_mysql_restartonconfigchange\" being true.");
					stop();
					start();
				}
			}
		}
	};
	
	public MySQL(DatabaseManager databaseManager) {
		super(databaseManager);
	}
	

	@Override
	public String getModuleName() {
		return "MySQL connector";
	}

	@Override
	public synchronized void stop() {
		if (isStarted()) {
			forcedStop();
		}
	}
	
	private synchronized void forcedStop() {
		synchronized (started) {
			started = false;
			logRuntimeInformation("Stopping MySQL connector...");
			
			updateConnectionPoolManager();
			dataSource = null;
			
			if (connectionPool != null) {
				synchronized (connectionPool) {
					long startedMS = getDate().getTime();
					
					while (!connectionPool.isEmpty() && (startedMS + getConfigurationManager().getLongValue(db_mysql_maxconnectionstopms[0], db_mysql_maxconnectionstopms[1]) >= getDate().getTime())) {
						for (SQLConnectionPoolObject cm : connectionPool) {
							cm.lock();
						}
					}
					
					for (SQLConnectionPoolObject cm : connectionPool) {
						cm.lock();
						
						try {
							cm.getConnection().close();
						} catch (SQLException e) {
							logRuntimeError("Caught error while closing connection: " + e.getMessage(), false);
						}
						
						connectionPool.remove(cm);
					}
					
				}
			}
		}
	}

	@Override
	public synchronized void start() {
		synchronized (started) {
			if (!isStarted()) {
				try {
					logRuntimeInformation("Starting up...");
					
					connectionPool = new ArrayList<>();
					started = true;
					
					logRuntimeInformation("Creating threaded connection pool...");
					updateConnectionPoolManager();
					
					logRuntimeInformation("Loading internal MySQL preparation files...");
					String mysql_creation = new String(Files.readAllBytes(Paths.get(getClass().getResource("mysql_tables_creation.sql").getPath())));
					mysql_creation += new String(Files.readAllBytes(Paths.get(getClass().getResource("mysql_triggers_creation.sql").getPath())));
					logRuntimeInformation("Internal MySQL preparation files loaded successfully.");
					
					SQLConnectionPoolObject connectionModel = getAndLockConnection();
					try {
						PreparedStatement preparedStatement = connectionModel.getConnection().prepareStatement(mysql_creation);
						
						logRuntimeInformation("Preparing the database. If this is the first time, this may take a momment...");
						preparedStatement.executeUpdate();
						logRuntimeInformation("The database is now ready for use.");
					} catch (SQLException ex) {
						connectionModel.unlock();
						throw ex;
					}
					connectionModel.unlock();
					
					getConfigurationManager().addListener(MySQL.db_mysql_address[0], confListener);
					getConfigurationManager().addListener(MySQL.db_mysql_port[0], confListener);
					getConfigurationManager().addListener(MySQL.db_mysql_username[0], confListener);
					getConfigurationManager().addListener(MySQL.db_mysql_password[0], confListener);
					
					logRuntimeInformation("Started successfully.");
				} catch (DatabaseException | SQLException | IOException ex) {
					logRuntimeError("Failed to start MySQL connector due to error: " + ex.getMessage(), true);
				}
			}
		}
	}
	
	public synchronized void updateConnectionPoolManager() {
		if (isStarted()) {
			connectionPoolManager = getScheduledThreadPool().scheduleWithFixedDelay(() -> {
				while (isStarted()) {
					synchronized (connectionPool) {
						if (connectionPool.size() < getConfigurationManager().getIntValue(db_mysql_connectionpoolcount[0], db_mysql_connectionpoolcount[1])) {
							logRuntimeInformation("Initializing MySQL connection (" + (connectionPool.size() + 1) + "/" + getConfigurationManager().getIntValue(db_mysql_connectionpoolcount[0], db_mysql_connectionpoolcount[1]) + ").");
							
							try {
								dataSource = new MysqlDataSource();
								dataSource.setAllowMultiQueries(true);
								dataSource.setServerName(getConfigurationManager().getValue(db_mysql_address[0], db_mysql_address[1]));
								dataSource.setPortNumber(getConfigurationManager().getIntValue(db_mysql_port[0], db_mysql_port[1]));
								dataSource.setUser(getConfigurationManager().getValue(db_mysql_username[0], db_mysql_username[1]));
								dataSource.setPassword(getConfigurationManager().getValue(db_mysql_password[0], db_mysql_password[1]));
								dataSource.setDatabaseName(getConfigurationManager().getValue(db_mysql_database[0], db_mysql_database[1]));
								dataSource.setCreateDatabaseIfNotExist(getConfigurationManager().getBooleanValue(db_mysql_autocreateschema[0], db_mysql_autocreateschema[1]));
								
								connectionPool.add(new SQLConnectionPoolObject(dataSource.getConnection(), dataSource));
							} catch (SQLException ex1) {
								logRuntimeError("Failed to establish MySQL connection (" + (connectionPool.size() + 1) + "/" + getConfigurationManager().getIntValue(db_mysql_connectionpoolcount[0], db_mysql_connectionpoolcount[1]) + "). " + ex1.getMessage(), false);
							}
							
						}
						
						for (SQLConnectionPoolObject cm : connectionPool) {
							if (cm.isError()) {
								connectionPool.remove(cm);
								logRuntimeWarning("Destroyed failed MySQL connection.");
								logRuntimeInformation("Initializing MySQL connection to fill gap (" + (connectionPool.size() + 1) + "/" + getConfigurationManager().getIntValue(db_mysql_connectionpoolcount[0], db_mysql_connectionpoolcount[1]) + ").");
								
								try {
									dataSource = new MysqlDataSource();
									dataSource.setAllowMultiQueries(true);
									dataSource.setServerName(getConfigurationManager().getValue(db_mysql_address[0], db_mysql_address[1]));
									dataSource.setPortNumber(getConfigurationManager().getIntValue(db_mysql_port[0], db_mysql_port[1]));
									dataSource.setUser(getConfigurationManager().getValue(db_mysql_username[0], db_mysql_username[1]));
									dataSource.setPassword(getConfigurationManager().getValue(db_mysql_password[0], db_mysql_password[1]));
									dataSource.setDatabaseName(getConfigurationManager().getValue(db_mysql_database[0], db_mysql_database[1]));
									dataSource.setCreateDatabaseIfNotExist(getConfigurationManager().getBooleanValue(db_mysql_autocreateschema[0], db_mysql_autocreateschema[1]));
									
									connectionPool.add(new SQLConnectionPoolObject(dataSource.getConnection(), dataSource));
								} catch (SQLException ex2) {
									logRuntimeError("Failed to establish MySQL connection (" + (connectionPool.size() + 1) + "/" + getConfigurationManager().getIntValue(db_mysql_connectionpoolcount[0], db_mysql_connectionpoolcount[1]) + "). " + ex2.getMessage(), false);
								}
							}
						}
					}
				}
			}, 0, getConfigurationManager().getLongValue(db_mysql_connectionpoolmanagerdelayms[0], db_mysql_connectionpoolmanagerdelayms[1]), TimeUnit.MILLISECONDS);
		}
		
		else {
			if (connectionPoolManager != null) {
				connectionPoolManager.cancel(false);
				connectionPoolManager = null;
			}
		}
	}
	
	private SQLConnectionPoolObject getAndLockConnection() throws DatabaseException {
		if (isStarted()) {
			long waitTime = getDate().getTime() + getConfigurationManager().getLongValue(db_mysql_maxconnectionpoolwaitms[0], db_mysql_maxconnectionpoolwaitms[1]);
		
			while (waitTime >= getDate().getTime()) {
				synchronized (connectionPool) {
					for (SQLConnectionPoolObject cm : connectionPool) {
						if (!cm.isLocked() && !cm.isError()) {
							cm.lock();
							
							try {
								if (cm.getConnection().isValid(getConfigurationManager().getIntValue(db_mysql_maxconnectionvalidationwaitms[0], db_mysql_maxconnectionvalidationwaitms[1]))) {
									return cm;
								}
								
								cm.setError(true);
							} catch (SQLException ex) {
								logRuntimeError("db_mysql_maxconnectionvalidationwaitms variable has an invalid value.", true);
							}
							
							cm.unlock();
						}
					}
				}
			}
			
			throw new DatabaseException("MySQL connection pool time limit reached.", DatabaseException.CONNECTOR_OVERLOAD);
		}
		
		throw new DatabaseException("Database not started.", DatabaseException.NO_CONNECTION);
	}

	@Override
	public DatabaseUser createUser(String username, byte[] hashedPassword, String email, String displayName, String recoverQuestion, String recoverQuestionAnswer, long cryptographySettingID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseUser VALUES (NULL, ?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 0, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, username);
			ps.setBlob(2, new Blob(hashedPassword, null));
			ps.setString(3, email);
			ps.setString(4, displayName);
			ps.setString(5, recoverQuestion);
			ps.setString(6, recoverQuestionAnswer);
			ps.setLong(7, cryptographySettingID);
			ps.executeUpdate();
			ResultSet generated = ps.getGeneratedKeys();
			
			if (generated.next()) {
				DatabaseUser user = new DatabaseUser(generated.getLong(1), username, hashedPassword, email, displayName, recoverQuestion, recoverQuestionAnswer, 0, 0, 0, false, false, cryptographySettingID);
				connectionModel.unlock();
				
				return user;
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a new user.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex1) {
			connectionModel.unlock();
			
			if (ex1.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.USER_ALREADY_EXISTS);
			}
			
			else if (ex1.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_CRYPTOGRAPHY_SETTING);
			}
			
			else if (ex1.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("Caught an SQLException with the error code: " + ex1.getErrorCode() + ".", DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUser getUserByUsername(String username) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUser WHERE username = ?")) {
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				java.sql.Blob resultsBlob = result.getBlob("hashedPassword");
				DatabaseUser user = new DatabaseUser(result.getLong("userID"), result.getString("username"), resultsBlob.getBytes(0, (int) resultsBlob.length()), result.getString("email"),
						result.getString("displayName"), result.getString("recoverQuestion"), result.getString("recoverQuestionAnswer"), result.getLong("selectedProfilePictureNumber"), 
						result.getLong("profilePictureCount"), result.getLong("groupOwnershipCount"), result.getBoolean("isAccountDisabled"), result.getBoolean("haveSelectedProfilePicture"), result.getLong("cryptographySettingID"));
				connectionModel.unlock();
				
				return user;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("Caught SQLException with error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUser getUserByID(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUser WHERE userID = ?")) {
			ps.setLong(1, userID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				java.sql.Blob resultsBlob = result.getBlob("hashedPassword");
				DatabaseUser user = new DatabaseUser(result.getLong("userID"), result.getString("username"), resultsBlob.getBytes(0, (int) resultsBlob.length()), result.getString("email"),
						result.getString("displayName"), result.getString("recoverQuestion"), result.getString("recoverQuestionAnswer"), result.getLong("selectedProfilePictureNumber"), 
						result.getLong("profilePictureCount"), result.getLong("groupOwnershipCount"), result.getBoolean("isAccountDisabled"), result.getBoolean("haveSelectedProfilePicture"), result.getLong("cryptographySettingID"));
				connectionModel.unlock();
				
				return user;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFile createFile(long ownerUserID, String filename, String fileExtension) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseFile VALUES (NULL, ?, ?, ?, 0, 0, 0)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, filename);
			ps.setString(2, fileExtension);
			ps.setLong(3, ownerUserID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseFile file = new DatabaseFile(generated.getLong(1), filename, fileExtension, ownerUserID, 0, 0, false);
					connectionModel.unlock();
					
					return file;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create new file.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFilePart createFilePart(long fileID, byte[] filePart) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseFilePart(NULL, ?, (SELECT filePartsCount FROM DatabaseFile WHERE fileID = ?), ?, ?)", Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, fileID);
			ps.setLong(2, fileID);
			ps.setBlob(3, new Blob(filePart, null));
			ps.setLong(4, filePart.length);
			
			if (ps.executeUpdate() > 0) {
				ResultSet results = ps.getGeneratedKeys();
				
				if (!results.next()) {
					ps.close();
					connectionModel.unlock();
					throw new DatabaseException("Failed to retrieve generated keys for filePartID", DatabaseException.GENERIC_ERROR);
				}

				long filePartID = results.getLong("filePartID");
				ps.close();
				connectionModel.unlock();
				connectionModel = getAndLockConnection();
				
				ps = connectionModel.getConnection().prepareStatement("SELECT filePartNumber FROM DatabaseFilePart WHERE filePartID = ?");
				ps.setLong(1, filePartID);
				results = ps.executeQuery();
				
				if (!results.next()) {
					ps.close();
					connectionModel.unlock();
					throw new DatabaseException("Expected new table insertion doesn't exist for DatabaseFilePart.", DatabaseException.GENERIC_ERROR);
				}
				
				DatabaseFilePart dbFilePart = new DatabaseFilePart(filePartID, fileID, results.getLong("filePartNumber"), filePart, filePart.length);
				ps.close();
				connectionModel.unlock();
				
				return dbFilePart;
			}
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
		
		connectionModel.unlock();
		throw new DatabaseException("MySQL reached a unexpected point at addFilePart().", DatabaseException.GENERIC_ERROR);
	}

	@Override
	public DatabaseFilePart getFilePart(long fileID, long filePartNumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFilePart WHERE fileID = ? AND filePartNumber = ?")) {
			ps.setLong(1, fileID);
			ps.setLong(2, filePartNumber);
			ResultSet results = ps.executeQuery();
			
			if (results.next()) {
				DatabaseFilePart filePart = new DatabaseFilePart(results.getLong("filePartID"), fileID, filePartNumber, results.getBytes("filePartBytes"), results.getInt("filePartSize"));
				connectionModel.unlock();
				
				return filePart;
			}

			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE_PART);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFile getFile(long fileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFile WHERE fileID = ?")) {
			ps.setLong(1, fileID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseFile file = new DatabaseFile(fileID, result.getString("fileName"), result.getString("fileExtension"), result.getLong("ownerUserID"), 
						result.getLong("filePartsCount"), result.getLong("fileSizeInBytes"), result.getBoolean("isFileComplete"));
				connectionModel.unlock();
				
				return file;
			}

			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("Caught SQLException with error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteFile(long fileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseFile WHERE fileID = ?")) {
			ps.setLong(1, fileID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteUserByID(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseUser WHERE userID = ?")) {
			ps.setLong(1, userID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void disableUserByID(long userID, boolean disabled) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseUser SET isAccountDisabled = ? WHERE userID = ?")) {
			ps.setBoolean(1, disabled);
			ps.setLong(2, userID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroup createGroup(String groupName, long ownerUserID, int groupAccess) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroup VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, groupName);
			ps.setLong(2, ownerUserID);
			ps.setInt(3, groupAccess);
			
			if (ps.executeUpdate() > 0) {
				ResultSet results = ps.getGeneratedKeys();
				
				if (results.next()) {
					DatabaseGroup group = new DatabaseGroup(results.getLong("groupID"), groupName, ownerUserID, groupAccess);
					connectionModel.unlock();
					
					return group;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create new group.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroup getGroup(long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroup WHERE groupID = ?")) {
			ps.setLong(1, groupID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroup dbGroup = new DatabaseGroup(groupID, result.getString("groupName"), result.getLong("ownerUserID"), result.getInt("groupAccess"));
				connectionModel.unlock();
				
				return dbGroup;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupChat> getAllGroupChats(long groupID) throws DatabaseException, GenericErrorCode {
		DatabaseGroup group = getGroup(groupID);
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT DatabaseGroupChat.* FROM DatabaseGroupChat INNER JOIN DatabaseChat "
				+ "ON DatabaseGroupChat.chatID = DatabaseChat.chatID WHERE DatabaseChat.chatID = ?")) {
			ps.setLong(1, group.getGroupID());
			ResultSet result = ps.executeQuery();
			ArrayList<DatabaseGroupChat> chatList = new ArrayList<>();
			
			while (result.next()) {
				chatList.add(new DatabaseGroupChat(result.getLong("groupChatID"), result.getLong("groupID"), result.getLong("chatID"), result.getString("chatName")));
			}
			
			connectionModel.unlock();
			return chatList;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupUser getGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupUser WHERE userID = ? AND groupID = ?")) {
			ps.setLong(1, userID);
			ps.setLong(2, groupID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupUser dbGroupUser = new DatabaseGroupUser(result.getLong("groupUserID"), result.getLong("groupID"), result.getLong("userID"));
				connectionModel.unlock();
				
				return dbGroupUser;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	
	@Override
	public DatabaseChat getChat(long chatID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseChat WHERE chatID = ?")) {
			ps.setLong(1, chatID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseChat dbChat = new DatabaseChat(result.getLong("chatID"), result.getLong("messagecount"));
				connectionModel.unlock();
				
				return dbChat;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_CHAT);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseChat createChat() throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseChat VALUES (NULL, 0)", Statement.RETURN_GENERATED_KEYS)) {
			int affectedRows = ps.executeUpdate();
			
			if (affectedRows > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseChat chat = new DatabaseChat(generated.getLong(1), 0);
					connectionModel.unlock();
					
					return chat;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create chat.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseChatMessage> getAllMessagesFromChat(long chatID) throws DatabaseException, GenericErrorCode {
		DatabaseChat chat = getChat(chatID);
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseChatMessage WHERE chatID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, chat.getChatID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseChatMessage> messageList = new ArrayList<>();
			
			while (results.next()) {
				messageList.add(new DatabaseChatMessage(results.getLong("chatMessageID"), results.getLong("chatID"), results.getLong("ownerUserID"), results.getString("message"), results.getBoolean("isQuote"), 
							results.getLong("quotedOwnerUserID"), results.getLong("chatMessageNumber")));
			}
			
			connectionModel.unlock();
			return messageList;
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}
	
	@Override
	public DatabaseSession createSession(String sessionToken, String deviceToken, long userID, long expiresAtMS, long sessionCreatedAtMS) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseSession VALUES (NULL, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, sessionToken);
			ps.setString(2, deviceToken);
			ps.setLong(3, userID);
			ps.setLong(4, expiresAtMS);
			ps.setLong(5, sessionCreatedAtMS);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseSession session = new DatabaseSession(generated.getLong(1), sessionToken, deviceToken, userID, expiresAtMS, sessionCreatedAtMS);
					connectionModel.unlock();
					
					return session;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create chat.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.SESSION_ALREADY_EXISTS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	//aqui
	@Override
	public void deleteSession(long sessionID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseSession WHERE sessionID = ?")) {
			ps.setLong(1, sessionID);

			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabasePrivateChat createPrivateChat(long chatID, long userID1, long userID2) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabasePrivateChat VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, chatID);
			ps.setLong(2, userID1);
			ps.setLong(3, userID2);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					connectionModel.unlock();
					return new DatabasePrivateChat(generated.getLong(1), chatID, userID1, userID2);
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabasePrivateChat.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.PRIVATE_CHAT_ALREADY_EXISTS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID1);
				getUserByID(userID2);
				getChat(chatID);
				
				throw new DatabaseException("Failed to create a DatabasePrivateChat.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabasePrivateChat getPrivateChat(long userID1, long userID2, boolean rotateUsers) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = (rotateUsers ? connectionModel.getConnection().prepareStatement("SELECT * FROM DatabasePrivateChat WHERE userID1 = ? AND userID2 = ?") : connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseChat WHERE (userID1 = ? AND userID2 = ?) OR (userID2 = ? OR userID1 = ?)"))) {
			ps.setLong(1, userID1);
			ps.setLong(2, userID2);
			
			if (rotateUsers) {
				ps.setLong(3, userID2);
				ps.setLong(4, userID1);
			}
			
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				connectionModel.unlock();
				return new DatabasePrivateChat(result.getLong("privateChatID"), result.getLong("chatID"), result.getLong("userID1"), result.getLong("userID2"));
			}

			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseProfilePicture createProfilePicture(long userID, long fileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseProfilePicture VALUES (NULL, ?, ?, (SELECT profilePictureCount FROM DatabaseUser WHERE userID = ?))", Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, userID);
			ps.setLong(2, fileID);
			ps.setLong(3, userID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					long generatedKey = generated.getLong(1);
					ps.close();
					ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseProfilePicture WHERE profilePictureId = ?");
					ps.setLong(1, generatedKey);
					generated = ps.executeQuery();
					
					if (generated.next()) {
						DatabaseProfilePicture profilePicture = new DatabaseProfilePicture(generated.getLong("profilePictureID"), generated.getLong("userID"), generated.getLong("fileID"), generated.getLong("profilePictureNumber"));
						ps.close();
						
						connectionModel.unlock();
						return profilePicture;
					}
				}
			}
			
			ps.close();
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseProfilePicture.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID);
				getFile(fileID);
				
				throw new DatabaseException("Failed to create a DatabaseProfilePicture.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseProfilePicture> getUserProfilePictures(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUser user = getUserByID(userID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseProfilePicture WHERE userID = ?")) {
			ps.setLong(1, user.getUserID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseProfilePicture> profilePictures = new ArrayList<>();
			
			while (results.next()) {
				profilePictures.add(new DatabaseProfilePicture(results.getLong("profilePictureID"), user.getUserID(), results.getLong("fileID"), results.getLong("profilePictureNumber")));
			}
			
			connectionModel.unlock();
			return profilePictures;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUserFileRepository getUserFileRepositoryByUserID(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUserFileRepository WHERE userID = ?")) {
			ps.setLong(1, userID);
			ResultSet results = ps.executeQuery();
			
			if (results.next()) {
				connectionModel.unlock();
				return new DatabaseUserFileRepository(results.getLong("userFileRepositoryID"), results.getLong("userID"), results.getLong("fileRepositoryID"), results.getLong("fileRepositoryFileCount"));
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER_FILE_REPOSITORY);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUserFileRepository createUserFileRepository(long userID, long fileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseUserFileRepository VALUES (NULL, ?, ?, 0)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, userID);
			ps.setLong(2, fileRepositoryID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					connectionModel.unlock();
					return new DatabaseUserFileRepository(generated.getLong(1), userID, fileRepositoryID, 0);
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseProfilePicture.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID);
				getFileRepository(fileRepositoryID);

				throw new DatabaseException("Failed to create a DatabaseProfilePicture.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabasePrivateChatFileRepository createPrivateChatFileRepository(long privateChatID, long fileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabasePrivateChatFileRepository VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, privateChatID);
			ps.setLong(2, fileRepositoryID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					connectionModel.unlock();
					return new DatabasePrivateChatFileRepository(generated.getLong(1), privateChatID, fileRepositoryID);
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabasePrivateChatFileRepository.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getPrivateChat(privateChatID);
				getFileRepository(fileRepositoryID);
				
				throw new DatabaseException("Failed to create a DatabasePrivateChatFileRepository.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUser changeUserRecoverQuestion(long userID, String newRecoverQuestion) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseUser SET recoverQuestion = ? WHERE userID = ?")) {
			ps.setString(1, newRecoverQuestion);
			ps.setLong(2, userID);
			
			if (ps.executeUpdate() > 0) {
				connectionModel.unlock();
				return getUserByID(userID);
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		} catch (SQLException ex) {
			if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUser changeUserRecoverAnswer(long userID, String newRecoverQuestionAnswer) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseUser SET recoverQuestionAnwer = ? WHERE userID = ?")) {
			ps.setString(1, newRecoverQuestionAnswer);
			ps.setLong(2, userID);
			
			if (ps.executeUpdate() > 0) {
				connectionModel.unlock();
				return getUserByID(userID);
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUser changeUserPassword(long userID, byte[] newHashedPassword, long newCryptographySettingID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseUser SET hashedPassword = ?, cryptographySettingID = ? WHERE userID = ?")) {
			ps.setBlob(1, new Blob(newHashedPassword, null));
			ps.setLong(2, newCryptographySettingID);
			ps.setLong(3, userID);
			
			if (ps.executeUpdate() > 0) {
				connectionModel.unlock();
				return getUserByID(userID);
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_CRYPTOGRAPHY_SETTING);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseUserFileRepositoryFile getUserFileRepositoryFile(long userFileRepositoryFileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUserFileRepositoryFile WHERE userFileRepositoryFileID = ?")) {
			ps.setLong(1, userFileRepositoryFileID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseUserFileRepositoryFile file = new DatabaseUserFileRepositoryFile(result.getLong("userFileRepositoryFileID"), result.getLong("userFileRepositoryID"), result.getLong("fileRepositoryFileID"), result.getLong("userFileRepositoryFileNumber"));
				connectionModel.unlock();
				
				return file;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER_FILE_REPOSITORY_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	/*
	 * "CREATE TABLE IF NOT EXISTS DatabaseUserFileRepositoryFile(userFileRepositoryFileID BIGINT NOT NULL AUTO_INCREMENT, userFileRepositoryID BIGINT NOT NULL, fileRepositoryFileID BIGINT NOT NULL, "
				+ "userFileRepositoryFileNumber BIGINT NOT NULL, PRIMARY KEY (userFileRepositoryFileID), FOREIGN KEY (userFileRepositoryID) REFERENCES DatabaseUserFileRepository(userFileRepositoryID) ON DELETE CASCADE, "
				+ "FOREIGN KEY (fileRepositoryFileID) REFERENCES DatabaseFileRepositoryFile(fileRepositoryFileID) ON DELETE CASCADE) ENGINE=InnoDB;" +
	 * */
	@Override
	public ArrayList<DatabaseUserFileRepositoryFile> getAllUserFileRepositoryFiles(long userFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUserFileRepository userFileRepository = getUserFileRepositoryByID(userFileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUserFileRepositoryFile WHERE userFileRepositoryID = ?")) {
			ps.setLong(1, userFileRepository.getUserFileRepositoryID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseUserFileRepositoryFile> userFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				userFileRepositoryFiles.add(new DatabaseUserFileRepositoryFile(results.getLong("userFileRepositoryFileID"), results.getLong("userFileRepositoryID"), results.getLong("fileRepositoryFileID"), results.getLong("userFileRepositoryFileNumber")));
			}
			
			connectionModel.unlock();
			return userFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseUserFileRepositoryFile> getUserFileRepositoryFilesInRange(long userFileRepositoryID, long startingFileNumber, long endingFileNumber) throws DatabaseException, GenericErrorCode {
		DatabaseUserFileRepository userFileRepository = getUserFileRepositoryByID(userFileRepositoryID);
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUserFileRepositoryFile WHERE (userFileRepositoryID = ?) AND (userFileRepositoryFileNumber BETWEEN ? AND ?)")) {
			ps.setLong(1, userFileRepository.getUserFileRepositoryID());
			ps.setLong(2, startingFileNumber);
			ps.setLong(3, endingFileNumber);
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseUserFileRepositoryFile> userFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				userFileRepositoryFiles.add(new DatabaseUserFileRepositoryFile(results.getLong("userFileRepositoryFileID"), results.getLong("userFileRepositoryID"), results.getLong("fileRepositoryFileID"), results.getLong("userFileRepositoryFileNumber")));
			}
			
			connectionModel.unlock();
			return userFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	/*
	 * "CREATE TABLE IF NOT EXISTS DatabaseFilePart(filePartID BIGINT NOT NULL AUTO_INCREMENT, fileID BIGINT NOT NULL, filePartNumber BIGINT NOT NULL, filePartBytes LONGBLOB NOT NULL, "
				+ "filePartSize INT NOT NULL, PRIMARY KEY (filePartID), FOREIGN KEY (fileID) REFERENCES DatabaseFile(fileID) ON DELETE CASCADE) ENGINE=InnoDB;" +*/
	@Override
	public ArrayList<DatabaseFilePart> getAllFileParts(long fileID) throws DatabaseException, GenericErrorCode {
		DatabaseFile file = getFile(fileID);
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFile WHERE fileID = ?")) {
			ps.setLong(1, file.getFileID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseFilePart> userFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				java.sql.Blob resultsBlob = results.getBlob("filePartBytes");
				userFileRepositoryFiles.add(new DatabaseFilePart(results.getLong("filePartID"), file.getFileID(), results.getLong("filePartNumber"), resultsBlob.getBytes(0, (int) resultsBlob.length()), results.getInt("filePartSize")));
			}
			
			connectionModel.unlock();
			return userFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseFilePart> getFilePartsInRange(long fileID, long startingPartNumber, long endingPartNumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseFile file = getFile(fileID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFile WHERE (fileID = ?) AND (filePartNumber BETWEEN ? AND ?)")) {
			ps.setLong(1, file.getFileID());
			ps.setLong(2, startingPartNumber);
			ps.setLong(3, endingPartNumber);
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseFilePart> userFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				java.sql.Blob resultsBlob = results.getBlob("filePartBytes");
				userFileRepositoryFiles.add(new DatabaseFilePart(results.getLong("filePartID"), file.getFileID(), results.getLong("filePartNumber"), resultsBlob.getBytes(0, (int) resultsBlob.length()), results.getInt("filePartSize")));
			}
			
			connectionModel.unlock();
			return userFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFile setFileComplete(long fileID, boolean isFileComplete) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseFile SET isFileComplete = ? WHERE fileID = ?")) {
			ps.setBoolean(1, isFileComplete);
			ps.setLong(2, fileID);
			
			if (ps.executeUpdate() > 0) {
				connectionModel.unlock();
				return getFile(fileID);
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteGroup(long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE DatabaseGroup WHERE groupID = ?")) {
			ps.setLong(1, groupID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupUser createGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupUser VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, groupID);
			ps.setLong(2, userID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseGroupUser groupUser = new DatabaseGroupUser(generated.getLong(1), groupID, userID);
					connectionModel.unlock();
					
					return groupUser;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseGroupUser.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.GROUP_USER_ALREADY_EXISTS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID);
				getGroup(groupID);
				
				throw new DatabaseException("Failed to create a DatabaseGroupUser.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupChatPermissionSetting createGroupChatPermissionSetting(long groupChatID, long groupPermissionLevelID) throws DatabaseException, GenericErrorCode {
		DatabaseGroupChat groupChat = getGroupChat(groupChatID);
		DatabaseGroupPermissionLevel permissionLevel = getGroupPermissionLevel(groupPermissionLevelID);
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		if (groupChat.getGroupID() == permissionLevel.getGroupID()) {
			try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupChatPermissionSetting VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				ps.setLong(1, groupChat.getGroupChatID());
				ps.setLong(2, permissionLevel.getGroupPermissionLevelID());
				
				if (ps.executeUpdate() > 0) {
					ResultSet generated = ps.getGeneratedKeys();
					
					if (generated.next()) {
						DatabaseGroupChatPermissionSetting chatPermission = new DatabaseGroupChatPermissionSetting(generated.getLong(1), groupChat.getGroupChatID(), permissionLevel.getGroupPermissionLevelID());
						connectionModel.unlock();
						
						return chatPermission;
					}
				}
				
				connectionModel.unlock();
				throw new DatabaseException("Failed to create a DatabaseGroupChatPermissionSetting.", DatabaseException.GENERIC_ERROR);
			} catch (SQLException ex) {
				connectionModel.unlock();
				
				if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
					throw new GenericErrorCode(GenericErrorCode.GROUP_CHAT_PERMISSION_SETTING_ALREADY_EXISTS);
				}
				
				else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
					throw new DatabaseException("Failed to create a DatabaseGroupChatPermissionSetting.", DatabaseException.GENERIC_ERROR);
				}
				
				throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
			}
		}
		
		throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
	}

	@Override
	public DatabaseGroupFileRepositoryPermissionSetting createGroupFileRepositoryPermissionSetting(long groupFileRepositoryID, long groupPermissionLevelID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupFileRepository groupFileRepository = getGroupFileRepository(groupFileRepositoryID);
		DatabaseGroupPermissionLevel groupPermissionLevel = getGroupPermissionLevel(groupPermissionLevelID);
		
		if (groupFileRepository.getGroupID() == groupPermissionLevel.getGroupID()) {
			try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupFileRepositoryPermissionSetting VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				ps.setLong(1, groupFileRepository.getGroupFileRepositoryID());
				ps.setLong(2, groupPermissionLevel.getGroupPermissionLevelID());
				
				if (ps.executeUpdate() > 0) {
					ResultSet generated = ps.getGeneratedKeys();
					
					if (generated.next()) {
						DatabaseGroupFileRepositoryPermissionSetting repositoryPermission = new DatabaseGroupFileRepositoryPermissionSetting(generated.getLong(1), groupFileRepository.getGroupFileRepositoryID(), 
								groupPermissionLevel.getGroupPermissionLevelID());
						connectionModel.unlock();
						
						return repositoryPermission;
					}
				}
				
				connectionModel.unlock();
				throw new DatabaseException("Failed to create a DatabaseGroupFileRepositoryPermissionSetting.", DatabaseException.GENERIC_ERROR);
			} catch (SQLException ex) {
				connectionModel.unlock();
				
				if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
					throw new GenericErrorCode(GenericErrorCode.GROUP_FILE_REPOSITORY_PERMISSION_SETTING_ALREADY_EXISTS);
				}
				
				else if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
					throw new DatabaseException("Failed to create a DatabaseGroupFileRepositoryPermissionSetting.", DatabaseException.GENERIC_ERROR);
				}
				
				throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
			}
		}
		
		throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
	}

	@Override
	public DatabaseGroupPermissionLevel createGroupPermissionLevel(long groupID, String permissionName, boolean removable, boolean allowBanGroupUsers, boolean allowKickGroupUsers,
			boolean allowManagePermissionLevels, boolean allowAccessAllGroupChats, boolean allowAccessAllGroupFileRepositories, boolean isGroupOwner) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupPermissionLevel VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, groupID);
			ps.setString(2, permissionName);
			ps.setBoolean(3, removable);
			ps.setBoolean(4, allowBanGroupUsers);
			ps.setBoolean(5, allowKickGroupUsers);
			ps.setBoolean(6, allowManagePermissionLevels);
			ps.setBoolean(7, allowAccessAllGroupChats);
			ps.setBoolean(8, allowAccessAllGroupFileRepositories);
			ps.setBoolean(9, isGroupOwner);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseGroupPermissionLevel permissionLevel = new DatabaseGroupPermissionLevel(generated.getLong(1), groupID, permissionName, removable, allowBanGroupUsers, allowKickGroupUsers, 
							allowManagePermissionLevels, allowAccessAllGroupChats, allowAccessAllGroupFileRepositories, isGroupOwner);
					connectionModel.unlock();
					
					return permissionLevel;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseGroupPermissionLevel.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getGroup(groupID);
				throw new DatabaseException("Failed to create a DatabaseGroupPermissionLevel.", DatabaseException.GENERIC_ERROR);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupChat createGroupChat(long groupID, long chatID, String chatName) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupChat VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, groupID);
			ps.setLong(2, chatID);
			ps.setString(3, chatName);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseGroupChat groupChat = new DatabaseGroupChat(generated.getLong(1), groupID, chatID, chatName);
					connectionModel.unlock();
					
					return groupChat;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseGroupChat.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getGroup(groupID);
				getChat(chatID);
				
				throw new DatabaseException("Failed to create a DatabaseGroupChat.", DatabaseException.GENERIC_ERROR);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupFileRepository> getAllGroupFileRepositories(long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroup group = getGroup(groupID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupFileRepository WHERE groupID = ?")) {
			ps.setLong(1, group.getGroupID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupFileRepository> groupFileRepositories = new ArrayList<>();
			
			while (results.next()) {
				groupFileRepositories.add(new DatabaseGroupFileRepository(results.getLong("groupRepositoryID"), results.getLong("groupID"), results.getLong("fileRepositoryID"), results.getString("groupRepositoryName"), results.getLong("groupFileRepositoryFileCount")));
			}
			
			connectionModel.unlock();
			return groupFileRepositories;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupChat getGroupChat(long groupChatID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupChat WHERE groupChatID = ?")) {
			ps.setLong(1, groupChatID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupChat groupChat = new DatabaseGroupChat(result.getLong("groupChatID"), result.getLong("groupID"), result.getLong("chatID"), result.getString("chatName"));
				connectionModel.unlock();
				
				return groupChat;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_CHAT);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupChatPermissionSetting> getGroupChatPermissionSettings(long groupChatID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupChat groupChat = getGroupChat(groupChatID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupChatPermissionSetting WHERE groupChatID = ?")) {
			ps.setLong(1, groupChat.getGroupChatID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupChatPermissionSetting> groupChatPermissionSettings = new ArrayList<>();
			
			while (results.next()) {
				groupChatPermissionSettings.add(new DatabaseGroupChatPermissionSetting(results.getLong("groupChatPermissionSettingID"), results.getLong("groupChatID"), results.getLong("groupPermissionLevelID")));
			}
			
			connectionModel.unlock();
			return groupChatPermissionSettings;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupFileRepositoryPermissionSetting> getGroupFileRepositoryPermissionSettings(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupFileRepository groupFileRepository = getGroupFileRepository(groupFileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupChatPermissionSetting WHERE groupChatID = ?")) {
			ps.setLong(1, groupFileRepository.getGroupFileRepositoryID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupFileRepositoryPermissionSetting> groupFileRepositoryPermissionSettings = new ArrayList<>();
			
			while (results.next()) {
				groupFileRepositoryPermissionSettings.add(new DatabaseGroupFileRepositoryPermissionSetting(results.getLong("groupFileRepositoryPermissionSettingID"), results.getLong("groupFileRepositoryID"), results.getLong("groupPermissionLevelID")));
			}
			
			connectionModel.unlock();
			return groupFileRepositoryPermissionSettings;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupUserPermission> getGroupUserPermissions(long groupUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupUser groupUser = getGroupUser(groupUserID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupUserPermission WHERE groupUserID = ?")) {
			ps.setLong(1, groupUser.getGroupUserID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupUserPermission> groupUserPermissions = new ArrayList<>();
			
			while (results.next()) {
				groupUserPermissions.add(new DatabaseGroupUserPermission(results.getLong("groupUserPermissionID"), results.getLong("groupUserID"), results.getLong("groupPermissionLevelID"), results.getBoolean("removable")));
			}
			
			connectionModel.unlock();
			return groupUserPermissions;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupPermissionLevel> getGroupPermissionLevels(long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroup group = getGroup(groupID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupPermissionLevel WHERE groupID = ?")) {
			ps.setLong(1, group.getGroupID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupPermissionLevel> groupPermissionLevels = new ArrayList<>();
			
			while (results.next()) {
				groupPermissionLevels.add(new DatabaseGroupPermissionLevel(results.getLong("groupPermissionLevelID"), results.getLong("groupID"), results.getString("permissionName"), 
						results.getBoolean("removable"), results.getBoolean("allowBanGroupUsers"), results.getBoolean("allowKickGroupUsers"), results.getBoolean("allowManagePermissionLevels"), 
						results.getBoolean("allowAccessAllGroupChats"), results.getBoolean("allowAccessAllGroupFileRepositories"), results.getBoolean("isGroupOwner")));
			}
			
			connectionModel.unlock();
			return groupPermissionLevels;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupUserPermission createGroupUserPermission(long groupUserID, long groupPermissionLevelID, boolean removable) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupUser groupUser = getGroupUser(groupUserID);
		DatabaseGroupPermissionLevel permissionLevel = getGroupPermissionLevel(groupPermissionLevelID);
		
		if (groupUser.getGroupID() == permissionLevel.getGroupID()) {
			try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupUserPermission VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				ps.setLong(1, groupUser.getGroupUserID());
				ps.setLong(2, permissionLevel.getGroupPermissionLevelID());
				ps.setBoolean(3, removable);
				
				if (ps.executeUpdate() > 0) {
					ResultSet generated = ps.getGeneratedKeys();
					
					if (generated.next()) {
						DatabaseGroupUserPermission permission = new DatabaseGroupUserPermission(generated.getLong(1), groupUser.getGroupUserID(), permissionLevel.getGroupPermissionLevelID(), removable);
						connectionModel.unlock();
						
						return permission;
					}
				}
				
				connectionModel.unlock();
				throw new DatabaseException("Failed to create a DatabaseGroupUserPermission.", DatabaseException.GENERIC_ERROR);
			} catch (SQLException ex) {
				connectionModel.unlock();
				
				if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
					throw new DatabaseException("Failed to create a DatabaseGroupUserPermission.", DatabaseException.GENERIC_ERROR);
				}
				
				throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
			}
		}
		
		throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
	}

	@Override
	public DatabaseGroupFileRepository getGroupFileRepository(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupFileRepository WHERE groupFileRepositoryID = ?")) {
			ps.setLong(1, groupFileRepositoryID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupFileRepository fileRepository = new DatabaseGroupFileRepository(result.getLong("groupFileRepositoryID"), result.getLong("groupID"), result.getLong("fileRepositoryID"), result.getString("groupFileRepositoryName"), 
						result.getLong("groupFileRepositoryFileCount"));
				connectionModel.unlock();
				
				return fileRepository;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_FILE_REPOSITORY);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupFileRepositoryFile> getAllGroupFileRepositoryFiles(long groupFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupFileRepository groupFileRepository = getGroupFileRepository(groupFileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupFileRepositoryFile WHERE groupID = ?")) {
			ps.setLong(1, groupFileRepository.getGroupFileRepositoryID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupFileRepositoryFile> groupFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				groupFileRepositoryFiles.add(new DatabaseGroupFileRepositoryFile(results.getLong("groupFileRepositoryFileID"), results.getLong("groupFileRepositoryID"), 
						results.getLong("fileRepositoryFileID"), results.getLong("groupFileRepositoryFileNumber")));
			}
			
			connectionModel.unlock();
			return groupFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupFileRepositoryFile> getGroupFileRepositoryFilesInRange(long groupFileRepositoryID, long startingFileNumber, long endingFilenumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseGroupFileRepository groupFileRepository = getGroupFileRepository(groupFileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupFileRepositoryFile WHERE groupID = ?")) {
			ps.setLong(1, groupFileRepository.getGroupFileRepositoryID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupFileRepositoryFile> groupFileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				groupFileRepositoryFiles.add(new DatabaseGroupFileRepositoryFile(results.getLong("groupFileRepositoryFileID"), results.getLong("groupFileRepositoryID"), 
						results.getLong("fileRepositoryFileID"), results.getLong("groupFileRepositoryFileNumber")));
			}
			
			connectionModel.unlock();
			return groupFileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupFileRepositoryFile getGroupFileRepositoryFile(long groupFileRepositoryFileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();

		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupFileRepositoryFile WHERE groupFileRepositoryFileID = ?")) {
			ps.setLong(1, groupFileRepositoryFileID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupFileRepositoryFile repositoryFile = new DatabaseGroupFileRepositoryFile(result.getLong("groupFileRepositoryFileID"), result.getLong("groupFileRepositoryID"), result.getLong("fileRepositoryFileID"), 
						result.getLong("groupFileRepositoryFileNumber"));
				connectionModel.unlock();
				
				return repositoryFile;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_FILE_REPOSITORY_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupInvite createGroupInvite(long groupID, long invitedUserID, long invitingUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseGroupInvite VALUES (NULL, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, invitedUserID);
			ps.setLong(2, invitingUserID);
			ps.setLong(3, groupID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseGroupInvite invite = new DatabaseGroupInvite(generated.getLong(1), invitedUserID, invitingUserID, groupID);
					connectionModel.unlock();
					
					return invite;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseGroupInvite.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(invitedUserID);
				getUserByID(invitingUserID);
				getGroup(groupID);
				
				throw new DatabaseException("Failed to create a DatabaseGroupUserPermission.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseGroupInvite> getGroupInvitesForUser(long invitedUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUser user = getUserByID(invitedUserID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupInvite WHERE invitedUserID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, user.getUserID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseGroupInvite> groupInvites = new ArrayList<>();
			
			while (results.next()) {
				groupInvites.add(new DatabaseGroupInvite(results.getLong("groupInviteID"), results.getLong("invitedUserID"), results.getLong("invitingUserID"), results.getLong("groupID")));
			}
			
			connectionModel.unlock();
			return groupInvites;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteGroupInvite(long groupInviteID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseGroupInvite WHERE groupInviteID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, groupInviteID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_INVITE);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupInvite getGroupInvite(long groupInviteID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupInvite WHERE groupInviteID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, groupInviteID);
			ResultSet result = ps.executeQuery();
			
			while (result.next()) {
				DatabaseGroupInvite invite = new DatabaseGroupInvite(result.getLong("groupInviteID"), result.getLong("invitedUserID"), result.getLong("invitingUserID"), result.getLong("groupID"));
				connectionModel.unlock();
				
				return invite;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_INVITE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseChatMessage createChatMessage(long chatID, long ownerUserID, String message, boolean isQuote, long quotedOwnerUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseChatMessage VALUES (NULL, ?, ?, ?, ?, ?, (SELECT messageCount FROM DatabaseChat WHERE chatID = ?))", Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, chatID);
			ps.setLong(2, ownerUserID);
			ps.setString(3, message);
			ps.setBoolean(4, isQuote);
			ps.setLong(5, quotedOwnerUserID);
			ps.setLong(6, chatID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					long chatMessageID = generated.getLong(1);
					ps.close();
					ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseChatMessage WHERE chatMessageID = ?");
					ps.setLong(1, chatMessageID);
					generated = ps.executeQuery();
					
					if (generated.next()) {
						DatabaseChatMessage chatMessage = new DatabaseChatMessage(generated.getLong("chatMessageID"), generated.getLong("chatID"), generated.getLong("ownerUserID"), generated.getString("message"), generated.getBoolean("isQuote"), generated.getLong("quotedOwnerUserID"), generated.getLong("chatMessageNumber"));
						ps.close();
						connectionModel.unlock();
						
						return chatMessage;
					}
				}
			}
			ps.close();
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseChatMessage.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(ownerUserID);
				if (isQuote) {
					getUserByID(quotedOwnerUserID);
				}
				getChat(chatID);
				
				throw new DatabaseException("Failed to create a DatabaseGroupUserPermission.", DatabaseException.GENERIC_ERROR);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseChatMessage> getMessagesFromChatInRange(long chatID, long startingMessageNumber, long endingMessageNumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseChat chat = getChat(chatID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseChat WHERE chatID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, chat.getChatID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseChatMessage> chatMessages = new ArrayList<>();
			
			while (results.next()) {
				chatMessages.add(new DatabaseChatMessage(results.getLong("chatMessageID"), results.getLong("chatID"), results.getLong("ownerUserID"), results.getString("message"), results.getBoolean("isQuote"), results.getLong("quotedOwnerUserID"), results.getLong("chatMessageNumber")));
			}
			
			connectionModel.unlock();
			return chatMessages;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteSessionsByUserID(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUser user = getUserByID(userID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseSession WHERE userID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, user.getUserID());
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
			}
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseCryptographySetting createCryptographySetting(String usedHashName, byte[] usedSalt, int iterations, int length) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseCryptographySetting VALUES (NULL, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, usedHashName);
			ps.setBlob(2, new Blob(usedSalt, null));
			ps.setInt(3, iterations);
			ps.setInt(4, length);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseCryptographySetting cryptoSetting = new DatabaseCryptographySetting(generated.getLong(1), usedHashName, usedSalt, iterations, length);
					connectionModel.unlock();
					
					return cryptoSetting;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseCryptographySetting.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseCryptographySetting getCryptographySetting(long cryptographySettingID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseCryptographySetting WHERE cryptographySettingID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, cryptographySettingID);
			ResultSet result = ps.getGeneratedKeys();
			
			if (result.next()) {
				java.sql.Blob salt = result.getBlob("usedSalt");
				DatabaseCryptographySetting setting = new DatabaseCryptographySetting(result.getLong("cryptographySettingID"), result.getString("usedHashName"), salt.getBytes(0, (int) salt.length()), result.getInt("iterations"), result.getInt("length"));
				connectionModel.unlock();
				
				return setting;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_CRYPTOGRAPHY_SETTING);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteCryptographySetting(long cryptographySettingID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseCryptographySetting WHERE cryptographySettingID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, cryptographySettingID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_CRYPTOGRAPHY_SETTING);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFileRepository createFileRepository() throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseFileRepository VALUES (NULL, 0)", Statement.RETURN_GENERATED_KEYS)) {
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseFileRepository fileRepository = new DatabaseFileRepository(generated.getLong(1), 0);
					connectionModel.unlock();
					
					return fileRepository;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseFileRepository.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFileRepositoryFile createFileRepositoryFile(long fileRepositoryID, long fileID, long repositoryFileNumber, long ownerUserID, String description) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseFileRepositoryFile VALUES (NULL, ?, ?, ?, (SELECT fileRepositoryFileCount FROM DatabaseFileRepository WHERE fileRepositoryID = ?), ?)", Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, fileRepositoryID);
			ps.setLong(2, ownerUserID);
			ps.setLong(3, fileID);
			ps.setLong(4, fileRepositoryID);
			ps.setString(5, description);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					long fileRepositoryFileID = generated.getLong(1);
					ps.close();
					ps = connectionModel.getConnection().prepareStatement("SELECT fileRepositoryFileNumber FROM DatabaseFileRepositoryFile WHERE fileRepositoryFileID = ?");
					ps.setLong(1, fileRepositoryFileID);
					generated = ps.executeQuery();
					
					if (generated.next()) {
						DatabaseFileRepositoryFile fileRepositoryFile = new DatabaseFileRepositoryFile(fileRepositoryFileID, fileRepositoryID, fileID, generated.getLong("repositoryFileNumber"), ownerUserID, description);
						ps.close();
						connectionModel.unlock();
						
						return fileRepositoryFile;
					}
				}
			}
			
			ps.close();
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseFileRepositoryFile.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getFile(fileID);
				getFileRepository(fileRepositoryID);
				getUserByID(ownerUserID);
				
				throw new DatabaseException("Failed to create a DatabaseFileRepositoryFile.", DatabaseException.GENERIC_ERROR);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteFileRepositoryFile(long fileRepositoryFileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseFileRepositoryFile WHERE fileRepositoryFileID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, fileRepositoryFileID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE_REPOSITORY_FILE);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseFileRepositoryFile> getAllFileRepositoryFiles(long fileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseFileRepository fileRepository = getFileRepository(fileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFileRepositoryFile WHERE fileRepositoryID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, fileRepository.getFileRepositoryID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseFileRepositoryFile> fileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				fileRepositoryFiles.add(new DatabaseFileRepositoryFile(results.getLong("fileRepositoryFileID"), results.getLong("fileRepositoryID"), results.getLong("fileID"), results.getLong("fileRepositoryFileNumber"), results.getLong("ownerUserID"), results.getString("description")));
			}
			
			connectionModel.unlock();
			return fileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseFileRepositoryFile> getFileRepositoryFilesInRange(long fileRepositoryID, long startingFileNumber, long endingFileNumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseFileRepository fileRepository = getFileRepository(fileRepositoryID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFileRepositoryFile WHERE (fileRepositoryID = ?) AND (fileRepositoryFileNumber BETWEEN ? AND ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, fileRepository.getFileRepositoryID());
			ps.setLong(2, startingFileNumber);
			ps.setLong(3, endingFileNumber);
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseFileRepositoryFile> fileRepositoryFiles = new ArrayList<>();
			
			while (results.next()) {
				fileRepositoryFiles.add(new DatabaseFileRepositoryFile(results.getLong("fileRepositoryFileID"), results.getLong("fileRepositoryID"), results.getLong("fileID"), results.getLong("fileRepositoryFileNumber"), results.getLong("ownerUserID"), results.getString("description")));
			}
			
			connectionModel.unlock();
			return fileRepositoryFiles;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFileRepositoryFile getFileRepositoryFile(long fileRepositoryFileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFileRepositoryFile WHERE fileRepositoryFileID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, fileRepositoryFileID);
			ResultSet result = ps.getGeneratedKeys();
			
			if (result.next()) {
				DatabaseFileRepositoryFile file = new DatabaseFileRepositoryFile(result.getLong("fileRepositoryFileID"), result.getLong("fileRepositoryID"), result.getLong("fileID"), result.getLong("fileRepositoryFileNumber"), result.getLong("ownerUserID"), result.getString("description"));
				connectionModel.unlock();
				
				return file;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE_REPOSITORY_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabasePrivateChat getPrivateChat(long privateChatID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabasePrivateChat WHERE privateChatID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, privateChatID);
			ResultSet result = ps.getGeneratedKeys();
			
			if (result.next()) {
				DatabasePrivateChat privateChat = new DatabasePrivateChat(result.getLong("privateChatID"), result.getLong("chatID"), result.getLong("userID1"), result.getLong("userID2"));
				connectionModel.unlock();
				
				return privateChat;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseProfilePicture getProfilePictureByID(long profilePictureID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseProfilePicture WHERE profilePictureID = ?")) {
			ps.setLong(1, profilePictureID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseProfilePicture picture = new DatabaseProfilePicture(profilePictureID, result.getLong("userID"), result.getLong("fileID"), result.getLong("profilePictureNumber"));
				connectionModel.unlock();
				
				return picture;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PROFILE_PICTURE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseProfilePicture getProfilePictureByNumber(long userID, long profilePictureNumber) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUser user = getUserByID(userID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseProfilePicture WHERE userID = ? AND profilePictureNumber = ?")) {
			ps.setLong(1, user.getUserID());
			ps.setLong(2, profilePictureNumber);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseProfilePicture picture = new DatabaseProfilePicture(result.getLong("profilePictureID"), userID, result.getLong("fileID"), profilePictureNumber);
				connectionModel.unlock();
				
				return picture;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PROFILE_PICTURE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}


	@Override
	public DatabaseUserFileRepository getUserFileRepositoryByID(long userFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseUserFileRepository WHERE userFileRepositoryID = ?")) {
			ps.setLong(1, userFileRepositoryID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseUserFileRepository fileRepository = new DatabaseUserFileRepository(userFileRepositoryID, result.getLong("userID"), result.getLong("fileRepositoryID"), result.getLong("fileRepositoryFileCount"));
				connectionModel.unlock();
				
				return fileRepository;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER_FILE_REPOSITORY);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabasePrivateChatFileRepository> getPrivateChatFileRepositories(long privateChatID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabasePrivateChat privateChat = getPrivateChat(privateChatID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabasePrivateChatFileRepository WHERE privateChatID = ?")) {
			ps.setLong(1, privateChat.getChatID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabasePrivateChatFileRepository> fileRepositories = new ArrayList<>();
			
			while (results.next()) {
				fileRepositories.add(new DatabasePrivateChatFileRepository(results.getLong("privateChatFileRepositoryID"), privateChat.getChatID(), results.getLong("fileRepositoryID")));
			}
			
			connectionModel.unlock();
			return fileRepositories;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}
	
	@Override
	public DatabasePrivateChatFileRepository getPrivateChatFileRepository(long privateChatFileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabasePrivateChatFileRepository WHERE privateChatFileRepositoryID = ?")) {
			ps.setLong(1, privateChatFileRepositoryID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabasePrivateChatFileRepository fileRepository = new DatabasePrivateChatFileRepository(privateChatFileRepositoryID, result.getLong("privateChatID"), result.getLong("fileRepositoryID"));
				connectionModel.unlock();
				
				return fileRepository;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseOnlineUser getOnlineUserByID(long onlineUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseOnlineUser WHERE onlineUserID = ?")) {
			ps.setLong(1, onlineUserID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseOnlineUser onlineUser = new DatabaseOnlineUser(onlineUserID, result.getLong("userID"), result.getString("deviceToken"), result.getInt("userStatus"), 
						result.getString("customStatus"), result.getLong("lastUpdatedMS"));
				connectionModel.unlock();
				
				return onlineUser;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_ONLINE_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public ArrayList<DatabaseOnlineUser> getOnlineUserByUserID(long userID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		DatabaseUser user = getUserByID(userID);
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseOnlineUser WHERE userID = ?")) {
			ps.setLong(1, user.getUserID());
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseOnlineUser> onlineUsers = new ArrayList<>();
			
			while (results.next()) {
				onlineUsers.add(new DatabaseOnlineUser(results.getLong("onlineUserID"), userID, results.getString("deviceToken"), results.getInt("userStatus"), 
						results.getString("customStatus"), results.getLong("lastUpdatedMS")));
			}
			
			connectionModel.unlock();
			return onlineUsers;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteOnlineUser(long onlineUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseOnlineUser WHERE onlineUserID = ?")) {
			ps.setLong(1, onlineUserID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_ONLINE_USER);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseOnlineUser createOnlineUser(long userID, String deviceToken, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseOnlineUser VALUES (NULL, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, userID);
			ps.setString(2, deviceToken);
			ps.setInt(3, userStatus);
			ps.setString(4, customStatus);
			ps.setLong(5, lastUpdatedMS);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseOnlineUser onlineUser = new DatabaseOnlineUser(generated.getLong(1), userID, deviceToken, userStatus, customStatus, lastUpdatedMS);
					connectionModel.unlock();
					
					return onlineUser;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseOnlineUser.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID);
				throw new DatabaseException("Failed to create a DatabaseOnlineUser.", DatabaseException.GENERIC_ERROR);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.ONLINE_USER_ALREADY_EXISTS);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseOnlineUser updateOnlineUser(long onlineUserID, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseOnlineUser SET userStatus = ?, customStatus = ?, lastUpdatedMS = ? WHERE onlineUserID = ?");
			ps.setInt(1, userStatus);
			ps.setString(2, customStatus);
			ps.setLong(3, lastUpdatedMS);
			ps.setLong(4, onlineUserID);
			
			if (ps.executeUpdate() > 0) {
				ps.close();
				ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseOnlineUser WHERE onlineUserID = ?");
				ps.setLong(1, onlineUserID);
				ResultSet result = ps.executeQuery();
				
				if (result.next()) {
					DatabaseOnlineUser onlineUser = new DatabaseOnlineUser(onlineUserID, result.getLong("userID"), result.getString("deviceToken"), userStatus, customStatus, lastUpdatedMS);
					ps.close();
					connectionModel.unlock();
					
					return onlineUser;
				}
			}
			
			ps.close();
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_ONLINE_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_BAD_NULL_ERROR) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseOnlineUser createOrUpdateOnlineUser(long userID, String deviceToken, int userStatus, String customStatus, long lastUpdatedMS) throws DatabaseException, GenericErrorCode {	
		try {
			return createOnlineUser(userID, deviceToken, userStatus, customStatus, lastUpdatedMS);
		} catch (GenericErrorCode ex) {
			if (ex.getErrorCode() == GenericErrorCode.ONLINE_USER_ALREADY_EXISTS) {
				for (DatabaseOnlineUser onlineUser : getOnlineUserByUserID(userID)) {
					if (onlineUser.getDeviceToken().equals(deviceToken)) {
						return updateOnlineUser(onlineUser.getOnlineUserID(), userStatus, customStatus, lastUpdatedMS);
					}
				}
			}
		}
		
		throw new DatabaseException("Failed to create or update a DatabaseOnlineUser.", DatabaseException.GENERIC_ERROR);
	}

	@Override
	public ArrayList<DatabaseOnlineUser> getAllOnlineUsers() throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseOnlineUser")) {
			ResultSet results = ps.executeQuery();
			ArrayList<DatabaseOnlineUser> onlineUsers = new ArrayList<>();
			
			while (results.next()) {
				onlineUsers.add(new DatabaseOnlineUser(results.getLong("onlineUserID"), results.getLong("userID"), results.getString("deviceToken"), results.getInt("userStatus"), 
						results.getString("customStatus"), results.getLong("lastUpdatedMS")));
			}
			
			connectionModel.unlock();
			return onlineUsers;
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupUser getGroupUser(long groupUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupUser WHERE groupUserID = ?")) {
			ps.setLong(1, groupUserID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupUser groupUser = new DatabaseGroupUser(result.getLong("groupUserID"), result.getLong("groupID"), result.getLong("userID"));
				connectionModel.unlock();
				
				return groupUser;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseGroupPermissionLevel getGroupPermissionLevel(long groupPermissionLevelID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseGroupPermissionLevel WHERE groupPermissionLevelID = ?")) {
			ps.setLong(1, groupPermissionLevelID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseGroupPermissionLevel permissionLevel = new DatabaseGroupPermissionLevel(groupPermissionLevelID, result.getLong("groupID"), result.getString("permissionName"), result.getBoolean("removable"), 
						result.getBoolean("allowBanGroupUsers"), result.getBoolean("allowKickGroupUsers"), result.getBoolean("allowManagePermissionLevels"), result.getBoolean("allowAccessAllGroupChats"), 
						result.getBoolean("allowAccessAllGroupFileRepositories"), result.getBoolean("isGroupOwner"));
				connectionModel.unlock();
				
				return permissionLevel;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_PERMISSION_LEVEL);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseSession getSessionBySessionID(long sessionID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseSession WHERE sessionID = ?")) {
			ps.setLong(1, sessionID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseSession session = new DatabaseSession(sessionID, result.getString("sessionToken"), result.getString("deviceToken"), result.getLong("userID"), result.getLong("expiresAtMS"), result.getLong("sessionCreatedAtMS"));
				connectionModel.unlock();
				
				return session;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseSession getSessionBySessionToken(String sessionToken) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseSession WHERE sessionID = ?")) {
			ps.setString(1, sessionToken);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseSession session = new DatabaseSession(result.getLong("sessionID"), sessionToken, result.getString("deviceToken"), result.getLong("userID"), result.getLong("expiresAtMS"), result.getLong("sessionCreatedAtMS"));
				connectionModel.unlock();
				
				return session;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteSessionBySessionToken(String sessionToken) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseSession WHERE sessionToken = ?")) {
			ps.setString(1, sessionToken);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_ONLINE_USER);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFileRepository getFileRepository(long fileRepositoryID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseFileRepository WHERE fileRepositoryID = ?")) {
			ps.setLong(1, fileRepositoryID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseFileRepository fileRepository = new DatabaseFileRepository(fileRepositoryID, result.getLong("fileRepositoryFileCount"));
				connectionModel.unlock();
				
				return fileRepository;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE_REPOSITORY);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteGroupUser(long groupUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseGroupUser WHERE groupUserID = ?")) {
			ps.setLong(1, groupUserID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_USER);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}
	
	@Override
	public DatabaseBannedGroupUser createBannedGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("INSERT INTO DatabaseBannedGroupUser VALUES (NULL, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, userID);
			ps.setLong(2, groupID);
			
			if (ps.executeUpdate() > 0) {
				ResultSet generated = ps.getGeneratedKeys();
				
				if (generated.next()) {
					DatabaseBannedGroupUser groupUser = new DatabaseBannedGroupUser(generated.getLong(1), userID, groupID);
					connectionModel.unlock();
					
					return groupUser;
				}
			}
			
			connectionModel.unlock();
			throw new DatabaseException("Failed to create a DatabaseOnlineUser.", DatabaseException.GENERIC_ERROR);
		} catch (SQLException ex) {
			connectionModel.unlock();
			
			if (ex.getErrorCode() == MYSQL_ER_NO_REFERENCED_ROW_2) {
				getUserByID(userID);
				getGroup(groupID);
				throw new DatabaseException("Failed to create a DatabaseOnlineUser.", DatabaseException.GENERIC_ERROR);
			}
			
			else if (ex.getErrorCode() == MYSQL_ER_DUP_ENTRY) {
				throw new GenericErrorCode(GenericErrorCode.BANNED_GROUP_USER_ALREADY_EXISTS);
			}
			
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseBannedGroupUser getBannedGroupUser(long userID, long groupID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseBannedGroupUser WHERE userID = ? AND groupId = ?")) {
			ps.setLong(1, userID);
			ps.setLong(2, groupID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseBannedGroupUser bannedGroupUser = new DatabaseBannedGroupUser(result.getLong("bannedGroupUserID"), result.getLong("userID"), result.getLong("groupID"));
				connectionModel.unlock();
				
				return bannedGroupUser;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseBannedGroupUser getBannedGroupUser(long bannedGroupUserID)
			throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseBannedGroupUser WHERE userID = ? AND groupId = ?")) {
			ps.setLong(1, bannedGroupUserID);
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseBannedGroupUser bannedGroupUser = new DatabaseBannedGroupUser(result.getLong("bannedGroupUserID"), result.getLong("userID"), result.getLong("groupID"));
				connectionModel.unlock();
				
				return bannedGroupUser;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public void deleteBannedGroupUser(long bannedGroupUserID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("DELETE FROM DatabaseBannedGroupUser WHERE bannedGroupUserID = ?")) {
			ps.setLong(1, bannedGroupUserID);
			
			if (ps.executeUpdate() == 0) {
				connectionModel.unlock();
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE);
			}
			
			connectionModel.unlock();
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseSession renewSession(long sessionID, long expiresAtMS) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try {
			PreparedStatement ps = connectionModel.getConnection().prepareStatement("UPDATE DatabaseBannedGroupUser SET expiresAtMS = ? WHERE sessionID = ?");
			ps.setLong(1, expiresAtMS);
			ps.setLong(2, sessionID);
			
			if (ps.executeUpdate() > 0) {
				ps.close();
				ps = connectionModel.getConnection().prepareStatement("SELECT * FROM DatabaseBannedGroupUser WHERE sessionID = ?");
				ps.setLong(1, sessionID);
				ResultSet result = ps.executeQuery();
				
				if (result.next()) {
					DatabaseSession session = new DatabaseSession(sessionID, result.getString("sessionToken"), result.getString("deviceToken"), result.getLong("userID"), result.getLong("expiresAtMS"), result.getLong("sessionCreatedAtMS"));
					ps.close();
					connectionModel.unlock();
					
					return session;
				}
			}
			
			ps.close();
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_ONLINE_USER);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}

	@Override
	public DatabaseFileRepositoryFile getFileRepositoryFileFromPrivateChatFileRepository(
			long privateChatFileRepositoryID, long fileRepositoryFileID) throws DatabaseException, GenericErrorCode {
		SQLConnectionPoolObject connectionModel = getAndLockConnection();
		
		try (PreparedStatement ps = connectionModel.getConnection().prepareStatement("SELECT DatabaseFileRepositoryFile* FROM DatabaseFileRepositoryFile "
				+ "INNER JOIN DatabasePrivateChatFileRepository ON DatabaseFileRepositoryFile.fileRepositoryID = DatabasePrivateChatFileRepository.fileRepositoryID "
				+ "WHERE DatabasePrivateChatFileRepository.privateChatFileRepositoryID = ? AND DatabaseFileRepositoryFile.fileRepositoryFileID = ?", Statement.RETURN_GENERATED_KEYS)) {
			ps.setLong(1, privateChatFileRepositoryID);
			ps.setLong(2, fileRepositoryFileID);
			
			ResultSet result = ps.executeQuery();
			
			if (result.next()) {
				DatabaseFileRepositoryFile fileRepositoryFile = new DatabaseFileRepositoryFile(result.getLong("fileRepositoryFileID"), result.getLong("fileRepositoryID"), result.getLong("fileID"), result.getLong("fileRepositoryFileNumber"), result.getLong("ownerUserID"), result.getString("description"));
				connectionModel.unlock();
				
				return fileRepositoryFile;
			}
			
			connectionModel.unlock();
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_FILE_REPOSITORY_FILE);
		} catch (SQLException ex) {
			connectionModel.unlock();
			throw new DatabaseException("MySQL returned a unexpected error code: " + ex.getErrorCode(), DatabaseException.GENERIC_ERROR);
		}
	}
}

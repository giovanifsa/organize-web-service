/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.webservices;

import java.io.InputStream;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import organize.server.models.GenericErrorCode;
import organize.server.modules.WebServiceManager;

@Path("/v1/file")
public class FileAPIv1 extends RESTfulAPI {
	public FileAPIv1(WebServiceManager webServiceManager) {
		super(webServiceManager);
	}
	
	@POST
	@Path("/getGroupFileRepositories")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupFileRepositories(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var groupFileRepositories = getFileManager().getGroupFileRepositories(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("groupID"));
					
					JSONArray answerData = new JSONArray();
					for (var groupFileRepository : groupFileRepositories) {
						JSONObject repository = new JSONObject();
						
						repository.put("groupFileRepositoryID", groupFileRepository.getGroupFileRepositoryID());
						repository.put("groupID", groupFileRepository.getGroupID());
						repository.put("fileRepositoryID", groupFileRepository.getFileRepositoryID());
						repository.put("groupRepositoryName", groupFileRepository.getGroupRepositoryName());
						repository.put("groupFileRepositoryFileCount", groupFileRepository.getGroupFileRepositoryFileCount());
						
						answerData.put(repository.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getGroupFileRepositoryFiles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupFileRepositoryFiles(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var groupFileRepositoryFiles = getFileManager().getGroupFileRepositoryFiles(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("groupFileRepositoryID"),
							requestProcessor.getRequestData().getLong("startingFileNumber"),
							requestProcessor.getRequestData().getLong("endingFileNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var groupFileRepositoryFile : groupFileRepositoryFiles) {
						JSONObject file = new JSONObject();
						
						file.put("groupFileRepositoryFileID", groupFileRepositoryFile.getGroupFileRepositoryFileID());
						file.put("groupFileRepositoryID", groupFileRepositoryFile.getGroupFileRepositoryID());
						file.put("fileRepositoryFileID", groupFileRepositoryFile.getFileRepositoryFileID());
						file.put("groupFileRepositoryFileNumber", groupFileRepositoryFile.getGroupFileRepositoryFileNumber());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getGroupFileRepositoryFileInformation")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupFileRepositoryFileInformation(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var file = getFileManager().getGroupFileRepositoryFileInformation(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("groupFileRepositoryFileID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("fileID", file.getFileID());
					answerData.put("fileName", file.getFileName());
					answerData.put("fileExtension", file.getFileExtension());
					answerData.put("ownerUserID", file.getOwnerUserID());
					answerData.put("filePartsCount", file.getFilePartsCount());
					answerData.put("fileSizeInBytes", file.getFileSizeInBytes());
					answerData.put("isFileComplete", file.getIsFileComplete());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getFilePartsFromGroupFileRepositoryFile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilePartsFromGroupFileRepositoryFile(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var fileParts = getFileManager().getFilePartsFromGroupFileRepositoryFile(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("groupFileRepositoryFileID"),
							requestProcessor.getRequestData().getLong("startingFileNumber"),
							requestProcessor.getRequestData().getLong("endingFileNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var filePart : fileParts) {
						JSONObject file = new JSONObject();
						
						file.put("filePartID", filePart.getFilePartID());
						file.put("fileID", filePart.getFileID());
						file.put("filePartNumber", filePart.getFilePartNumber());
						file.put("filePartBytes", new JSONArray(filePart.getFilePartBytes().toString()));
						file.put("filePartSize", filePart.getFilePartSize());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getUserFileRepository")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserFileRepository(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var fileRepository = getFileManager().getUserFileRepository(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("userFileRepositoryID", fileRepository.getUserFileRepositoryID());
					answerData.put("userID", fileRepository.getUserID());
					answerData.put("fileRepositoryID", fileRepository.getFileRepositoryID());
					answerData.put("fileRepositoryFileCount", fileRepository.getFileRepositoryFileCount());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getUserFileRepositoryFiles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserFileRepositoryFiles(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var userFileRepositoryFiles = getFileManager().getUserFileRepositoryFiles(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("startingFileNumber"),
							requestProcessor.getRequestData().getLong("endingFileNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var userFileRepositoryFile : userFileRepositoryFiles) {
						JSONObject file = new JSONObject();
						
						file.put("userFileRepositoryFileID", userFileRepositoryFile.getUserFileRepositoryFileID());
						file.put("userFileRepositoryID", userFileRepositoryFile.getUserFileRepositoryID());
						file.put("fileRepositoryFileID", userFileRepositoryFile.getFileRepositoryFileID());
						file.put("userFileRepositoryFileNumber", userFileRepositoryFile.getUserFileRepositoryFileNumber());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getFilePartsFromUserFileRepositoryFile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilePartsFromUserFileRepositoryFile(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var fileParts = getFileManager().getFilePartsFromUserFileRepositoryFile(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("userFileRepositoryFileID"),
							requestProcessor.getRequestData().getLong("startingFileNumber"),
							requestProcessor.getRequestData().getLong("endingFileNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var filePart : fileParts) {
						JSONObject file = new JSONObject();
						
						file.put("filePartID", filePart.getFilePartID());
						file.put("fileID", filePart.getFileID());
						file.put("filePartNumber", filePart.getFilePartNumber());
						file.put("filePartBytes", new JSONArray(filePart.getFilePartBytes().toString()));
						file.put("filePartSize", filePart.getFilePartSize());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getUserFileRepositoryFileInformation")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserFileRepositoryFileInformation(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var file = getFileManager().getUserFileRepositoryFileInformation(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("userFileRepositoryFileID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("fileID", file.getFileID());
					answerData.put("fileName", file.getFileName());
					answerData.put("fileExtension", file.getFileExtension());
					answerData.put("ownerUserID", file.getOwnerUserID());
					answerData.put("filePartsCount", file.getFilePartsCount());
					answerData.put("fileSizeInBytes", file.getFileSizeInBytes());
					answerData.put("isFileComplete", file.getIsFileComplete());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getPrivateChatFileRepositories")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrivateChatFileRepositories(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var privateChatFileRepositories = getFileManager().getPrivateChatFileRepositories(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("otherUserID"));
					
					JSONArray answerData = new JSONArray();
					for (var privateChatFileRepository : privateChatFileRepositories) {
						JSONObject file = new JSONObject();
						
						file.put("privateChatFileRepositoryID", privateChatFileRepository.getPrivateChatFileRepositoryID());
						file.put("privateChatID", privateChatFileRepository.getPrivateChatID());
						file.put("fileRepositoryID", privateChatFileRepository.getFileRepositoryID());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getPrivateChatFileRepositoryFiles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrivateChatFileRepositoryFiles(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var privateChatFileRepositoryFiles = getFileManager().getPrivateChatFileRepositoryFiles(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("otherUserID"),
							requestProcessor.getRequestData().getLong("privateChatFileRepositoryID"),
							requestProcessor.getRequestData().getLong("startingFileNumber"),
							requestProcessor.getRequestData().getLong("endingFileNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var privateChatFileRepositoryFile : privateChatFileRepositoryFiles) {
						JSONObject fileRepositoryFile = new JSONObject();
						
						fileRepositoryFile.put("fileRepositoryFileID", privateChatFileRepositoryFile.getFileRepositoryFileID());
						fileRepositoryFile.put("fileRepositoryID", privateChatFileRepositoryFile.getFileRepositoryID());
						fileRepositoryFile.put("ownerUserID", privateChatFileRepositoryFile.getOwnerUserID());
						fileRepositoryFile.put("fileID", privateChatFileRepositoryFile.getFileID());
						fileRepositoryFile.put("fileRepositoryFileNumber", privateChatFileRepositoryFile.getFileRepositoryFileNumber());
						fileRepositoryFile.put("description", privateChatFileRepositoryFile.getDescription());
						
						answerData.put(fileRepositoryFile.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getPrivateChatFileRepositoryFileParts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrivateChatFileRepositoryFileParts(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					var fileParts = getFileManager().getPrivateChatFileRepositoryFileParts(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"),
							requestProcessor.getRequestData().getLong("otherUserID"),
							requestProcessor.getRequestData().getLong("privateChatFileRepositoryID"),
							requestProcessor.getRequestData().getLong("fileRepositoryFileID"),
							requestProcessor.getRequestData().getLong("startingPartNumber"),
							requestProcessor.getRequestData().getLong("endingPartNumber"));
					
					JSONArray answerData = new JSONArray();
					for (var filePart : fileParts) {
						JSONObject file = new JSONObject();
						
						file.put("filePartID", filePart.getFilePartID());
						file.put("fileID", filePart.getFileID());
						file.put("filePartNumber", filePart.getFilePartNumber());
						file.put("filePartBytes", new JSONArray(filePart.getFilePartBytes().toString()));
						file.put("filePartSize", filePart.getFilePartSize());
						
						answerData.put(file.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}

	@Override
	public String getModuleName() {
		return "FileAPI";
	}

	@Override
	public void start() {
		
	}

	@Override
	public void stop() {
		
	}
}

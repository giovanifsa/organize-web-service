/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.webservices;

import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.cj.xdevapi.JsonArray;

import organize.server.models.GenericErrorCode;
import organize.server.models.StaticData;
import organize.server.models.database.DatabaseChat;
import organize.server.models.database.DatabaseChatMessage;
import organize.server.models.database.DatabaseGroup;
import organize.server.models.database.DatabaseGroupChat;
import organize.server.models.database.DatabaseGroupInvite;
import organize.server.models.database.DatabaseGroupUser;
import organize.server.models.database.DatabaseOnlineUser;
import organize.server.models.database.DatabaseUser;
import organize.server.modules.NetworkSpamBlockerManager;
import organize.server.modules.WebServiceManager;

@Path("/v1/community")
public class CommunityAPIv1 extends RESTfulAPI {
	
	public CommunityAPIv1(WebServiceManager webServiceManager) {
		super(webServiceManager);
	}
	
	@POST
	@Path("/getPrivateChatInformation")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrivateChatInformation(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseChat chatInformation = getCommunityManager().getPrivateChatInformation(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("otherUserID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("chatID", chatInformation.getChatID());
					answerData.put("messageCount", chatInformation.getMessageCount());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/sendMessageToUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendMessageToUser(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseChatMessage chatMessage = getCommunityManager().sendMessageToUser(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("otherUserID"), 
							requestProcessor.getRequestData().getString("message"), 
							requestProcessor.getRequestData().getBoolean("isQuote"), 
							requestProcessor.getRequestData().getLong("quotedOwnerUserID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("chatMessageID", chatMessage.getChatMessageID());
					answerData.put("chatID", chatMessage.getChatID());
					answerData.put("ownerUserID", chatMessage.getOwnerUserID());
					answerData.put("message", chatMessage.getMessage());
					answerData.put("isQuote", chatMessage.isQuote());
					answerData.put("quotedOwnerUserID", chatMessage.getQuotedOwnerUserID());
					answerData.put("chatMessageNumber", chatMessage.getChatMessageNumber());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getMessagesFromPrivateChat")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessagesFromPrivateChat(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					ArrayList<DatabaseChatMessage> chatMessageList = getCommunityManager().getMessagesFromPrivateChat(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("otherUserID"), 
							requestProcessor.getRequestData().getLong("startingMessageNumber"), 
							requestProcessor.getRequestData().getLong("endingMessageNumber"));
					
					JSONArray answerData = new JSONArray();
					for (DatabaseChatMessage chatMessage : chatMessageList) {
						JSONObject message = new JSONObject();
						message.put("chatMessageID", chatMessage.getChatMessageID());
						message.put("chatID", chatMessage.getChatID());
						message.put("ownerUserID", chatMessage.getOwnerUserID());
						message.put("message", chatMessage.getMessage());
						message.put("isQuote", chatMessage.isQuote());
						message.put("quotedOwnerUserID", chatMessage.getQuotedOwnerUserID());
						message.put("chatMessageNumber", chatMessage.getChatMessageNumber());
						answerData.put(message.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/sendMessageToGroupChat")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendMessageToGroupChat(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseChatMessage sentToGroupChat = getCommunityManager().sendMessageToGroupChat(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupChatID"), 
							requestProcessor.getRequestData().getString("message"), 
							requestProcessor.getRequestData().getBoolean("isQuote"), 
							requestProcessor.getRequestData().getLong("quotedOwnerUserID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("chatMessageID", sentToGroupChat.getChatMessageID());
					answerData.put("chatID", sentToGroupChat.getChatID());
					answerData.put("ownerUserID", sentToGroupChat.getOwnerUserID());
					answerData.put("message", sentToGroupChat.getMessage());
					answerData.put("isQuote", sentToGroupChat.isQuote());
					answerData.put("quotedOwnerUserID", sentToGroupChat.getQuotedOwnerUserID());
					answerData.put("chatMessageNumber", sentToGroupChat.getChatMessageNumber());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/userHaveGroupChatAccessPermission")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response userHaveGroupChatAccessPermission(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					boolean userHaveChatAccessPermission = getCommunityManager().userHaveGroupChatAccessPermission(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupChatID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("userHaveGroupChatAccessPermission", userHaveChatAccessPermission);
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getGroupChats")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupChats(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					ArrayList<DatabaseGroupChat> groupChats = getCommunityManager().getGroupChats(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupID"));
					
					JSONArray answerData = new JSONArray();
					
					for (DatabaseGroupChat groupChat : groupChats) {
						JSONObject arrayObject = new JSONObject();
						arrayObject.put("groupChatID", groupChat.getGroupChatID());
						arrayObject.put("groupID", groupChat.getGroupID());
						arrayObject.put("chatID", groupChat.getChatID());
						arrayObject.put("chatName", groupChat.getChatName());
						
						answerData.put(arrayObject.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getMessagesFromGroupChat")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessagesFromGroupChat(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					ArrayList<DatabaseChatMessage> groupChatMessages = getCommunityManager().getMessagesFromGroupChat(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupChatID"), 
							requestProcessor.getRequestData().getLong("startingMessageNumber"), 
							requestProcessor.getRequestData().getLong("endingMessageNumber"));
					
					JSONArray answerData = new JSONArray();
					
					for (DatabaseChatMessage chatMessage : groupChatMessages) {
						JSONObject arrayObject = new JSONObject();
						arrayObject.put("chatMessageID", chatMessage.getChatMessageID());
						arrayObject.put("chatID", chatMessage.getChatID());
						arrayObject.put("ownerUserID", chatMessage.getOwnerUserID());
						arrayObject.put("message", chatMessage.getMessage());
						arrayObject.put("isQuote", chatMessage.isQuote());
						arrayObject.put("quotedOwnerUserID", chatMessage.getQuotedOwnerUserID());
						arrayObject.put("chatMessageNumber", chatMessage.getChatMessageNumber());
						
						answerData.put(arrayObject.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/createGroup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createGroup(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseGroup createdGroup = getCommunityManager().createGroup(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getString("groupName"), 
							requestProcessor.getRequestData().getInt("groupAccess"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("groupID", createdGroup.getGroupID());
					answerData.put("groupName", createdGroup.getGroupName());
					answerData.put("ownerUserID", createdGroup.getOwnerUserID());
					answerData.put("groupAccess", createdGroup.getGroupAccess());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/deleteGroup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteGroup(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					getCommunityManager().deleteGroup(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupID"));
					
					jsonResponse = requestProcessor.getAnswerResult();
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/setOnlineStatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setOnlineStatus(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseOnlineUser onlineUser = getCommunityManager().setOnlineStatus(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getInt("userStatus"), 
							requestProcessor.getRequestData().getString("customStatus"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("onlineUserID", onlineUser.getOnlineUserID());
					answerData.put("userStatus", onlineUser.getUserStatus());
					answerData.put("customStatus", onlineUser.getCustomStatus());
					answerData.put("deviceToken", onlineUser.getDeviceToken());
					answerData.put("userID", onlineUser.getUserID());
					answerData.put("lastUpdatedMS", onlineUser.getLastUpdatedMS());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getGroupInvites")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupInvites(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					ArrayList<DatabaseGroupInvite> groupInvites = getCommunityManager().getGroupInvites(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"));
					
					JSONArray answerData = new JSONArray();
					
					for (DatabaseGroupInvite invite : groupInvites) {
						JSONObject arrayObject = new JSONObject();
						arrayObject.put("groupInviteID", invite.getGroupInviteID());
						arrayObject.put("groupID", invite.getGroupID());
						arrayObject.put("invitingUserID", invite.getInvitingUserID());
						arrayObject.put("invitedUserID", invite.getInvitedUserID());
						answerData.put(arrayObject.toString());
					}
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/inviteUserToGroup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inviteUserToGroup(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseGroupInvite groupInvite = getCommunityManager().inviteUserToGroup(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("userToInviteID"), 
							requestProcessor.getRequestData().getLong("groupID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("groupInviteID", groupInvite.getGroupInviteID());
					answerData.put("invitedUserID", groupInvite.getInvitedUserID());
					answerData.put("invitingUserID", groupInvite.getInvitingUserID());
					answerData.put("groupID", groupInvite.getGroupID());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/acceptGroupInvite")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptGroupInvite(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseGroupUser newGroupUser = getCommunityManager().acceptGroupInvite(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupInviteID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("groupUserID", newGroupUser.getGroupUserID());
					answerData.put("groupID", newGroupUser.getGroupID());
					answerData.put("userID", newGroupUser.getUserID());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getUserStatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserStatus(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseOnlineUser onlineUser = getCommunityManager().getUserStatus(
							requestProcessor.getRequestData().getLong("userID")).removeSensitiveData();
					
					JSONObject answerData = new JSONObject();
					answerData.put("userStatus", onlineUser.getUserStatus());
					answerData.put("customStatus", onlineUser.getCustomStatus());
					answerData.put("userID", onlineUser.getUserID());
					
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/removeUserFromGroup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeUserFromGroup(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					getCommunityManager().removeUserFromGroup(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupUserID"));
					
					jsonResponse = requestProcessor.getAnswerResult();
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/banUserFromGroup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response banUserFromGroup(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("serverEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					getCommunityManager().removeUserFromGroup(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("groupUserID"));
					
					jsonResponse = requestProcessor.getAnswerResult();
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}

	@Override
	public String getModuleName() {
		return "CommunityAPIv1";
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isStarted() {
		// TODO Auto-generated method stub
		return false;
	}
}

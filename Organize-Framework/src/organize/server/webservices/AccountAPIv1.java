/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.webservices;

import java.io.InputStream;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import organize.server.models.GenericErrorCode;
import organize.server.models.database.DatabaseSession;
import organize.server.models.database.DatabaseUser;
import organize.server.modules.NetworkSpamBlockerManager;
import organize.server.modules.WebServiceManager;

@Path("/v1/account")
public class AccountAPIv1 extends RESTfulAPI {
	
	public AccountAPIv1(WebServiceManager webServiceManager) {
		super(webServiceManager);
	}
	
	@POST
	@Path("/createAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createAccount(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseUser user = getAccountManager().createAccount(
							requestProcessor.getRequestData().getString("username"),
							requestProcessor.getRequestData().getString("password"), 
							requestProcessor.getRequestData().getString("email"), 
							requestProcessor.getRequestData().getString("displayName"), 
							requestProcessor.getRequestData().getString("recoverQuestion"), 
							requestProcessor.getRequestData().getString("recoverQuestionAnswer"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("userID", user.getUserID());
					answerData.put("username", user.getUsername());
					answerData.put("email", user.getEmail());
					answerData.put("displayName", user.getDisplayName());
					answerData.put("selectedProfilePictureID", user.getSelectedProfilePictureID());
					answerData.put("profilePictureCount", user.getProfilePictureCount());
					answerData.put("groupOwnershipCount", user.getGroupOwnershipCount());
					answerData.put("isAccountDisabled", user.isAccountDisabled());
					answerData.put("haveSelectedProfilePicture", user.isHaveSelectedProfilePicture());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					ex.printStackTrace();
					
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException | IllegalArgumentException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getAccountRecoverQuestion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAccountRecoverQuestion(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(Base64.getDecoder().decode(jsonRequest.getString("base64ServerEncryptedClientCryptography")))));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					
					JSONObject answerData = new JSONObject();
					answerData.put("recoverQuestion", getAccountManager().getAccountRecoverQuestion(
							requestProcessor.getRequestData().getString("username"), 
							requestProcessor.getRequestData().getString("email")));
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorcode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorcode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/recoverAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response recoverAccount(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseUser user = getAccountManager().recoverAccount(
							requestProcessor.getRequestData().getString("username"), 
							requestProcessor.getRequestData().getString("email"), 
							requestProcessor.getRequestData().getString("newPassword"), 
							requestProcessor.getRequestData().getString("recoverQuestionAnswer"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("userID", user.getUserID());
					answerData.put("username", user.getUsername());
					answerData.put("email", user.getEmail());
					answerData.put("displayName", user.getDisplayName());
					answerData.put("selectedProfilePictureID", user.getSelectedProfilePictureID());
					answerData.put("profilePictureCount", user.getProfilePictureCount());
					answerData.put("groupOwnershipCount", user.getGroupOwnershipCount());
					answerData.put("isAccountDisabled", user.isAccountDisabled());
					answerData.put("haveSelectedProfilePicture", user.isHaveSelectedProfilePicture());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_USER) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/logIntoAccount")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logIntoAccount(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseSession session = getAccountManager().logIntoAccount(
							requestProcessor.getRequestData().getString("username"), 
							requestProcessor.getRequestData().getString("password"), 
							requestProcessor.getRequestData().getString("deviceToken"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("sessionID", session.getSessionID());
					answerData.put("sessionToken", session.getSessionToken());
					answerData.put("deviceToken", session.getDeviceToken());
					answerData.put("userID", session.getUserID());
					answerData.put("expiresAtMS", session.getExpiresAt());
					answerData.put("sessionCreatedAtMS", session.getSessionCreatedAtMS());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_USER) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/endSession")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response endSession(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					getAccountManager().endSession(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"));
					
					jsonResponse = requestProcessor.getAnswerResult();
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/endAllAccountSessions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response endAllSessions(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					getAccountManager().endAllAccountSessions(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"));
					
					jsonResponse = requestProcessor.getAnswerResult();
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/getUserInformation")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInformation(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseUser userInformation = getAccountManager().getUserInformation(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken"), 
							requestProcessor.getRequestData().getLong("userID"));
					
					JSONObject answerData = new JSONObject();
					answerData.put("userID", userInformation.getUserID());
					answerData.put("username", userInformation.getUsername());
					answerData.put("email", userInformation.getEmail());
					answerData.put("displayName", userInformation.getDisplayName());
					answerData.put("selectedProfilePictureID", userInformation.getSelectedProfilePictureID());
					answerData.put("profilePictureCount", userInformation.getProfilePictureCount());
					answerData.put("groupOwnershipCount", userInformation.getGroupOwnershipCount());
					answerData.put("isAccountDisabled", userInformation.isAccountDisabled());
					answerData.put("haveSelectedProfilePicture", userInformation.isHaveSelectedProfilePicture());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}
		
		return Response.ok(jsonResponse.toString()).build();
	}
	
	@POST
	@Path("/renewSession")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response renewSession(@Context HttpServletRequest request, InputStream jsonInputStream) {
		JSONObject jsonResponse = new JSONObject();
		
		if (isStarted()) {
			try {
				try {
					getNetworkSpamBlockerManager().validateConnection(request.getRemoteAddr());
					
					JSONObject jsonRequest = new JSONObject(receiveStringFromInputStream(jsonInputStream));
					JSONObject jsonRequestCryptography = new JSONObject(new String(
							getCryptographyManager().
							decryptWithServerPrivateKey(jsonRequest.getString("base64ServerEncryptedClientCryptography").getBytes())));
					
					RequestProcessorV1 requestProcessor = new RequestProcessorV1(jsonRequest, jsonRequestCryptography);
					DatabaseSession renewedSession = getAccountManager().renewSession(
							requestProcessor.getRequestData().getString("sessionToken"), 
							requestProcessor.getRequestData().getString("deviceToken")); 
					
					JSONObject answerData = new JSONObject();
					answerData.put("sessionID", renewedSession.getSessionID());
					answerData.put("sessionToken", renewedSession.getSessionToken());
					answerData.put("deviceToken", renewedSession.getDeviceToken());
					answerData.put("userID", renewedSession.getUserID());
					answerData.put("expiresAtMS", renewedSession.getExpiresAt());
					answerData.put("sessionCreatedAtMS", renewedSession.getSessionCreatedAtMS());
					jsonResponse = requestProcessor.getEncryptedAnswerResult(answerData);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_SESSION) {
						getNetworkSpamBlockerManager().addHeavyWeightToConnection(request.getRemoteAddr());
					}
					
					else if (ex.getErrorCode() != GenericErrorCode.SERVER_ERROR && ex.getErrorCode() != GenericErrorCode.API_DISABLED_ERROR) {
						getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					}
					
					jsonResponse.put("errorCode", ex.getErrorCode());
				} catch (JSONException ex) {
					getNetworkSpamBlockerManager().addSmallWeightToConnection(request.getRemoteAddr());
					
					jsonResponse.put("errorCode", GenericErrorCode.API_MALFORMED_REQUEST);
				}
			} catch (GenericErrorCode ex) {
				jsonResponse.put("errorCode", ex.getErrorCode());
			}
		}
		
		else {
			jsonResponse.put("errorCode", GenericErrorCode.SERVICE_NOT_AVAILABLE);
		}

		return Response.ok(jsonResponse.toString()).build();
	}

	@Override
	public String getModuleName() {
		return "AccountAPI";
	}

	@Override
	public void start() {
		setStarted(true);
	}

	@Override
	public void stop() {
		setStarted(false);
	}
}

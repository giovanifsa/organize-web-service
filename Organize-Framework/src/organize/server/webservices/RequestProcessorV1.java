/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.webservices;

import java.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import organize.server.models.Cryptographer;
import organize.server.models.GenericErrorCode;

public class RequestProcessorV1 {
	private final JSONObject requestJson;
	private final JSONObject answerJson;
	private final Cryptographer cryptographer;
	private final JSONObject requestData;
	
	public RequestProcessorV1(JSONObject requestJson, JSONObject jsonRequestCryptography) throws GenericErrorCode, JSONException {
		this.requestJson = requestJson;
		this.answerJson = new JSONObject();
		
		cryptographer = new Cryptographer(
				Base64.getDecoder().decode(jsonRequestCryptography.getString("base64key")),
				jsonRequestCryptography.getString("keyType"), 
				jsonRequestCryptography.getString("blockCipherMode"), 
				Base64.getDecoder().decode(jsonRequestCryptography.getString("base64ivSpec")),
				jsonRequestCryptography.getString("padding"));
		
		requestData = new JSONObject(new String(cryptographer.decryptWithKey(Base64.getDecoder().decode(requestJson.getString("base64ClientEncryptedRequestData")))));
	}
	
	public JSONObject getRequestData() {
		return requestData;
	}
	
	public JSONObject getEncryptedAnswerResult(JSONObject answerData) throws JSONException, GenericErrorCode {
		JSONObject jsonAnswer = new JSONObject();
		jsonAnswer.put("errorCode", GenericErrorCode.OK);
		jsonAnswer.put("base64ClientEncryptedAnswerData", Base64.getEncoder().encodeToString(cryptographer.encryptWithKey(answerData.toString().getBytes())));
		return jsonAnswer;
	}
	
	public JSONObject getEncryptedAnswerResult(JSONArray answerData) throws JSONException, GenericErrorCode {
		JSONObject jsonAnswer = new JSONObject();
		jsonAnswer.put("errorCode", GenericErrorCode.OK);
		jsonAnswer.put("base64ClientEncryptedAnswerData", Base64.getEncoder().encodeToString(cryptographer.encryptWithKey(answerData.toString().getBytes())));
		return jsonAnswer;
	}
	
	public JSONObject getAnswerResult() {
		JSONObject jsonAnswer = new JSONObject();
		jsonAnswer.put("errorCode", GenericErrorCode.OK);
		return jsonAnswer;
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Context;

import org.glassfish.jersey.server.ResourceConfig;

import organize.server.models.StaticData;
import organize.server.modules.AccountManager;
import organize.server.modules.CommunityManager;
import organize.server.modules.ConfigurationManager;
import organize.server.modules.CryptographyManager;
import organize.server.modules.DatabaseManager;
import organize.server.modules.FileManager;
import organize.server.modules.LogManager;
import organize.server.modules.NetworkSpamBlockerManager;
import organize.server.modules.RconManager;
import organize.server.modules.WebServiceManager;

/**
 * Entry point for the server application.
 * @author giovani
 */
@ApplicationPath("api")
public class ServerMain extends ResourceConfig {
	public static String thread_poolsize[] = {"thread_poolsize", "20"};
	
	//Global Data
	private final ServletContext servletContext;
	private final SecureRandom secureRandom;
	private ScheduledThreadPoolExecutor threadPool;
	
	//Modules
	private AccountManager accountManager;
	private CommunityManager communityManager;
	private ConfigurationManager configurationManager;
	private CryptographyManager cryptographyManager;
	private DatabaseManager databaseController;
	private FileManager fileManager;
	private LogManager logManager;
	private NetworkSpamBlockerManager networkSpamBlockerManager;
	private RconManager rconManager;
	private WebServiceManager webServiceManager;
	
	private boolean loggerCreationPassed = false;
	
	public ServerMain(@Context ServletContext servletContext) {
		this.servletContext = servletContext;
		this.secureRandom = new SecureRandom();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			stop();
		}));
		
		start();
	}
	
	public void start() {
		System.out.println(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS - ").format(getDate()) + "[Organize] Starting up from \"" + servletContext.getRealPath("") + "\".");
		
		//Configuration module startup
		configurationManager = new ConfigurationManager(this);
		configurationManager.setConfigFile(new File(StaticData.CONFIGURATION_FILE));
		configurationManager.readFromConfigFile();
		
		//Thread pool startup
		int poolsize = Integer.valueOf(thread_poolsize[1]);
		
		try {
			poolsize = Integer.valueOf(configurationManager.getValue(thread_poolsize[0], thread_poolsize[1]));
			
			if (poolsize <= 0) {
				poolsize = Integer.valueOf(thread_poolsize[1]);
			}
		}
		catch (NumberFormatException ex) {}
		
		threadPool = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(poolsize);
		
		//Logging module startup		
		logManager = new LogManager(this);
		logManager.start();
		
		//Database module startup
		databaseController = new DatabaseManager(this);
		databaseController.start();

		//Other modules startup
		cryptographyManager = new CryptographyManager(this);
		cryptographyManager.start();
		
		fileManager = new FileManager(this);
		fileManager.start();
		
		accountManager = new AccountManager(this);
		accountManager.start();
		
		communityManager = new CommunityManager(this);
		communityManager.start();
		
		networkSpamBlockerManager = new NetworkSpamBlockerManager(this);
		networkSpamBlockerManager.start();
		
		webServiceManager = new WebServiceManager(this);
		webServiceManager.start();
		
		rconManager = new RconManager(this);
		rconManager.start();
	}
	
	public void stop() {
		
	}
	
	public ServletContext getServletContext() {
		return servletContext;
	}
	
	public ScheduledThreadPoolExecutor getThreadPool() {
		return threadPool;
	}
	
	public Date getDate() {
		return new Date();
	}
	
	public boolean isLoggingAvaiable() {
		return logManager != null && logManager.isStarted();
	}
	
	/**
	 * Method for handling runtime errors.
	 * @param owner Creator of the error.
	 * @param message Description of the error.
	 * @param fatal False if it's recoverable, true and the server will end.
	 */
	public void serverRuntimeError(String owner, String message, boolean fatal) {
		String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS - ").format(getDate());
		
		if (isLoggingAvaiable()) {
			logManager.logError(time, owner, message, fatal);
		}
		
		else if (loggerCreationPassed){
			System.err.println(time + "[Organize] NOTE: LOG SERVICE UNAVAILABLE.");
		}
		
		System.err.println(time + "[Organize] " + (fatal ? "FATAL: " : "ERROR: ") + owner + " module reports the following problem: " + message);
	}
	
	public void serverRuntimeWarning(String owner, String message) {
		String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS - ").format(getDate());
		
		if (isLoggingAvaiable()) {
			logManager.logWarning(time, owner, message);
		}
		
		else if (loggerCreationPassed){
			System.out.println(time + "[Organize] NOTE: LOG SERVICE UNAVAILABLE.");
		}
		
		System.out.println(time + "[Organize] " + owner + " module reports the following problem: " + message);
	}
	
	public void serverRuntimeInformation(String owner, String message) {
		String time = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS - ").format(getDate());
		
		if (isLoggingAvaiable()) {
			logManager.logInfo(time, owner, message);
		}
		
		else if (loggerCreationPassed){
			System.out.println(time + "[Organize] NOTE: LOG SERVICE UNAVAILABLE.");
		}
		
		System.out.println(time + "[Organize] " + owner + " module reports the following information: " + message);
	}
	
	public SecureRandom getSecureRandom() {
		return secureRandom;
	}
	
	public AccountManager getAccountManager() {
		return accountManager;
	}
	
	public CommunityManager getCommunityManager() {
		return communityManager;
	}
	
	public ConfigurationManager getConfigurationManager() {
		return configurationManager;
	}
	
	public CryptographyManager getCryptographyManager() {
		return cryptographyManager;
	}
	
	public DatabaseManager getDatabaseManager() {
		return databaseController;
	}
	
	public FileManager getFileManager() {
		return fileManager;
	}
	
	public LogManager getLogManager() {
		return logManager;
	}
	
	public NetworkSpamBlockerManager getNetworkSpamBlockerManager() {
		return networkSpamBlockerManager;
	}
	
	public RconManager getRconManager() {
		return rconManager;
	}
	
	public WebServiceManager getWebServiceManager() {
		return webServiceManager;
	}
}

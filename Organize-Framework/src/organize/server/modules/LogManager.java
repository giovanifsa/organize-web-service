/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import organize.server.ServerMain;
import organize.server.models.StaticData;

public class LogManager extends ServerModule {
	public static String log_logdir[] = {"log_logdir", StaticData.ORGANIZE_FOLDER + "logs" + File.separator};
	private File logFile = null;
	
	private ConcurrentLinkedQueue<String> queue = null;
	private volatile BufferedWriter bufferedWriter = null;
	private ScheduledFuture<?> logProcessorTask = null;
	
	private volatile boolean available = false;
	private volatile boolean processingAvailable = false;
	
	public LogManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	public void logError(String time, String owner, String message, boolean fatal) {
		if (available) {
			String str = time + ("[Organize] " + (fatal ? "ERROR: " : "FATAL: ") + owner + " module reports the following problem: " + message);
			queue.add(str);
		}
	}
	
	public void logInfo(String time, String owner, String message) {
		if (available) {
			String str = time + "[Organize] " + "INFO: " + owner + " module reports the following information: " + message;
			queue.add(str);
		}
		
	}
	
	public void logWarning(String time, String owner, String message) {
		if (available) {
			String str = time + "[Organize] " + "WARNING: " + owner + " module reports the following problem: " + message;
			queue.add(str);
		}
	}
	
	public synchronized void closeLog() {
		if (available) {
			available = false;
			logProcessorTask.cancel(false);
		}
	}
	
	public synchronized File getLogFile() {
		return logFile;
	}

	@Override
	public String getModuleName() {
		return "Log";
	}

	@Override
	public synchronized void start() {
		if (!isStarted()) {
			logRuntimeInformation("Initializing...");
			
			String logDir = getConfigurationManager().getValue(log_logdir[0], log_logdir[1]);
			
			if (!logDir.endsWith(File.separator)) {
				logDir += File.separator;
			}
			
			File logDirFile = new File(logDir);
			logFile = new File(logDir + getDate().getTime() + "_log.txt");
			
			logRuntimeInformation("Writing logs to file \"" + logFile.getAbsolutePath() + "\".");
			
			try {
				logDirFile.mkdirs();
				logFile.createNewFile();
				
				bufferedWriter = new BufferedWriter(new FileWriter(logFile));
				
				queue = new ConcurrentLinkedQueue<>();
				
				processingAvailable = true;
				logProcessorTask = getScheduledThreadPool().scheduleAtFixedRate(() -> {
					if (processingAvailable) {
						for (String str : queue) {
							try {
								bufferedWriter.write(str + "\n");
								bufferedWriter.flush();
								queue.remove(str);
							} catch (IOException e) {
								processingAvailable = false;
								closeLog();
							}
						}
					}
				}, 500, 500, TimeUnit.MILLISECONDS);
				
				available = true;
				
			} catch (IOException | SecurityException ex) {
				logRuntimeError("Failed to initialize: " + ex.getMessage(), false);
			}
		}
		
	}

	@Override
	public synchronized void stop() {
		processingAvailable = false;
		available = false;
		
		if (logProcessorTask != null) {
			logProcessorTask.cancel(false);
			logProcessorTask = null;
		}
		
		if (bufferedWriter != null) {
			try {
				bufferedWriter.close();
			} catch (IOException e) {}
			
			bufferedWriter = null;
		}
		
		if (queue != null) {
			queue.clear();
			queue = null;
		}
	}

	@Override
	public boolean isStarted() {
		return available;
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import organize.server.ServerMain;
import organize.server.models.GenericErrorCode;
import organize.server.models.IPAddressConnection;
import organize.server.models.NotStartedException;
import organize.server.models.configuration.ConfigurationListener;

public class NetworkSpamBlockerManager extends ServerModule {
	public static final String networkspamblockermanager_maxconnectionweight[] = {"networkspamblockermanager_maxconnectionweight", "1"};
	public static final String networkspamblockermanager_connectionweightcleanupms[] = {"networkspamblockermanager_connectionweightcleanupms", "60000"};
	public static final String networkspamblockermanager_garbagecollectorupdatems[] = {"networkspamblockermanager_garbagecollectorupdatems", "20000"};
	public static final String networkspamblockermanager_heavyweight[] = {"networkspamblockermanager_passworderrorweight", "1000"};
	public static final String networkspamblockermanager_mediumweight[] = {"networkspamblockermanager_passworderrorweight", "500"};
	public static final String networkspamblockermanager_smallweight[] = {"networkspamblockermanager_commonerrorweight", "10"};
	
	private ConcurrentLinkedQueue<IPAddressConnection> connectionList;
	private ScheduledFuture<?> garbageCollector;
	private final ConfigurationListener confListener = new ConfigurationListener() {
		@Override
		public void onRemove(String var, String lastValue) {}
		
		@Override
		public void onCreate(String var, String value) {}
		
		@Override
		public void onChange(String var, String oldValue, String newValue) {
			if (isStarted()) {
				updateScheduledGarbageCollector();
			}
		}
	};

	public NetworkSpamBlockerManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	private void updateScheduledGarbageCollector() {
		if (garbageCollector != null) {
			garbageCollector.cancel(false);
		}
		
		long collectorms = getConfigurationManager().getLongValue(networkspamblockermanager_garbagecollectorupdatems[0], networkspamblockermanager_garbagecollectorupdatems[1]);
		garbageCollector = getScheduledThreadPool().scheduleAtFixedRate(() -> {
			for (IPAddressConnection connection : connectionList) {
				connection.clearOld(getDate().getTime() - getConfigurationManager().getLongValue(networkspamblockermanager_connectionweightcleanupms[0],
						networkspamblockermanager_connectionweightcleanupms[1]));
			}
		}, collectorms, collectorms, TimeUnit.MILLISECONDS);
	}
	
	public synchronized void addWeightToConnection(String IPAddress, long weight) throws NotStartedException {
		if (isStarted()) {
			for (IPAddressConnection connection : connectionList) {
				if (connection.getIPAddress().equals(IPAddress)) {
					connection.addConnection(getDate().getTime(), weight);
					return;
				}
			}
			
			IPAddressConnection connection = new IPAddressConnection(IPAddress);
			connection.addConnection(getDate().getTime(), weight);
			connectionList.add(connection);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public synchronized void addHeavyWeightToConnection(String IPAddress) throws NotStartedException {
		addWeightToConnection(IPAddress, getConfigurationManager().getLongValue(networkspamblockermanager_heavyweight[0], networkspamblockermanager_heavyweight[1]));
	}
	
	public synchronized void addMediumWeightToConnection(String IPAddress) throws NotStartedException {
		addWeightToConnection(IPAddress, getConfigurationManager().getLongValue(networkspamblockermanager_mediumweight[0], networkspamblockermanager_mediumweight[1]));
	}
	
	public synchronized void addSmallWeightToConnection(String IPAddress) throws NotStartedException {
		addWeightToConnection(IPAddress, getConfigurationManager().getLongValue(networkspamblockermanager_smallweight[0], networkspamblockermanager_smallweight[1]));
	} 
	
	public boolean isIPAccessAllowed(String IPAddress) throws NotStartedException {
		if (isStarted()) {
			for (IPAddressConnection connection : connectionList) {
				if (connection.getIPAddress().equals(IPAddress)) {
					return connection.isConnectionAllowed(
							getConfigurationManager().getLongValue(networkspamblockermanager_connectionweightcleanupms[0], networkspamblockermanager_connectionweightcleanupms[1]),
							getConfigurationManager().getLongValue(networkspamblockermanager_maxconnectionweight[0], networkspamblockermanager_maxconnectionweight[1]));
				}
			}
			
			return true;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void validateConnection(String IPAddress) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			if (!isIPAccessAllowed(IPAddress)) {
				throw new GenericErrorCode(GenericErrorCode.IPADDRESS_MAX_CONNECTIONS_EXCEEDED);
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}

	@Override
	public String getModuleName() {
		return "NetworkSpamBlocker";
	}

	@Override
	public synchronized void start() {
		synchronized (started) {
			if (!isStarted()) {
				connectionList = new ConcurrentLinkedQueue<>();
				updateScheduledGarbageCollector();
				getConfigurationManager().addListener(confListener);
				
				started = true;
			}
		}
	}

	@Override
	public synchronized void stop() {
		synchronized (started) {
			if (isStarted()) {
				started = false;
				garbageCollector.cancel(false);
				getConfigurationManager().removeListener(confListener);
				connectionList.clear();
				connectionList = null;
			}
		}
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import organize.server.ServerMain;
import organize.server.models.NotStartedException;
import organize.server.models.configuration.ConfigurationListener;
import organize.server.models.database.DatabaseConnector;
import organize.server.models.database.DatabaseException;
import organize.server.models.database.DatabaseMethods;
import organize.server.models.dbconnectors.MySQL;

public class DatabaseManager extends ServerModule {
	public static final String db_database_type[] = {"db_database_type", "MySQL"};
	public static final String db_database_error_retry[] = {"db_database_error_retry", "3"};
	public static final String db_database_restartondbchange[] = {"db_database_restartondbchange", "true"};
	public static String db_database_starttimeoutms[] = {"db_database_starttimeoutms", "5000"};
	
	private volatile DatabaseConnector database;
	
	ConfigurationListener confListener = new ConfigurationListener() {
		@Override
		public void onChange(String var, String oldValue, String newValue) {
			if (isStarted() && (getConfigurationManager().getBooleanValue(db_database_restartondbchange[0], db_database_restartondbchange[1]))) {
				stop();
				start();
			}
		}

		@Override
		public void onCreate(String var, String value) {}

		@Override
		public void onRemove(String var, String lastValue) {}
	};
	
	public DatabaseManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	@Override
	public synchronized void start() {
		synchronized (started) {
			if (!isStarted()) {
				logRuntimeInformation("Initializing database connectors...");
				
				String value = getConfigurationManager().getValue(db_database_type[0], db_database_type[1]);
				
				switch (value.toLowerCase()) {
				/*
					case "mysql": {
						database = new MySQL(this);
						logRuntimeInformation("Using: MySQL");
						
						break;
					}
					
					
					case "postgresql": {
						database = new PostgreSQL(this);
						logRuntimeInformation("Using: PostgreSQL");
						
						break;
					}
				
					case "db4o":*/
					default: {
						database = new MySQL(this);
						logRuntimeInformation("Using: MySQL");
						
						break;
					}
				}
				
				database.start();
				getConfigurationManager().addListener(confListener);
				
				started = true;
			}
		}
	}
	
	public DatabaseMethods getActiveDatabase() throws DatabaseException, NotStartedException {
		if (isStarted()) {
			waitForStart();
			return database;
		}

		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void waitForStart() throws DatabaseException {
		long timeRemaining = getConfigurationManager().getLongValue(db_database_starttimeoutms[0], db_database_starttimeoutms[1]);
		
		while (!isStarted()) {
			if (timeRemaining <= 0) {
				throw new DatabaseException("Timeout reached while waiting for database start.", DatabaseException.NO_CONNECTION);
			}
			
			else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException ex) {
					logRuntimeError("Caught InterruptedException from Thread.sleep().", false);
				} finally {
					timeRemaining--;
				}
			}
		}
	}

	@Override
	public String getModuleName() {
		return "Database";
	}

	@Override
	public synchronized void stop() {
		synchronized (started) {
			if (isStarted()) {
				logRuntimeWarning("Ending database connector...");
				database.stop();
				getConfigurationManager().removeListener(confListener);
				
				started = false;
				logRuntimeWarning("Database connector ended successfully.");
			}
		}
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;


import java.util.ArrayList;

import organize.server.ServerMain;
import organize.server.models.GenericErrorCode;
import organize.server.models.NotStartedException;
import organize.server.models.StaticData;
import organize.server.models.database.DatabaseFile;
import organize.server.models.database.DatabaseFilePart;
import organize.server.models.database.DatabaseFileRepositoryFile;
import organize.server.models.database.DatabaseGroupFileRepository;
import organize.server.models.database.DatabaseGroupFileRepositoryFile;
import organize.server.models.database.DatabaseGroupFileRepositoryPermissionSetting;
import organize.server.models.database.DatabaseGroupPermissionLevel;
import organize.server.models.database.DatabaseGroupUser;
import organize.server.models.database.DatabaseGroupUserPermission;
import organize.server.models.database.DatabasePrivateChat;
import organize.server.models.database.DatabasePrivateChatFileRepository;
import organize.server.models.database.DatabaseSession;
import organize.server.models.database.DatabaseUser;
import organize.server.models.database.DatabaseUserFileRepository;
import organize.server.models.database.DatabaseUserFileRepositoryFile;

public class FileManager extends ServerModule {
	public static final String fileservice_maxrepositoryfileretrieverange[] = {"fileservice_maxrepositoryfileretrieverange", "1000"};
	public static final String fileservice_maxgrouprepositories[] = {"communityservice_maxgrouprepositories", "30"};
	public static final String fileservice_maxgrouprepositorypermissionsettings[] = {"communityservice_maxgrouprepositorypermissionsettings", "50"};
	public static final String fileservice_maxfilepartretrieverange[] = {"fileservice_maxfilepartretrieverange", "200"};
	public static final String fileservice_maxfilepartsize[] = {"fileservice_maxfilepartsizeinbytes", "60000"};
	
	public FileManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	public boolean userHaveGroupFileRepositoryAccess(long userID, long groupFileRepositoryID) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseGroupFileRepository groupFileRepository = getDatabaseManager().getActiveDatabase().getGroupFileRepository(groupFileRepositoryID);
			DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByID(userID);
			DatabaseGroupUser groupUser = getDatabaseManager().getActiveDatabase().getGroupUser(user.getUserID(), groupFileRepository.getGroupID());
			ArrayList<DatabaseGroupUserPermission> groupUserPermissions = getDatabaseManager().getActiveDatabase().getGroupUserPermissions(groupUser.getUserID());
			ArrayList<DatabaseGroupFileRepositoryPermissionSetting> groupFileRepositoryPermissionSettings = getDatabaseManager().getActiveDatabase().getGroupFileRepositoryPermissionSettings(groupFileRepositoryID);
			
			for (DatabaseGroupPermissionLevel permissionLevel : getDatabaseManager().getActiveDatabase().getGroupPermissionLevels(groupFileRepository.getGroupID())) {
				for (DatabaseGroupUserPermission userPermission : groupUserPermissions) {
					if (permissionLevel.getGroupPermissionLevelID() == userPermission.getGroupPermissionLevelID()) {
						if (permissionLevel.isAllowAccessAllGroupFileRepositories() || permissionLevel.isGroupOwner() || permissionLevel.isAllowManagePermissionLevels()) {
							return true;
						}
						
						for (DatabaseGroupFileRepositoryPermissionSetting groupFileRepositoryPermission : groupFileRepositoryPermissionSettings) {
							if (groupFileRepositoryPermission.getGroupPermissionLevelID() == userPermission.getGroupPermissionLevelID()) {
								return true;
							}
						}
					}
				}
			}
			
			return false;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseGroupFileRepository> getGroupFileRepositories(String sessionToken, String deviceToken, long groupID) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			return getDatabaseManager().getActiveDatabase().getAllGroupFileRepositories(getDatabaseManager().getActiveDatabase().getGroupUser(getAccountManager().validateSession(sessionToken, deviceToken).getUserID(), groupID).getGroupID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseGroupFileRepositoryFile> getGroupFileRepositoryFiles(String sessionToken, String deviceToken, long groupFileRepositoryID, long startingFileNumber, long endingFileNumber) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			long lower = StaticData.filterLowerValueOfLong(startingFileNumber, endingFileNumber);
			long higher = StaticData.filterHigherValueOfLong(startingFileNumber, endingFileNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxrepositoryfileretrieverange[0], fileservice_maxrepositoryfileretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			
				if (userHaveGroupFileRepositoryAccess(session.getUserID(), groupFileRepositoryID)) {
					DatabaseGroupFileRepository dbGroupFileRepository = getDatabaseManager().getActiveDatabase().getGroupFileRepository(groupFileRepositoryID);
					return getDatabaseManager().getActiveDatabase().getGroupFileRepositoryFilesInRange(dbGroupFileRepository.getFileRepositoryID(), lower, higher);
				}
				
				throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_FILE_REPOSITORY_ACCESS);
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseFile getGroupFileRepositoryFileInformation(String sessionToken, String deviceToken, long groupFileRepositoryFileID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroupFileRepositoryFile groupFile = getDatabaseManager().getActiveDatabase().getGroupFileRepositoryFile(groupFileRepositoryFileID);
			
			if (userHaveGroupFileRepositoryAccess(session.getUserID(), groupFile.getGroupFileRepositoryID())) {
				return getDatabaseManager().getActiveDatabase().getFile(getDatabaseManager().getActiveDatabase().getFileRepositoryFile(groupFile.getFileRepositoryFileID()).getFileID());
			}
			
			throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_FILE_REPOSITORY_ACCESS);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseFilePart> getFilePartsFromGroupFileRepositoryFile(String sessionToken, String deviceToken, long groupFileRepositoryFileID, long startingPartNumber, long endingPartNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = Math.max(startingPartNumber, endingPartNumber);
			long higher = Math.min(startingPartNumber, endingPartNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxfilepartretrieverange[0], fileservice_maxfilepartretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseGroupFileRepositoryFile groupFileRepositoryFile = getDatabaseManager().getActiveDatabase().getGroupFileRepositoryFile(groupFileRepositoryFileID);
				
				if (userHaveGroupFileRepositoryAccess(session.getUserID(), groupFileRepositoryFile.getGroupFileRepositoryID())) {
					return getDatabaseManager().getActiveDatabase().getFilePartsInRange(
							getDatabaseManager().getActiveDatabase().getFileRepositoryFile(groupFileRepositoryFile.getFileRepositoryFileID()).getFileID(), 
							startingPartNumber, endingPartNumber);
				}
				
				throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_FILE_REPOSITORY_ACCESS);
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseUserFileRepository getUserFileRepository(String sessionToken, String deviceToken) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			return getDatabaseManager().getActiveDatabase().getUserFileRepositoryByUserID(session.getUserID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseUserFileRepositoryFile> getUserFileRepositoryFiles(String sessionToken, String deviceToken, long startingFileNumber, long endingFileNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = Math.max(startingFileNumber, endingFileNumber);
			long higher = Math.min(startingFileNumber, endingFileNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxfilepartretrieverange[0], fileservice_maxfilepartretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				getDatabaseManager().getActiveDatabase().getUserFileRepositoryFilesInRange(getDatabaseManager().getActiveDatabase().getUserFileRepositoryByUserID(session.getUserID()).getUserFileRepositoryID(), lower, higher);
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseFilePart> getFilePartsFromUserFileRepositoryFile(String sessionToken, String deviceToken, long userFileRepositoryFileID, long startingPartNumber, long endingPartNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = Math.max(startingPartNumber, endingPartNumber);
			long higher = Math.min(startingPartNumber, endingPartNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxfilepartretrieverange[0], fileservice_maxfilepartretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseUserFileRepository userFileRepository = getDatabaseManager().getActiveDatabase().getUserFileRepositoryByUserID(session.getUserID());
				DatabaseUserFileRepositoryFile userFileRepositoryFile = getDatabaseManager().getActiveDatabase().getUserFileRepositoryFile(userFileRepositoryFileID);
				
				if (userFileRepository.getUserFileRepositoryID() == userFileRepositoryFile.getUserFileRepositoryID()) {
					return getDatabaseManager().getActiveDatabase().getFilePartsInRange(
							getDatabaseManager().getActiveDatabase().getFileRepositoryFile(userFileRepositoryFile.getFileRepositoryFileID()).getFileID(),
							lower, higher);
				}
				
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER_FILE_REPOSITORY_FILE);
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseFile getUserFileRepositoryFileInformation(String sessionToken, String deviceToken, long userFileRepositoryFileID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseUserFileRepository userFileRepository = getDatabaseManager().getActiveDatabase().getUserFileRepositoryByUserID(session.getUserID());
			DatabaseUserFileRepositoryFile userFileRepositoryFile = getDatabaseManager().getActiveDatabase().getUserFileRepositoryFile(userFileRepositoryFileID);
			
			if (userFileRepository.getUserFileRepositoryID() == userFileRepositoryFile.getUserFileRepositoryID()) {
				return getDatabaseManager().getActiveDatabase().getFile(getDatabaseManager().getActiveDatabase().getFileRepositoryFile(userFileRepositoryFile.getFileRepositoryFileID()).getFileID());
			}
			
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER_FILE_REPOSITORY_FILE);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabasePrivateChatFileRepository> getPrivateChatFileRepositories(String sessionToken, String deviceToken, long otherUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseUser otherUser = getDatabaseManager().getActiveDatabase().getUserByID(otherUserID);
			
			try {
				DatabasePrivateChat privateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUser.getUserID(), true);
				return getDatabaseManager().getActiveDatabase().getPrivateChatFileRepositories(privateChat.getPrivateChatID());
			} catch (GenericErrorCode ex) {
				if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_PRIVATE_CHAT) {
					return new ArrayList<DatabasePrivateChatFileRepository>();
				}
				
				throw ex;
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseFileRepositoryFile> getPrivateChatFileRepositoryFiles(String sessionToken, String deviceToken, long otherUserID, long privateChatFileRepositoryID, long startingFileNumber, long endingFileNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = Math.max(startingFileNumber, endingFileNumber);
			long higher = Math.min(startingFileNumber, endingFileNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxfilepartretrieverange[0], fileservice_maxfilepartretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseUser otherUser = getDatabaseManager().getActiveDatabase().getUserByID(otherUserID);
				
				try {
					DatabasePrivateChat privateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUser.getUserID(), true);
					DatabasePrivateChatFileRepository privateChatFileRepository = getDatabaseManager().getActiveDatabase().getPrivateChatFileRepository(privateChatFileRepositoryID);
					
					if (privateChatFileRepository.getPrivateChatID() == privateChat.getPrivateChatID()) {
						return getDatabaseManager().getActiveDatabase().getFileRepositoryFilesInRange(privateChatFileRepository.getFileRepositoryID(), lower, higher);
					}
					
					throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY);
					
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_PRIVATE_CHAT) {
						return new ArrayList<DatabaseFileRepositoryFile>();
					}
					
					throw ex;
				}
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseFilePart> getPrivateChatFileRepositoryFileParts(String sessionToken, String deviceToken, long otherUserID, long privateChatFileRepositoryID, long fileRepositoryFileID, long startingPartNumber, long endingPartNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = Math.max(startingPartNumber, endingPartNumber);
			long higher = Math.min(startingPartNumber, endingPartNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(fileservice_maxfilepartretrieverange[0], fileservice_maxfilepartretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseUser otherUser = getDatabaseManager().getActiveDatabase().getUserByID(otherUserID);
				
				try {
					DatabasePrivateChat privateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUser.getUserID(), true);
					DatabasePrivateChatFileRepository privateChatFileRepository = getDatabaseManager().getActiveDatabase().getPrivateChatFileRepository(privateChatFileRepositoryID);
					
					if (privateChatFileRepository.getPrivateChatID() == privateChat.getPrivateChatID()) {
						DatabaseFileRepositoryFile privateChatFileRepositoryFile = getDatabaseManager().getActiveDatabase().getFileRepositoryFileFromPrivateChatFileRepository(privateChatFileRepositoryID, fileRepositoryFileID);
						return getDatabaseManager().getActiveDatabase().getFilePartsInRange(privateChatFileRepositoryFile.getFileID(), lower, higher);
					}
					
					throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY);
					
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_PRIVATE_CHAT) {
						throw new GenericErrorCode(GenericErrorCode.UNKNOWN_PRIVATE_CHAT_FILE_REPOSITORY_FILE);
					}
					
					throw ex;
				}
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}

	@Override
	public String getModuleName() {
		return "File";
	}

	@Override
	public synchronized void start() {
		setStarted(true);
	}

	@Override
	public synchronized void stop() {
		setStarted(false);
	}
}

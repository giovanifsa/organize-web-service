/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import organize.server.ServerMain;
import organize.server.models.GenericErrorCode;
import organize.server.models.NotStartedException;
import organize.server.models.StaticData;
import organize.server.models.configuration.ConfigurationListener;
import organize.server.models.database.DatabaseBannedGroupUser;
import organize.server.models.database.DatabaseChat;
import organize.server.models.database.DatabaseChatMessage;
import organize.server.models.database.DatabaseGroup;
import organize.server.models.database.DatabaseGroupChat;
import organize.server.models.database.DatabaseGroupChatPermissionSetting;
import organize.server.models.database.DatabaseGroupInvite;
import organize.server.models.database.DatabaseGroupPermissionLevel;
import organize.server.models.database.DatabaseGroupUser;
import organize.server.models.database.DatabaseGroupUserPermission;
import organize.server.models.database.DatabaseOnlineUser;
import organize.server.models.database.DatabasePrivateChat;
import organize.server.models.database.DatabaseSession;
import organize.server.models.database.DatabaseUser;

public class CommunityManager extends ServerModule {
	public static final String communityservice_characterlimit[] = {"communityservice_characterlimit", "512"};
	public static final String communityservice_maxonlinedevicesperuser[] ={"communityservice_maxdevicesperuser", "20"};
	public static final String communityservice_customstatusmaxsize[] = {"communityservice_customstatusmaxsize", "32"};
	public static final String communityservice_maxmessageretrieverange[] = {"communityservice_maxmessageretrieverange", "1000"};
	public static final String communityservice_maxgroupchats[] = {"communityservice_maxgroupchats", "150"};
	public static final String communityservice_maxgrouppermissionlevels[] = {"communityservice_maxgrouppermissionlevels", "50"};
	public static final String communityservice_maxgroupchatpermissionsettings[] = {"communityservice_maxgroupchatpermissionsettings", "50"};
	public static final String communityservice_maxgroupquantityperuser[] = {"communityservice_maxgroupquantityperuser", "15"};
	public static final String communityservice_minimumonlineuserupdatems[] = {"communityservice_minimumonlineuserupdatems", "300000"};
	public static final String communityservice_garbagecollectorupdatems[] = {"communityservice_garbagecollectorupdatems", "20000"};
	
	private ScheduledFuture<?> scheduledOfflineUsersCollector;
	private final ConfigurationListener configListener = new ConfigurationListener() {
		@Override
		public void onRemove(String var, String lastValue) {}
		
		@Override
		public void onCreate(String var, String value) {}
		
		@Override
		public void onChange(String var, String oldValue, String newValue) {
			if (isStarted()) {
				updateScheduledGarbageCollector();
			}
		}
	};
	
	public CommunityManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	private void updateScheduledGarbageCollector() {
		if (scheduledOfflineUsersCollector != null) {
			scheduledOfflineUsersCollector.cancel(false);
		}
		
		long collectorms = getConfigurationManager().getLongValue(communityservice_garbagecollectorupdatems[0], communityservice_garbagecollectorupdatems[1]);
		
		scheduledOfflineUsersCollector = getScheduledThreadPool().scheduleAtFixedRate(() -> {
			try {
				for (DatabaseOnlineUser onlineUser : getDatabaseManager().getActiveDatabase().getAllOnlineUsers()) {
					if ((onlineUser.getLastUpdatedMS() + getConfigurationManager().getLongValue(communityservice_minimumonlineuserupdatems[0], communityservice_minimumonlineuserupdatems[1])) <=
							getDate().getTime()) {
						getDatabaseManager().getActiveDatabase().deleteOnlineUser(onlineUser.getOnlineUserID());
					}
				}
			} catch (GenericErrorCode ex) {
				if (ex.getErrorCode() != GenericErrorCode.UNKNOWN_ONLINE_USER) {
					logRuntimeError("Caught error code " + ex.getErrorCode() + "and error " + ex.getMessage() + " while collecting offline users.", false);
				}
			}
			
		}, 0, collectorms, TimeUnit.MILLISECONDS);
	}
	
	public DatabaseChat getPrivateChatInformation(String sessionToken, String deviceToken, long otherUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseUser otherUser = getDatabaseManager().getActiveDatabase().getUserByID(otherUserID);
			DatabasePrivateChat dbPrivateChat;
			
			if (session.getUserID() != otherUserID) {
				try {
					dbPrivateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUser.getUserID(), true);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_CHAT) {
						dbPrivateChat = getDatabaseManager().getActiveDatabase().createPrivateChat(
								getDatabaseManager().getActiveDatabase().createChat().getChatID(), session.getUserID(), otherUser.getUserID());
					}
					
					else {
						throw ex;
					}
				}
				
				return getDatabaseManager().getActiveDatabase().getChat(dbPrivateChat.getChatID());
			}
			
			throw new GenericErrorCode(GenericErrorCode.SELF_MESSAGING);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseChatMessage sendMessageToUser(String sessionToken, String deviceToken, long otherUserID, String message, boolean isQuote, long quotedOwnerUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			
			if (message.length() <= getConfigurationManager().getIntValue(communityservice_characterlimit[0], communityservice_characterlimit[1])) {
				if (session.getUserID() != otherUserID) {
					DatabasePrivateChat privateChat;
				
					try {
						 privateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUserID, true);
					} catch (GenericErrorCode ex) {
						if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_CHAT) {
							privateChat = getDatabaseManager().getActiveDatabase().createPrivateChat(getDatabaseManager().getActiveDatabase().createChat().getChatID(), session.getUserID(), otherUserID);
						}
						
						else {
							throw ex;
						}
					}
					
					return getDatabaseManager().getActiveDatabase().createChatMessage(privateChat.getChatID(), session.getUserID(), message, isQuote, quotedOwnerUserID);
				}
				
				throw new GenericErrorCode(GenericErrorCode.SELF_MESSAGING);
			}
			
			throw new GenericErrorCode(GenericErrorCode.PARAMETER_LIMIT_EXCEEDED);
		}
			
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseChatMessage> getMessagesFromPrivateChat(String sessionToken, String deviceToken, long otherUserID, long startingMessageNumber, long endingMessageNumber) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			long lower = StaticData.filterLowerValueOfLong(startingMessageNumber, endingMessageNumber);
			long higher = StaticData.filterHigherValueOfLong(startingMessageNumber, endingMessageNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(communityservice_maxmessageretrieverange[0], communityservice_maxmessageretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseUser otherUser = getDatabaseManager().getActiveDatabase().getUserByID(otherUserID);
				ArrayList<DatabaseChatMessage> chatMessages = new ArrayList<>();
				DatabasePrivateChat dbPrivateChat;
				
				if (session.getUserID() != otherUserID) {
					try {
						dbPrivateChat = getDatabaseManager().getActiveDatabase().getPrivateChat(session.getUserID(), otherUser.getUserID(), true);
					} catch (GenericErrorCode ex) {
						if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_CHAT) {
							dbPrivateChat = getDatabaseManager().getActiveDatabase().createPrivateChat(
									getDatabaseManager().getActiveDatabase().createChat().getChatID(), session.getUserID(), otherUser.getUserID());
						}
						
						else {
							throw ex;
						}
					}
	
					chatMessages = getDatabaseManager().getActiveDatabase().getMessagesFromChatInRange(dbPrivateChat.getChatID(), lower, higher);
				}
				
				return chatMessages;
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	private boolean userHaveGroupChatAccessPermission(long userID, long groupChatID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByID(userID);
			DatabaseGroupChat groupChat = getDatabaseManager().getActiveDatabase().getGroupChat(groupChatID);
			DatabaseGroupUser groupUser = getDatabaseManager().getActiveDatabase().getGroupUser(user.getUserID(), groupChat.getGroupID());
			ArrayList<DatabaseGroupChatPermissionSetting> groupChatPermissionSettings = getDatabaseManager().getActiveDatabase().getGroupChatPermissionSettings(groupChat.getGroupChatID());
			ArrayList<DatabaseGroupPermissionLevel> groupPermissionLevels = getDatabaseManager().getActiveDatabase().getGroupPermissionLevels(groupChat.getGroupID());
			ArrayList<DatabaseGroupUserPermission> groupUserPermissions = getDatabaseManager().getActiveDatabase().getGroupUserPermissions(groupUser.getGroupUserID());
			
			for (DatabaseGroupPermissionLevel groupPermissionLevel : groupPermissionLevels) {
				for (DatabaseGroupUserPermission userPermission : groupUserPermissions) {
					if (userPermission.getGroupPermissionLevelID() == groupPermissionLevel.getGroupPermissionLevelID()) {
						if (groupPermissionLevel.isAllowAccessAllGroupChats() || groupPermissionLevel.isAllowManagePermissionLevels() || groupPermissionLevel.isGroupOwner()) {
							return true;
						}
						
						else {
							for (DatabaseGroupChatPermissionSetting groupChatPermissionSetting : groupChatPermissionSettings) {
								if (groupChatPermissionSetting.getGroupPermissionLevelID() == userPermission.getGroupPermissionLevelID()) {
									return true;
								}
							}
						}
					}
				}
			}
			
			return false;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public boolean userHaveGroupChatAccessPermission(String sessionToken, String deviceToken, long groupChatID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
		
			return userHaveGroupChatAccessPermission(session.getUserID(), groupChatID);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseChatMessage sendMessageToGroupChat(String sessionToken, String deviceToken, long groupChatID, String message, boolean isQuote, long quotedOwnerUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroupChat groupChat = getDatabaseManager().getActiveDatabase().getGroupChat(groupChatID);
			
			if (userHaveGroupChatAccessPermission(session.getUserID(), groupChat.getGroupChatID())) {
				return getDatabaseManager().getActiveDatabase().createChatMessage(groupChat.getChatID(), session.getUserID(), message, isQuote, quotedOwnerUserID);
			}
			
			throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_CHAT_ACCESS);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseGroupChat> getGroupChats(String sessionToken, String deviceToken, long groupID) throws GenericErrorCode {
		if (isStarted()) {
			return getDatabaseManager().getActiveDatabase().getAllGroupChats(getDatabaseManager().getActiveDatabase().getGroupUser(getAccountManager().validateSession(sessionToken, deviceToken).getUserID(), groupID).getGroupID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseChatMessage> getMessagesFromGroupChat(String sessionToken, String deviceToken, long groupChatID, long startingMessageNumber, long endingMessageNumber) throws GenericErrorCode {
		if (isStarted()) {
			long lower = StaticData.filterLowerValueOfLong(startingMessageNumber, endingMessageNumber);
			long higher = StaticData.filterHigherValueOfLong(startingMessageNumber, endingMessageNumber);
			
			if ((higher - lower) <= getConfigurationManager().getLongValue(communityservice_maxmessageretrieverange[0], communityservice_maxmessageretrieverange[1])) {
				DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
				DatabaseGroupChat groupChat = getDatabaseManager().getActiveDatabase().getGroupChat(groupChatID);
				
				if (userHaveGroupChatAccessPermission(session.getUserID(), groupChatID)) {
					return getDatabaseManager().getActiveDatabase().getMessagesFromChatInRange(groupChat.getChatID(), lower, higher);
				}
				
				throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_CHAT_ACCESS);
			}
			
			throw new GenericErrorCode(GenericErrorCode.RANGE_LIMIT_EXCEEDED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseGroup createGroup(String sessionToken, String deviceToken, String groupName, int groupAccess) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseUser sessionUser = getDatabaseManager().getActiveDatabase().getUserByID(session.getUserID());
			
			if (sessionUser.getGroupOwnershipCount() < getConfigurationManager().getLongValue(communityservice_maxgroupquantityperuser[0], communityservice_maxgroupquantityperuser[1])) {
				DatabaseGroup newGroup = getDatabaseManager().getActiveDatabase().createGroup(groupName, sessionUser.getUserID(), ((groupAccess >= 0 && groupAccess <= 3) ? groupAccess : 0));
				DatabaseGroupUser groupUser = getDatabaseManager().getActiveDatabase().createGroupUser(sessionUser.getUserID(), newGroup.getGroupID());
				getDatabaseManager().getActiveDatabase().createGroupUserPermission(groupUser.getGroupUserID(), 
						getDatabaseManager().getActiveDatabase().createGroupPermissionLevel(newGroup.getGroupID(), "#OWNER", false, true, true, true, true, true, true).getGroupPermissionLevelID(), false);
				
				return newGroup;
			}
			
			throw new GenericErrorCode(GenericErrorCode.USER_GROUP_CREATION_LIMIT_REACHED);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void deleteGroup(String sessionToken, String deviceToken, long groupID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroup group = getDatabaseManager().getActiveDatabase().getGroup(groupID);
			
			if (session.getUserID() == group.getOwnerUserID()) {
				getDatabaseManager().getActiveDatabase().deleteGroup(group.getGroupID());
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	 
	public DatabaseOnlineUser setOnlineStatus(String sessionToken, String deviceToken, int userStatus, String customStatus) throws GenericErrorCode {
		if (isStarted()) {
			if (customStatus != null && customStatus.length() > getConfigurationManager().getIntValue(communityservice_customstatusmaxsize[0], communityservice_customstatusmaxsize[1])) {
				throw new GenericErrorCode(GenericErrorCode.PARAMETER_LIMIT_EXCEEDED);
			}
			
			int finalStatus = DatabaseOnlineUser.STATUS_OFFLINE;
			String finalCustom = null;
			
			switch (userStatus) {
				case DatabaseOnlineUser.STATUS_ONLINE:
				case DatabaseOnlineUser.STATUS_AWAY:
				case DatabaseOnlineUser.SLEEPING:
				case DatabaseOnlineUser.OCCUPIED: {
					finalStatus = userStatus;
					finalCustom = null;
				}
				
				case DatabaseOnlineUser.CUSTOM: {
					finalStatus = userStatus;
					finalCustom = customStatus;
				}
				
				default: {
					finalStatus = DatabaseOnlineUser.STATUS_OFFLINE;
					finalCustom = null;
				}
			}
			
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			int deviceQuantity = 0;
			DatabaseOnlineUser foundUser = null;
			
			for (DatabaseOnlineUser onlineUser : getDatabaseManager().getActiveDatabase().getOnlineUserByUserID(session.getUserID())) {
				deviceQuantity++;
				
				if (onlineUser.getDeviceToken().equals(deviceToken)) {
					foundUser = getDatabaseManager().getActiveDatabase().createOrUpdateOnlineUser(session.getUserID(), deviceToken, finalStatus, finalCustom, getDate().getTime());
				}
				
				else {
					try {
						if ((onlineUser.getLastUpdatedMS() + getConfigurationManager().getLongValue(
								communityservice_minimumonlineuserupdatems[0], communityservice_minimumonlineuserupdatems[1])) <= getDate().getTime()) {
							getDatabaseManager().getActiveDatabase().deleteOnlineUser(onlineUser.getOnlineUserID());
						}
					} catch (GenericErrorCode ex) {}
				}
			}
			
			if (foundUser == null) {
				if (deviceQuantity < getConfigurationManager().getIntValue(communityservice_maxonlinedevicesperuser[0], communityservice_maxonlinedevicesperuser[1])) {
					return getDatabaseManager().getActiveDatabase().createOrUpdateOnlineUser(session.getUserID(), deviceToken, finalStatus, finalCustom, getDate().getTime());
				}
				
				throw new GenericErrorCode(GenericErrorCode.DEVICE_LIMIT_EXCEEDED);
			}
			
			return foundUser;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public ArrayList<DatabaseGroupInvite> getGroupInvites(String sessionToken, String deviceToken) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			return getDatabaseManager().getActiveDatabase().getGroupInvitesForUser(session.getUserID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseGroupInvite inviteUserToGroup(String sessionToken, String deviceToken, long userToInviteID, long groupID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroup group = getDatabaseManager().getActiveDatabase().getGroup(groupID);
			getDatabaseManager().getActiveDatabase().getGroupUser(session.getUserID(), group.getGroupID());
			DatabaseUser userToInvite = getDatabaseManager().getActiveDatabase().getUserByID(userToInviteID);
			
			try {
				getDatabaseManager().getActiveDatabase().getBannedGroupUser(userToInvite.getUserID(), group.getGroupID());
				throw new GenericErrorCode(GenericErrorCode.CANNOT_INVITE_USER_BANNED_FROM_GROUP);
			} catch (GenericErrorCode ex) {
				if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_BANNED_GROUP_USER) {
					for (DatabaseGroupInvite groupInvite : getDatabaseManager().getActiveDatabase().getGroupInvitesForUser(userToInvite.getUserID())) {
						if (groupInvite.getInvitedUserID() == userToInvite.getUserID() && groupInvite.getGroupID() == group.getGroupID()) {
							throw new GenericErrorCode(GenericErrorCode.USER_ALREADY_INVITED_TO_GROUP);
						}
					}
					
					return getDatabaseManager().getActiveDatabase().createGroupInvite(group.getGroupID(), userToInvite.getUserID(), session.getUserID());
				}
				
				throw ex;
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseGroupUser acceptGroupInvite(String sessionToken, String deviceToken, long groupInviteID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroupInvite invite = getDatabaseManager().getActiveDatabase().getGroupInvite(groupInviteID);
			
			if (invite.getInvitedUserID() == session.getUserID()) {
				getDatabaseManager().getActiveDatabase().deleteGroupInvite(invite.getGroupInviteID());
				
				try {
					getDatabaseManager().getActiveDatabase().getBannedGroupUser(session.getUserID(), invite.getGroupID());
					throw new GenericErrorCode(GenericErrorCode.CANNOT_JOIN_BANNED_GROUP);
				} catch (GenericErrorCode ex) {
					if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_BANNED_GROUP_USER) {
						return getDatabaseManager().getActiveDatabase().createGroupUser(session.getUserID(), invite.getGroupID());
					}
					
					throw ex;
				}
				
			}
			
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_GROUP_INVITE);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseOnlineUser getUserStatus(long userID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByID(userID);
			
			for (DatabaseOnlineUser onlineUser : getDatabaseManager().getActiveDatabase().getOnlineUserByUserID(userID)) {
				if (onlineUser.getUserID() == userID) {
					return onlineUser.removeSensitiveData();
				}
			}
			
			return new DatabaseOnlineUser(user.getUserID(), DatabaseOnlineUser.STATUS_OFFLINE, null);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void removeUserFromGroup(String sessionToken, String deviceToken, long groupUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroupUser groupUser = getDatabaseManager().getActiveDatabase().getGroupUser(groupUserID);
			
			try {
				DatabaseGroupUser sessionGroupUser = getDatabaseManager().getActiveDatabase().getGroupUser(session.getUserID(), groupUser.getGroupID());
				ArrayList<DatabaseGroupUserPermission> sessionGroupUserPermissions = getDatabaseManager().getActiveDatabase().getGroupUserPermissions(sessionGroupUser.getGroupUserID());
				
				for (DatabaseGroupPermissionLevel permissionLevel : getDatabaseManager().getActiveDatabase().getGroupPermissionLevels(sessionGroupUser.getGroupUserID())) {
					for (DatabaseGroupUserPermission groupUserPermission : sessionGroupUserPermissions) {
						if (groupUserPermission.getGroupPermissionLevelID() == permissionLevel.getGroupPermissionLevelID() && (permissionLevel.isAllowKickGroupUsers() || permissionLevel.isGroupOwner())) {
							getDatabaseManager().getActiveDatabase().deleteGroupUser(groupUserID);
							return;
						}
					}
				}
				
				throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_GROUP_PERMISSION);
			} catch (GenericErrorCode ex) {
				if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_GROUP_USER) {
					throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_GROUP_PERMISSION);
				}
				
				throw ex;
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseBannedGroupUser banUserFromGroup(String sessionToken, String deviceToken, long groupUserID) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = getAccountManager().validateSession(sessionToken, deviceToken);
			DatabaseGroupUser groupUser = getDatabaseManager().getActiveDatabase().getGroupUser(groupUserID);
			
			try {
				DatabaseGroupUser sessionGroupUser = getDatabaseManager().getActiveDatabase().getGroupUser(session.getUserID(), groupUser.getGroupID());
				ArrayList<DatabaseGroupUserPermission> sessionGroupUserPermissions = getDatabaseManager().getActiveDatabase().getGroupUserPermissions(sessionGroupUser.getGroupUserID());
				
				for (DatabaseGroupPermissionLevel permissionLevel : getDatabaseManager().getActiveDatabase().getGroupPermissionLevels(sessionGroupUser.getGroupUserID())) {
					for (DatabaseGroupUserPermission groupUserPermission : sessionGroupUserPermissions) {
						if (groupUserPermission.getGroupPermissionLevelID() == permissionLevel.getGroupPermissionLevelID() && (permissionLevel.isAllowBanGroupUsers() || permissionLevel.isGroupOwner())) {
							getDatabaseManager().getActiveDatabase().deleteGroupUser(groupUserID);
							return getDatabaseManager().getActiveDatabase().createBannedGroupUser(groupUser.getUserID(), groupUser.getGroupID());
						}
					}
				}
				
				throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_GROUP_PERMISSION);
			} catch (GenericErrorCode ex) {
				if (ex.getErrorCode() == GenericErrorCode.UNKNOWN_GROUP_USER) {
					throw new GenericErrorCode(GenericErrorCode.USER_HAS_NO_GROUP_PERMISSION);
				}
				
				throw ex;
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}

	@Override
	public String getModuleName() {
		return "Community";
	}

	@Override
	public void start() {
		synchronized (started) {
			if (!isStarted()) {
				logRuntimeInformation("Initializing...");
				updateScheduledGarbageCollector();
				getConfigurationManager().addListener(configListener);
				
				started = true;
				logRuntimeInformation("Initialization successful.");
			}
		}
	}

	@Override
	public void stop() {
		synchronized (started) {
			if (isStarted()) {
				started = false;
				
				logRuntimeInformation("Stopping...");
				getConfigurationManager().removeListener(configListener);
				
				if (scheduledOfflineUsersCollector != null) {
					scheduledOfflineUsersCollector.cancel(false);
				}
				
				logRuntimeInformation("Stopped successfully.");
			}
		}
	}
}







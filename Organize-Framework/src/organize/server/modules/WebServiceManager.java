/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import organize.server.ServerMain;
import organize.server.models.GenericErrorCode;
import organize.server.webservices.AccountAPIv1;
import organize.server.webservices.CommunityAPIv1;
import organize.server.webservices.FileAPIv1;

public class WebServiceManager extends ServerModule {
	public static final String webservices_enabledapis[] = {"webservices_enabledapis", "v1"};
	public static final String webservices_maxreceivebytes[] = {"webservices_maxreceivebytes", "10000000"};
	public static final String webservices_receivebuffersize[] = {"webservices_receivebuffersize", "2048"};
	
	private AccountAPIv1 accountAPIv1;
	private CommunityAPIv1 communityAPIv1;
	private FileAPIv1 fileAPIv1;
	private volatile boolean started = false;
	
	public WebServiceManager(ServerMain serverMain) {
		super(serverMain);
		accountAPIv1 = new AccountAPIv1(this);
		accountAPIv1.start();
		
		communityAPIv1 = new CommunityAPIv1(this);
		communityAPIv1.start();
		
		fileAPIv1 = new FileAPIv1(this);
		fileAPIv1.start();
		
		logRuntimeInformation("Registering V1 APIs");
		serverMain.register(communityAPIv1);
		serverMain.register(accountAPIv1);
		serverMain.register(fileAPIv1);
		logRuntimeInformation("Registration finished without errors.");
	}

	public AccountAPIv1 getAccountAPIv1() {
		return accountAPIv1;
	}

	public CommunityAPIv1 getCommunityAPIv1() {
		return communityAPIv1;
	}
	
	/**
	 * Reads an String object from a InputStream, and stores it in StaticData.STANDARD_CHARSET form.
	 * @param inputStream InputStream to receive bytes from.
	 * @return String object encoded in StaticData.STANDARD_CHARSET.
	 * @throws GenericErrorCode GenericErrorCode containing GenericErrorCode.API_TRANSFER_LIMIT_EXCEEDED or GenericErrorCode.API_TRANSFER_FAILED.
	 */
	public String receiveStringFromInputStream(InputStream inputStream) throws GenericErrorCode {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[getConfigurationManager().getIntValue(webservices_receivebuffersize[0], webservices_receivebuffersize[1])];
		
		try {
			while (inputStream.read(buffer) != -1) {
				outputStream.write(buffer);
				
				if (outputStream.size() > getConfigurationManager().getIntValue(webservices_receivebuffersize[0], webservices_receivebuffersize[1])) {
					throw new GenericErrorCode(GenericErrorCode.API_TRANSFER_LIMIT_EXCEEDED);
				}
			}
		} catch (IOException e) {
			throw new GenericErrorCode(GenericErrorCode.API_TRANSFER_FAILED);
		}
		
		return outputStream.toString();
	}

	@Override
	public String getModuleName() {
		return "WebService";
	}

	@Override
	public synchronized void start() {
		started = true;
	}

	@Override
	public void stop() {
		started = false;
	}

	@Override
	public boolean isStarted() {
		return started;
	}
}

/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import organize.server.ServerMain;
import organize.server.models.configuration.ConfigurationListener;

class SelectedVariableListener {
	public ConfigurationListener listener;
	public String variable;
}

public class ConfigurationManager extends ServerModule {
	public static final String CONFIGURATION_SEARCH_REGEX = "^\\s*(.+)\\s*=\\s*\"(.+)\"\\s*$";
	
	private final HashMap<String, String> variables = new HashMap<>();
	private final ArrayList<ConfigurationListener> listeners = new ArrayList<>();
	private final ArrayList<SelectedVariableListener> selectedListeners = new ArrayList<>();
	private volatile File configFile;
	
	public ConfigurationManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	public synchronized void setConfigFile(File f) {
		configFile = f;
		logRuntimeInformation("Selected file: " + f.getAbsolutePath());
	}
	
	public synchronized void readFromConfigFile() {
		if (configFile != null) {
			try (BufferedReader bfReader = new BufferedReader(new FileReader(configFile))) {
				String line;
				
				while ((line = bfReader.readLine()) != null) {
					Pattern pattern = Pattern.compile(CONFIGURATION_SEARCH_REGEX);
					Matcher match = pattern.matcher(line);
					
					if (match.matches()) {
						setValue(match.group(1), match.group(2));
						logRuntimeInformation("Loaded variable from file: " + match.group(0));
					}
				}
			} catch (IOException ex) {
				logRuntimeError("Configuration file loading error: " + ex.getMessage(), false);
			}
		}
	}
	
	public synchronized void writeToConfigFile() {
		if (configFile != null) {
			try (PrintWriter pWriter = new PrintWriter(configFile)) {
				for (Entry<String, String> s :variables.entrySet()) {
					pWriter.println(s.getKey() + " = " + s.getValue());
				}
			} catch (IOException ex) {
				logRuntimeError("Configuration file saving error: " + ex.getMessage(), false);
			}
		}
	}
	
	public synchronized void addListener(ConfigurationListener cl) {
		if (cl != null) {
			listeners.add(cl);
		}
	}
	
	public synchronized void addListener(String var, ConfigurationListener cl) {
		if (var != null && !var.isEmpty() && cl != null) {
			SelectedVariableListener l = new SelectedVariableListener();
			l.listener = cl;
			l.variable = var;
			selectedListeners.add(l);
		}
	}
	
	public synchronized void removeListener(ConfigurationListener cl) {
		for (ConfigurationListener cLi : listeners) {
			if (cLi == cl) {
				listeners.remove(cLi);
			}
		}
		
		for (SelectedVariableListener cLi : selectedListeners) {
			if (cLi.listener == cl) {
				selectedListeners.remove(cLi);
			}
		}
	}
	
	public synchronized boolean containsVar(String var) {
		return variables.containsKey(var);
	}
	
	public synchronized String getValue(String var, String defaultValue) {
		if (!variables.containsKey(var)) {
			variables.put(var, defaultValue);
		}
		
		return variables.getOrDefault(var, defaultValue);
	}
	
	/**
	 * Gets a integer value from a variable.
	 * @param var Variable to get the integer value from.
	 * @param defaultValue Default value of the variable.
	 * @return The integer value of the 
	 * @
	 */
	public synchronized int getIntValue(String var, String defaultValue) {
		int value = 0;
		
		try {
			value = Integer.valueOf(getValue(var, defaultValue));
		} catch (NumberFormatException ex) {
			try {
				value = Integer.valueOf(defaultValue);
			} catch (NumberFormatException ex1) {}
		}
		
		return value;
	}
	
	public synchronized long getLongValue(String var, String defaultValue) {
		long value = 0;
		
		try {
			value = Long.valueOf(getValue(var, defaultValue));
		} catch (NumberFormatException ex) {
			try {
				value = Long.valueOf(defaultValue);
			} catch (NumberFormatException ex1) {}
		}
		
		return value;
	}
	
	public synchronized boolean getBooleanValue(String var, String defaultValue) {
		return Boolean.valueOf(getValue(var, defaultValue));
	}
	
	public synchronized byte[] getBytesValue(String var, String defaultValue) {
		return getValue(var, defaultValue).getBytes();
	}
	
	public synchronized long setLongValue(String var, long value) {
		long lastValue = getLongValue(var, "0");
		variables.put(var, String.valueOf(value));
	
		return lastValue;
	}
	
	public synchronized byte[] setBytesValue(String var, byte[] value) {
		byte[] lastBytes = getBytesValue(var, "");
		variables.put(var, String.valueOf(value));
	
		return lastBytes;
	}
	
	public synchronized String setValue(String var, String value) {
		return variables.put(var, value);
	}
	
	public synchronized ArrayList<String> getSeparatedValues(String var, String separator, String defaultValue) {
		ArrayList<String> foundValues = new ArrayList<>();
		
		for (String str : getValue(var, defaultValue).split(separator)) {
			foundValues.add(str);
		}
		
		return foundValues;
	}

	@Override
	public String getModuleName() {
		return "Configuration";
	}

	@Override
	public void start() {/*This module can't be started or stopped*/}

	@Override
	public void stop() {/*This module can't be started or stopped*/}
	
	@Override
	public boolean isStarted() {
		return true;
	}
}

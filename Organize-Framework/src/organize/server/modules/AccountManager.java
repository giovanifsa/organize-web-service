/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.util.Arrays;

import organize.server.ServerMain;
import organize.server.models.GenericErrorCode;
import organize.server.models.HashedData;
import organize.server.models.NotStartedException;
import organize.server.models.StaticData;
import organize.server.models.database.DatabaseCryptographySetting;
import organize.server.models.database.DatabaseSession;
import organize.server.models.database.DatabaseUser;

public class AccountManager extends ServerModule {
	public static final String accountservice_sessionexpirationms[] = {"accountservice_sessionexpirationms", "864000000"};
	public static final String accountservice_usernamecharlimit[] = {"accountservice_usernamecharlimit", "24"};
	public static final String accountservice_passwordcharlimit[] = {"accountservice_passwordcharlimit", "64"};
	public static final String accountservice_emailcharlimit[] = {"accountservice_emailcharlimit", "320"};
	public static final String accountservice_recoverquestioncharlimit[] = {"accountservice_recoverquestioncharlimit", "255"};
	public static final String accountservice_displaynamecharlimit[] = {"accountservice_displaynamecharlimit", "24"};
	public static final String accountservice_recoverquestionanswercharlimit[] = {"accountservice_recoverquestionanswercharlimit", "255"};
	public static final String accountservice_passwordminimumcharacterquantity[] = {"accountservice_passwordminimumcharacterquantity", "8"};
	public static final String accountservice_sessiontokenlength[] = {"accountservice_passwordminimumcharacterquantity", "256"};
	public static final String accountservice_devicetokenlength[] = {"accountservice_passwordminimumcharacterquantity", "256"};
	public static final String accountservice_maxtokengenerationretries[] = {"accountservice_maxtokengenerationretries", "1000"};
	
	public AccountManager(ServerMain serverMain) {
		super(serverMain);
	}
	
	public DatabaseUser createAccount(String username, String password, String email, String displayName, String recoverQuestion, String recoverQuestionAnswer) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			if (username == null || password == null || email == null || recoverQuestion == null || recoverQuestionAnswer == null) {
				throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
			}
			
			if (password.length() < getConfigurationManager().getIntValue(accountservice_passwordminimumcharacterquantity[0], accountservice_passwordminimumcharacterquantity[1])) {
				throw new GenericErrorCode(GenericErrorCode.PARAMETER_MINIMUM_NOT_REACHED);
			}
			
			if (username.length() > getConfigurationManager().getIntValue(accountservice_usernamecharlimit[0], accountservice_usernamecharlimit[1]) ||
					password.length() > getConfigurationManager().getIntValue(accountservice_passwordcharlimit[0], accountservice_passwordcharlimit[1]) ||
					email.length() > getConfigurationManager().getIntValue(accountservice_emailcharlimit[0], accountservice_emailcharlimit[1]) ||
					recoverQuestion.length() > getConfigurationManager().getIntValue(accountservice_recoverquestioncharlimit[0], accountservice_recoverquestioncharlimit[1]) ||
					displayName.length() > getConfigurationManager().getIntValue(accountservice_displaynamecharlimit[0], accountservice_displaynamecharlimit[1]) ||
					recoverQuestionAnswer.length() > getConfigurationManager().getIntValue(accountservice_recoverquestionanswercharlimit[0], accountservice_recoverquestioncharlimit[1])) {
				throw new GenericErrorCode(GenericErrorCode.PARAMETER_LIMIT_EXCEEDED);
			}
	
			HashedData hashedPassword = getCryptographyManager().doCPUIntensiveHash(password.getBytes());
			DatabaseUser newUser = getDatabaseManager().getActiveDatabase().createUser(username, hashedPassword.getHashedData(), email, displayName, recoverQuestion, recoverQuestionAnswer,
					getDatabaseManager().getActiveDatabase().createCryptographySetting(hashedPassword.getUsedHashName(), hashedPassword.getUsedSalt(), hashedPassword.getIterations(), hashedPassword.getLength()).getCryptographySettingID()).removeSensitiveData();
			getDatabaseManager().getActiveDatabase().createUserFileRepository(newUser.getUserID(), getDatabaseManager().getActiveDatabase().createFileRepository().getFileRepositoryID());
			return newUser;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public String getAccountRecoverQuestion(String username, String email) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByUsername(username);
			
			if (user.getUsername().equals(username) && user.getEmail().equals(email)) {
				return user.getRecoverQuestion();
			}
			
			throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseUser recoverAccount(String username, String email, String newPassword, String recoverQuestionAnswer) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			if (newPassword != null) {
				DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByUsername(username);
			
				if (user.getUsername().equals(username) && user.getEmail().equals(email) && user.getRecoverQuestionAnswer().equals(recoverQuestionAnswer)) {
					HashedData hashedPassword = getCryptographyManager().doCPUIntensiveHash(newPassword.getBytes(StaticData.STANDARD_CHARSET));
					DatabaseUser updatedUser = getDatabaseManager().getActiveDatabase().changeUserPassword(user.getUserID(), hashedPassword.getHashedData(), getDatabaseManager().getActiveDatabase().createCryptographySetting(hashedPassword.getUsedHashName(), hashedPassword.getUsedSalt(), hashedPassword.getIterations(), hashedPassword.getLength()).getCryptographySettingID());
					getDatabaseManager().getActiveDatabase().deleteCryptographySetting(user.getCryptographySettingID());
					return updatedUser;
				}
				
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseSession logIntoAccount(String username, String password, String deviceToken) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			if (deviceToken != null && deviceToken.length() <= getConfigurationManager().getIntValue(accountservice_devicetokenlength[0], accountservice_devicetokenlength[1])) {
				DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByUsername(username);
				DatabaseCryptographySetting cryptographySetting = getDatabaseManager().getActiveDatabase().getCryptographySetting(user.getCryptographySettingID());
				HashedData hashedPassword = getCryptographyManager().hashData(password.getBytes(StaticData.STANDARD_CHARSET), cryptographySetting.getUsedHashName(), cryptographySetting.getUsedSalt(), cryptographySetting.getIterations(), cryptographySetting.getLength());
				
				if (Arrays.compare(user.getHashedPassword(), hashedPassword.getHashedData()) == 0) {
					for (int x = 0; x < getConfigurationManager().getIntValue(accountservice_maxtokengenerationretries[0], accountservice_maxtokengenerationretries[1]); x++) {
						try {
							return getDatabaseManager().getActiveDatabase().createSession(
							StaticData.generateRandomString(getConfigurationManager().getIntValue(accountservice_sessiontokenlength[0], accountservice_sessiontokenlength[1]), 
									getSecureRandom()),
							deviceToken,
							user.getUserID(),
							getDate().getTime() + getConfigurationManager().getLongValue(accountservice_sessionexpirationms[0], accountservice_sessionexpirationms[1]),
							getDate().getTime());
						} catch (GenericErrorCode ex) {
							if (ex.getErrorCode() != GenericErrorCode.SESSION_ALREADY_EXISTS) {
								throw ex;
							}
						}
					}
				}
				
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_USER);
			}
			
			throw new GenericErrorCode(GenericErrorCode.INVALID_PARAMETERS);
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void endSession(String sessionToken, String deviceToken) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseSession session = validateSession(sessionToken, deviceToken);
			getDatabaseManager().getActiveDatabase().deleteSession(session.getSessionID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public void endAllAccountSessions(String sessionToken, String deviceToken) throws GenericErrorCode {
		if (isStarted()) {
			DatabaseSession session = validateSession(sessionToken, deviceToken);
			getDatabaseManager().getActiveDatabase().deleteSessionsByUserID(session.getUserID());
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseSession validateSession(String sessionToken, String deviceToken) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseSession session = validateSession(getDatabaseManager().getActiveDatabase().getSessionBySessionToken(sessionToken));
			
			if (!session.getDeviceToken().equals(deviceToken)) {
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
			}
			
			return session;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseUser getUserInformation(String sessionToken, String deviceToken, long userID) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseSession session = validateSession(sessionToken, deviceToken);
			DatabaseUser user = getDatabaseManager().getActiveDatabase().getUserByID(userID);
			
			if (session.getUserID() == user.getUserID()) {
				return user;
			}
			
			else {
				return user.removeSensitiveData();
			}
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseSession validateSession(DatabaseSession databaseSession) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			if (databaseSession.getExpiresAt() < getDate().getTime()) {
				getDatabaseManager().getActiveDatabase().deleteSession(databaseSession.getSessionID());
				throw new GenericErrorCode(GenericErrorCode.UNKNOWN_SESSION);
			}
			
			return databaseSession;
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}
	
	public DatabaseSession renewSession(String sessionToken, String deviceToken) throws GenericErrorCode, NotStartedException {
		if (isStarted()) {
			DatabaseSession session = validateSession(sessionToken, deviceToken);
			return getDatabaseManager().getActiveDatabase().renewSession(session.getUserID(), getDate().getTime() + getConfigurationManager().getLongValue(accountservice_sessionexpirationms[0], accountservice_sessionexpirationms[1]));
		}
		
		else {
			throw new NotStartedException("Not started yet.");
		}
	}

	@Override
	public String getModuleName() {
		return "AccountManager";
	}

	@Override
	public void start() {
		logRuntimeInformation("Starting up...");
		setStarted(true);
		logRuntimeInformation("Started successfully.");
	}

	@Override
	public void stop() {
		logRuntimeInformation("Stopping...");
		setStarted(false);
		logRuntimeInformation("Stopped successfully.");
	}
}

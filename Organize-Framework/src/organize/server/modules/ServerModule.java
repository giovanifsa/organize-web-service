/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package organize.server.modules;

import java.security.SecureRandom;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import organize.server.ServerMain;

public abstract class ServerModule {
	protected final ServerMain serverMain;
	protected volatile Boolean started = false;
	
	public ServerModule(ServerMain serverMain) {
		this.serverMain = serverMain;
	}
	
	public abstract String getModuleName();
	public abstract void start();
	public abstract void stop();
	
	public void setStarted(boolean started) {
		synchronized (this.started) {
			this.started = started;
		}
	}
	
	public boolean isStarted() {
		synchronized (started) {
			return started;
		}
	}
	
	public ServerMain getServerMain() {
		return serverMain;
	}
	
	public void logRuntimeInformation(String information) {
		getServerMain().serverRuntimeInformation(getModuleName(), information);
	}
	
	public void logRuntimeWarning(String warning) {
		getServerMain().serverRuntimeWarning(getModuleName(), warning);
	}
	
	public void logRuntimeError(String error, boolean fatal) {
		getServerMain().serverRuntimeError(getModuleName(), error, fatal);
	}
	
	public AccountManager getAccountManager() {
		return getServerMain().getAccountManager();
	}
	
	public CommunityManager getCommunityManager() {
		return getServerMain().getCommunityManager();
	}
	
	public ConfigurationManager getConfigurationManager() {
		return getServerMain().getConfigurationManager();
	}
	
	public CryptographyManager getCryptographyManager() {
		return getServerMain().getCryptographyManager();
	}
	
	public DatabaseManager getDatabaseManager() {
		return getServerMain().getDatabaseManager();
	}
	
	public FileManager getFileManager() {
		return getServerMain().getFileManager();
	}
	
	public LogManager getLogManager() {
		return getServerMain().getLogManager();
	}
	
	public NetworkSpamBlockerManager getNetworkSpamBlockerManager() {
		return getServerMain().getNetworkSpamBlockerManager();
	}
	
	public RconManager getRconManager() {
		return getServerMain().getRconManager();
	}
	
	public WebServiceManager getWebServiceManager() {
		return getServerMain().getWebServiceManager();
	}
	
	public SecureRandom getSecureRandom() {
		return getServerMain().getSecureRandom();
	}
 	
	public Date getDate() {
		return getServerMain().getDate();
	}
	
	public ScheduledThreadPoolExecutor getScheduledThreadPool() {
		return getServerMain().getThreadPool();
	}
}

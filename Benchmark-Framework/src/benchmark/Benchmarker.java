/*
 * Organize Framework - Framework for groupware softwares.
 * Copyright (C) 2018  Giovani Frigo
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package benchmark;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class ThreadTeste extends Thread {
	public static String generateRandomString(int stringSize) {
		StringBuilder strBuilder = new StringBuilder();
		
		for (int i = 0; i < stringSize; i++) {
			strBuilder.append((char) (byte) (Benchmarker.random.nextInt(25) + 65));
		}
		
		return strBuilder.toString();
	}
	
	public volatile boolean finalizado = false;
	public volatile boolean executando = false;
	
	@Override
	public void run() {
		finalizado = false;
		executando = true;
		
		try {
			SecretKey key = Benchmarker.generator.generateKey();
			Cipher cipher = Cipher.getInstance(Benchmarker.tipo + "/" + Benchmarker.block + "/" + Benchmarker.padding);
			cipher.init(Cipher.ENCRYPT_MODE, key, Benchmarker.ivspec);
			
			String base64ClientEncryptedRequestData = "{ \"username\" : \"" + generateRandomString(10) + "\", \"password\" : \"ronaldomuitobom\", \"email\" : \"" + generateRandomString(15) +"\", \"displayName\" : \"pinto\", \"recoverQuestion\" : \"pinto\", \"recoverQuestionAnswer\" : \"pinto\" }";
			
			String base64ServerEncryptedClientCryptography = "{ "
			+ "\"base64key\" : \"" + Base64.getEncoder().encodeToString(key.getEncoded()) + "\","
			+ "\"keyType\" : \"" + Benchmarker.tipo + "\","
			+ "\"blockCipherMode\" : \"" + Benchmarker.block + "\","
			+ "\"base64ivSpec\" : \"" + Base64.getEncoder().encodeToString(Benchmarker.ivspec.getIV()) + "\","
			+ "\"padding\" : \"" + Benchmarker.padding + "\" }";
			
			byte[] eae = encryptUsingPublicKey(base64ServerEncryptedClientCryptography.getBytes(), Base64.getDecoder().decode(Benchmarker.base64PublicKey));
			
			String strbase64ServerEncryptedClientCryptography = Base64.getEncoder().encodeToString(eae);
			String strbase64ClientEncryptedRequestData = Base64.getEncoder().encodeToString(cipher.doFinal(base64ClientEncryptedRequestData.getBytes()));
			String strFinal = "{ \"base64ServerEncryptedClientCryptography\" : \"" + strbase64ServerEncryptedClientCryptography + "\", \"base64ClientEncryptedRequestData\" : \"" + strbase64ClientEncryptedRequestData + "\" }";
			
			URL url = new URL("http://localhost:8080/Organize-Framework/api/v1/account/createAccount");
			HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
			conexao.setDoOutput(true);
			conexao.setRequestMethod("POST");
			conexao.addRequestProperty("Content-Type", "application/json");
			conexao.setRequestProperty("Content-Length", Integer.toString(strFinal.getBytes().length));
			conexao.getOutputStream().write(strFinal.getBytes());
			conexao.connect();
			
			System.out.println(new String(conexao.getInputStream().readAllBytes()));
		} catch (IOException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException | NoSuchAlgorithmException ex) {
			
		}
		
		finalizado = true;
		executando = false;
	}
	
	
	public static byte[] encryptUsingPublicKey(byte[] decryptedData, byte[] publicKey) {
		try {
			Cipher rsa = Cipher.getInstance("RSA");
			rsa.init(Cipher.ENCRYPT_MODE, KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKey)));
	        return rsa.doFinal(decryptedData);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		return null;
	}
	
	public static byte[] decryptUsingPrivateKey(byte[] encryptedData, byte[] privateKey) {
		try {
			Cipher rsa = Cipher.getInstance("RSA");
			rsa.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKey)));
	        return rsa.doFinal(encryptedData);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		 
		return null;
	}
}

class BlocoDeTestes {
	public ArrayList<ThreadTeste> listaThreadTestes = new ArrayList<>();
	
	public BlocoDeTestes(int quantiaDeTestes) {
		for (int x = 0; x < quantiaDeTestes; x++) {
			listaThreadTestes.add(new ThreadTeste());
		}
	}
	
	public void realizarTeste() {
		for (ThreadTeste t : listaThreadTestes) {
			t.start();
		}
		
		boolean todosTerminados = false;
		while (!todosTerminados) {
			todosTerminados = true;
			
			for (ThreadTeste t : listaThreadTestes) {
				if (!t.finalizado) {
					todosTerminados = false;
					break;
				}
			}
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {}
		}
	}
}


public class Benchmarker {
	//public static final String base64PrivateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCUhddBhpZwltziotX3HwN/tc7I6QnRvzhqrJt7ZGP5RNtUlNPL0VyaC1rjOLR8XGMkFvj13TrXsIqNY+QF/3BKrrtWaWz2jqMUfP1/2jD0kQkwgvYQZgS3L3jBGpLQSuUp1P/Gdif//RrvyrYriJgE101FB+NV329GzNNO/aU2E6yLzWnPM3VRZifBrUBOfZepLx3aEAbH2xr/b/atfCE2z+D36dPP0TMkFvgxo0OiBzBNN9LvDA1uWhpSOWmYMXSJ3y6fck3TuSljDrVTziAo0Jn31rqf4Yreb5janCqsAlFulwy+sztRAea/YwpJs8f5hdJ2FdDYg4YsLkIWPxLXAgMBAAECggEAVOFAqbQPGf0980/4VCinST9QFEYKVJKh3WUT3qr676RIbMg5WowmDetbX+wySbhK+H8gfPKYBFhidneWA09+j0/YxIOvwCC4VqcwAPdjbwHMbKsLkUecfkxlCIb1TJDg8s1F99fi+EtgL5Z8EdkYd/vLrx3Y8GSnJaE9SjFvT72Cf/HqqwxQl4+AVwS3vGRHIJgefFliCKZORM1uGuEFwfFN/4Tu/blZ7yXqdsGyoOhYlsGSkYtFm5u5iZDo/jUoSWwySnPsBB0iq7+IpfJHNtVZWXRUn9DjTlbmLoschV29NucEOVEujXfWWuZ6n2FstgF37pSolfD9yJVJor5mQQKBgQDZEJ1RbD5oIYLLTtUQBadCp5UQ8lTJTUOu4P/geQiVgKTP6JWUDlVkpdZBoa0SL8n54RvGD/B+SGNHty26fIPNCnn0bu7jjiKZloHgRJ2BC8z80wgY3cdKfwGiGaDMlXwXOSorVX9tMsgdQhbgJg74lim7Rhie2DiQ+dABlc+MpwKBgQCvKdkRNAHj4/o33SZO3uFTe6BRTqQNlB/6+kFQq3ge8hc30pmijw1hwf05+2vXnamQtK7brcl526F1b2FZyQ4ibcx3Q1lq2IGrHpi/+EzCpeOVYyifIe7sWRJQ4nb+Ht/+tkcysOt2v31ADxHdaO/tO92Wfy0IXyAMmlXs5SgeUQKBgFEKVvHTS+lsuXxWTk3RDoVgM9ND5kvRwPdFocKY7Y+DnATTfSTtxQvyrm+KgSdNGEdmuAvWX5+GPyrDRRte6YZf5vBPinkC1AArpQmqFHPdiAbMczV1d7ZkfKOcleBL3yx8dzX+kbifQP9LcWb2TpiY17PHk+GaMGRQIEy3Nen5AoGBAIEDY5umIJZHPe6Tdjm1M2UXszbvoylVJlDmreDKyoiQasV02i+/CW4tQWPJaN7AgznPutKWp8SXHc6vIJbO3RhCosxc0Kk/mhH9UShjq7XaFUvcYvkCN0XhPPpSMq5s8zJZ8wrFAEjQNWJPNytKtRyYcVb9iJ+X4LUnn44CIpBhAoGAXvxLcjE/tLvALuNFkHlZVj0rB/LEq1QUcmDq7PJAk72YMmTYK6r7wra46jf+575PD34GL25G2juDvLzcIkGBZHiAdNrKFsuQ4BEmxsVd57qCI/tUfkXDsw8CWFUH07Wzp8bBO3TohtJuG9931P2Z8PK7AinfJckLD9RPmsr0j/I=";
	public static final String base64PublicKey = "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEA0OBPXiBsuI9V0t+bJmXCWjECRUE5kwImYmCX2hrALydCxUroANHDlJhr+EnhRKFl/gl8CJu1zCDoM6LTU8A3K1RqVudW76QAI73JgEw4NbTWrFRX48YRBg18Vdhyiofi6p8sASF50O0ePtpYN/OhVQDotmhcOnQJDjML+2Bp98cHoJiXD/uPliIdI9m3/gYevlP1hXFZiRvf72rV8GDWHQP9JCB5ETWyvT4ibVArRchFmDh2wKzB1C1eJzLTyQ6QqqvSFQj0SepKBpOuhifwCz3gPrP9CEzercxa03mnLhtATWDCvwnKCAdIfJM3B00TKUwnH7taK63t2Cm5mA9wwqM33wZ6zmiJs9Bk18+iVxjUwxPnyQibHCG1wEmYKsMdbm1ueH4xk1xyazMf1iZYRSDFZ7yWV/UzBxUSyoOnEUI3hDNrTitkAGTAXZaOGEERg+g0nW0W/m13RWi9/n0ZxkixX8BSH1o7N0c2JiTuWhzKfSNueBDpkneGXqwby2k/AgMBAAE=";
	public static final String tipo = "AES";
	public static final String block = "CBC";
	public static final String padding = "PKCS5PADDING";
	public static KeyGenerator generator;
	public static final IvParameterSpec ivspec = new IvParameterSpec(new byte[] {67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67});
	public static final SecureRandom random = new SecureRandom();
	
	public static void main(String args[]) throws NoSuchAlgorithmException {
		generator = KeyGenerator.getInstance(tipo);
		generator.init(128);
		
		BlocoDeTestes bloco = new BlocoDeTestes(5000);
		long tempoInicial = System.currentTimeMillis();
		bloco.realizarTeste();
		System.out.println("Tempo: " + (System.currentTimeMillis() - tempoInicial));
	}
}
